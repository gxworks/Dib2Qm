How to get started
=====

(Currently only as experimental version, with overlay keyboard)

![IMG](./img/h1010.png)

Tap on the (red) 'A' on the top bar repeatedly to see the effect.

![IMG](./img/h1020.png)

Notice the 4 navigation keys in the corners.
You can also tap on the sides: scrollbar.
The red 'X' (top left) is the ESC (reset)
button, next to it are the ZOOM buttons.
Try it out!

![IMG](./img/h1030.png)

The status bar (bottom) handles page
navigation: '>>' = forward, '<<' = backward,
('>' and '<' for supplementary pages)
Tap on '>>' to go to the next page.

On the right of the top bar you will
find the menu button (not supported yet,
currently only for dark mode) and the
clipboard buttons. Below are the
tool bar and the entry bar.

Tools:

- VW.. (= VieW ..) for flipping through the viewing modes,
- CLR (= CLeaR) for removing temporary data (from stack),
- SEND, RECeiVe etc. after ending this introduction.
- ...

Tap on '>>' to go to the next page.

Press the 'push' button
(the green '>' on the entry bar
or the status bar) to get started.

![IMG](./img/h1040.png)

Use the green keys of the overlay keyboard
(shifting up and down with the shift keys
on the left, possibly using the top A button to see the keys etc.)
to enter your e-mail address.

![IMG](./img/h1050.png)

(Use your own e-mail address, not the one shown in the sample)

Then press the 'push' button
(the green '>' on the entry bar
or the status bar).

![IMG](./img/h1060.png)

Your e-mail address is complemented by the assumed e-mail connection data.
This technical data has to match the data given by your e-mail provider/ account
(host of the provider, user name, IMAP port number, SMTP port number).

If the assumed values are not correct, change them (note the 'backspace' key
on the right for deleting parts of the data before entering the correct values).

Then press the 'push' button (the green '>').

![IMG](./img/h1070.png)

Next is your so-called access code, which is a kind of PIN for getting
faster access later on. Usually this is something rather short, depending
on your security needs. It can be a few digits or letters or combined.
Enter your PIN with the overlay keyboard, using the top A button as needed.

![IMG](./img/h1080.png)

![IMG](./img/h1090.png)

(Use your own freely choosable PIN, not the sample shown).

Then press the 'push' button (the green '>').

![IMG](./img/h1100.png)

Now enter the password of your e-mail account.

Afterwards press the 'push' button (the green '>') to finish the setup.

![IMG](./img/h1110.png)

Now you are ready to use your e-mail account for secured chats.

The app has created an internal secret key with a so-called fingerprint,
which is displayed below the three parameter lines and above your
contact data (fingerprint 1F7F3... in the sample, yours will be different).
This can be used to double-check the contact data.

Note the negative page count in the status bar. This is intentional.

Enter the e-mail address of your friend.

![IMG](./img/h1120.png)

(... not the address shown, but whatever real address you want to contact)

Press the CON+ button (= CONtact+, shown as - CO - N+ -) to add the address to your data, creating a chat.

![IMG](./img/h1130.png)

Now you have to make sure that the top A button is red:

![IMG](./img/h1140.png)

Press on that new chat.

![IMG](./img/h1150.png)

Note the changed paged number. You can use '<<' and '>>' as before to go back or forward.

Then press the INVI button (= INVIte) to send an invitation.

![IMG](./img/h1160.png)

![IMG](./img/h1170.png)

Your invitation will appear on your friend's app as a new chat if he presses RECV.

Your friend also has to press on the chat and INVI on his side to send his contact data.

Then press RECV (= RECeiVE) to get his contact data (incl. the 'fingerprint' for security).

![IMG](./img/h1180.png)

Your friend's fingerprint should then show up with a leading '?'.

Confirm the fingerprint, if it is correct, with the ACK (= ACKnowledge) button.

![IMG](./img/h1190.png)

The question mark is gone.

Use CLR (= CLeaR) to clear additional parameter data.

![IMG](./img/h1200.png)

Type a short hello and press SEND on both sides.

Use RECV to get each other's hello.

![IMG](./img/h1210.png)

Enjoy!

... and maybe give me (gxworks at mail.de) some feedback :-)
