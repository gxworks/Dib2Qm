Dib2Qm (aka Dibdib Messenger)

Messenger for secured chats and groups (end-to-end encrypted).

Dib2Qm is a messenger that works via e-mail servers, end-to-end encrypted.

Currently only as experimental version! Some of the following features are
not supported yet.

Text messages are not only end-to-end encrypted, they also get stored on your
device in an encrypted way. Images, attachments etc. can be sent -- encrypted,
of course, but since they depend on third-party programs, they are stored
unencrypted. Dib2Qm makes use of your device's 'Download' folder (also for
backups and archives).

You need an e-mail account (on a server with standard IMAP and SMTP services),
for which a small app like this one is allowed to access the services.

Just try it out! -- on a non-critical device (And if your e-mail provider blocks
it, use a different provider. Dib2Qm does not depend on a centralized server.)

- You can only chat with people who also use Dib2Qm.
- After the setup, please first start the messaging service (via menu).
- If you get a connection, the status should turn green (... 'ON').
- Add a contact, via menu: you have to enter the other one's e-mail address.
- If you then touch the new contact, you will be asked to send the 'invitation'
(i.e. the key info for exchanging secured messages).
- Once the key exchange is completely done (black/red, orange, green), the chat
works similar to other apps.

If one of your chats gets 'stuck' (due to a key exchange problem), just send
another 'invitation' from within the chat.

It is possible to use the address of your device's 'Google account', but they
insist on you explicitly allowing messenger apps: you would log in with your
browser (!), go to 'Settings', 'Forwarding...IMAP', 'Enable IMAP', 'Save', then
to 'Accounts', 'Other ... settings', '...Security', '2-Step...', 'Off', and
'Connected apps', 'Allow less secure', 'On'.

Strange? It is easier to use an extra e-mail account for Dib2Qm, but depending
on your provider you might still have to allow IMAP services. Both, using the
device's account address or using a dedicated e-mail address for Dib2Qm, have
their advantages and disadvantages ...

If you want to do a complete re-start, go to the device's settings, clear the
app data, then open a file manager, clear the 'Download/dibdib' data, and then
you can start the app a-fresh.
