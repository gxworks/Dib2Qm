Dib2Qm (sog. Dibdib Messenger)

Messenger für sichere Chats und Gruppen ('end-to-end').

Dib2Qm ist ein Messenger, der über E-Mail Server arbeitet,
Ende-zu-Ende-verschlüsselt.

Im Moment nur als experimentelle Version! Manche der nachfolgend beschriebenen
Funktionen sind noch nicht implementiert.

Textinhalte werden nicht nur Ende-zu-Ende-verschlüsselt, sondern auch auf dem
Gerät verschlüsselt gespeichert. Bilder, Anhänge etc. können auch gesandt
werden, was dann verschlüsselt geschieht, aber sie brauchen andere Apps, um
genutzt zu werden, und müssen deshalb unverschlüsselt auf dem Gerät abgelegt
werden. Dazu wird der 'Download'-Ordner des Gerätes verwendet (auch z.B. für
Backups und für Archive).

Um Dib2Qm nutzen zu können, brauchen Sie einen E-Mail-Account, d.h. eine
'normale' E-Mail-Adresse. Bei deren Anbieter müssen die Einstellungen so
vorgenommen werden, dass eine kleine App wie diese die Services des Accounts
nutzen darf (insbesondere IMAP und SMTP). Manche Anbieter blockieren das leider
mehr oder weniger stark.

Ggf. einfach ausprobieren! -- auf einem unkritischen Gerät. Und wenn der E-Mail-
Anbieter die Dienste blockiert bzw. sich nicht umstellen lässt, einen anderen
ausprobieren. Dib2Qm ist nicht abhängig von irgendwelchen zentralistischen
Internet-Diensten.

- Ein Chat funktioniert nur mit Leuten, die auch Dib2Qm benutzen.
- Nach den ersten Einstellungen, bitte per Menü den Service starten.
- Wenn die Verbindung klappt, wird der Status grün (... ON).
- Einen Kontakt hinzufügen, per Menü: E-Mail-Addresse des Kontaktes eingeben.
- Danach den Kontakt antippen und eine 'Einladung' senden.
- Wenn der Schlüsseltausch abgeschlossen ist (schwarz/rot, orange, grün),
dann funktioniert der Chat ähnlich wie bei anderen Apps.

Falls ein Chat mit Fehler hängen bleibt bzw. Nachrichten plötzlich nicht mehr
durchkommen, liegt es meist an einem seltenen Netzwerk-Fehler. Dann einfach
eine neue 'Einladung' (bzw. Schlüssel) innerhalb des Chats senden.

Wenn Sie diese App löschen oder komplett neu starten wollen, müssen bei den
Einstellungen des Gerätes die Daten der App gelöscht werden und außerdem die
Daten im 'Download/dibdib' Ordner.
