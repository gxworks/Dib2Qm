// Copyright (C) 2022, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_net;

import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_feed.QOpFeed;

// =====

/** For networking thread as triggered by MainThreads. */
public enum NetRunner implements QIfs.QRunnableIf {

  // =====

  INSTANCE,
  ;

  QToken mTask;
  QToken mNext = null;

  @Override
  public boolean guard(QToken xTask, long... xParameters) {
    return xTask.op instanceof QOpNet;
  }

  @Override
  public int start(long xTick, QToken xmTask) {
    mNext = null;
    mTask = (xmTask.op instanceof QOpNet) ? xmTask : null;
    return (null == mTask) ? 0 : 1;
  }

  @Override
  public int step(long dummy) {
    return (null == mTask) ? 0 : 1;
  }

  @Override
  public int stepAsync() {
    mNext = mTask;
    mNext = QOpNet.exec(mTask);
    return 0;
  }

  @Override
  public QToken call() {
    if (null == mNext) {
      mNext = mTask;
      mNext.wip = QWord.FALSE;
      mNext.op = QOpFeed.zzGET;
    }
    return mNext;
  }

  @Override
  public void removeWipData4Interrupts() {}

  // =====
}
