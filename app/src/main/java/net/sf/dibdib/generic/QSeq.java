// Copyright (C) 2016, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.generic;

import java.util.Arrays;
import net.sf.dibdib.config.Dib2Root;
import net.sf.dibdib.generic.QIfs.*;
import net.sf.dibdib.thread_any.ShashFunc;
import net.sf.dibdib.thread_ui.UiValTag;

public class QSeq extends QSeqR {

  public static final QWordIf[] EMPTY_WORD_LIST = new QWordIf[0];

  public static QSeq NIL = new QSeq(EMPTY_WORD_LIST, 0);

  protected QSeq(QWordIf[] xmWords, long stamp) {
    mWords = xmWords;
    this.shash = stamp;
    jStart = 0;
    jEnd = (null != mWords) ? mWords.length : 0; // ((0 == stamp) ? 0 : 1);
  }

  private static QSeq createQSeq(boolean asTemp, int xjStart, int xjEnd, QWordIf... seq) {
    long stamp = 0;
    int shift = 0;
    if ((0 > xjStart) || (xjStart > xjEnd) || (xjEnd > seq.length)) {
      return QWord.NaN;
    } else if (((xjEnd - xjStart) == 1) && (seq[xjStart] instanceof QSeq)) {
      return (QSeq) seq[xjStart];
    }
    int j0 = xjStart;
    for (int i0 = 0; (i0 < 10) && (64 > shift); ++i0, ++j0) {
      if (j0 >= xjEnd) {
        break;
      }
      final QSeqIf wd = seq[j0];
      if (!(wd instanceof QWord) || !ShashFunc.isSememe(((QWord) wd).shash)) {
        continue;
      }
      final long next = ((QWord) wd).shash & ~1L;
      stamp = stamp | (next >>> shift);
      if (0 != (next & 0xffffffL)) {
        break;
      }
      int shift2 = 16;
      for (; shift2 < 64; ++shift2) {
        if (0 != (next << (64 - shift2))) {
          break;
        }
      }
      shift += 6 + shift2;
    }
    if ((0 == xjStart) && (seq.length == xjEnd)) {
      return new QSeq(seq, stamp);
    } else if (!asTemp && ((xjEnd - xjStart) < (seq.length - 32))) {
      return new QSeq(Arrays.copyOfRange(seq, xjStart, xjEnd), stamp);
    }
    QSeq out = new QSeq(seq, stamp);
    out.jStart = xjStart;
    out.jEnd = xjEnd;
    return out;
  }

  public static QSeq createQSeq(int xjStart, int xjEnd, QWordIf... seq) {
    return createQSeq(false, xjStart, xjEnd, seq);
  }

  public static QSeq createQSeq4Temp(int xjStart, int xjEnd, QWordIf... seq) {
    return createQSeq(true, xjStart, xjEnd, seq);
  }

  public static QSeq createQSeq(QWordIf... seq) {
    return createQSeq(0, seq.length, seq);
  }

  public static QSeq createFlat(QSeq... seq) {
    QWordIf[] out = new QWordIf[seq.length];
    int c0 = 0;
    for (QSeq s0 : seq) {
      if (s0 instanceof QWordIf) {
        out[c0++] = (QWordIf) s0;
        continue;
      }
      out[c0++] = QWord.createQWord(s0.toStringFull(), false);
    }
    return QSeq.createQSeq(out);
  }

  public static QSeq createQSeq(String str) {
    QWordIf[] a0 = words4String(str);
    if (0 == a0.length) {
      return NIL;
    } else if (1 == a0.length) {
      return (QSeq) a0[0];
    }
    QSeq out = createQSeq(a0);
    out.zString = str;
    return out;
  }

  /** This could be used some other time for referenced data/ lazy evaluation. */
  private QSeq getActualObject() {
    return this;
  }

  /** Getter, returns -1 if WiP. */
  public int size() {
    if (null == mWords) {
      return 1;
    }
    final QSeq out = getActualObject();
    if (null == out) {
      return -1;
    }
    return out.jEnd - out.jStart;
  }

  public String toStringFull() {
    if (null != zString) {
      return zString;
    }
    if (null == mWords) {
      return toString();
    }
    final QSeq out = getActualObject();
    if (null == out) {
      return "(WiP)";
    } else if (null == out.zString) {
      out.zString = out.asString4Array(0x5);
    }
    return out.zString;
  }

  public String format(boolean hex) {
    final QSeq out = getActualObject();
    if (null == out) {
      return "(WiP)";
    }
    return out.asString4Array(hex ? 0x3 : 0x1);
  }

  public QWord atom() {
    // :-)
    if (this instanceof QWord) {
      return (QWord) this;
    } else if (null == mWords) {
      return QWord.NaN;
    }
    final QSeq out = getActualObject();
    if (1 == (out.jEnd - out.jStart)) {
      final QSeqIf ax = out.mWords[jStart];
      // Unintended case.
      if (ax instanceof QWord) {
        return (QWord) ax;
      }
    }
    return QWord.NaN;
  }

  public QWordIf at(int index) {
    if (this instanceof QWord) {
      return ((0 == index) || (-1 == index)) ? ((QWord) this) : QWord.NaN;
    } else if (null == mWords) {
      return QWord.NaN;
    }
    final QSeq out = getActualObject();
    int i0 = (0 <= index) ? (index + out.jStart) : (out.jEnd + index);
    if ((out.jStart <= i0) && (i0 < out.jEnd)) {
      return out.mWords[i0];
    }
    return QWord.NaN;
  }

  public QWord atomAt(int index) {
    QSeqIf ax = at(index);
    return (ax instanceof QWord) ? (QWord) ax : QWord.NaN;
  }

  public QWordIf[] atoms() {
    final QSeq out = getActualObject();
    if (null == out.mWords) {
      return new QWordIf[] {out.atom()};
    }
    if ((0 == out.jStart) && (out.mWords.length == out.jEnd)) {
      return out.mWords;
    }
    QWordIf[] a0 = new QWordIf[out.jEnd - out.jStart];
    System.arraycopy(out.mWords, out.jStart, a0, 0, a0.length);
    return a0;
  }

  public QSeq sub(int xjStart, int xjEnd) {
    if (0 > xjStart) {
      return QWord.NaN;
    }
    if (xjStart == xjEnd) {
      return NIL;
    }
    final QSeq out = getActualObject();
    if (out instanceof QWord) {
      //      if ((0 == xjStart) && ((1 == xjEnd) || (-1 == xjEnd))) {
      return (QWord) out;
      //      }
      //      return QWord.NaN;
    } else if (null == mWords) {
      return QWord.NaN;
    }
    int j0 = xjStart + out.jStart;
    int j1 = (0 <= xjEnd) ? (xjEnd + out.jStart) : (out.jEnd + 1 + xjEnd);
    if ((j0 <= j1) && (j1 <= out.jEnd)) {
      if (j0 == j1) {
        return NIL;
      } else if ((j0 + 1) == j1) {
        return (out.mWords[j0] instanceof QSeq) ? (QSeq) out.mWords[j0] : QWord.NaN;
      }
      return createQSeq(Arrays.copyOfRange(out.mWords, j0, j1));
    }
    return QWord.NaN;
  }

  public QSeq appendWords(int offset, int end, QWordIf... xSeq) {
    if (0 == xSeq.length) {
      return this;
    } else if (QWord.V_NULL == this) {
      return createQSeq(xSeq);
    }
    final QSeq me = getActualObject();
    if (null == me) {
      return QWord.NaN;
    }
    final int size = ((null == mWords) ? 1 : (me.jEnd - me.jStart)) + (end - offset);
    QWordIf[] out = new QWordIf[size];
    if (null == mWords) {
      out[0] = (this instanceof QWord) ? (QWord) this : QWord.NaN;
    } else {
      System.arraycopy(me.mWords, me.jStart, out, 0, me.jEnd - me.jStart);
    }
    System.arraycopy(xSeq, offset, out, size - xSeq.length, end - offset);
    return createQSeq(out);
  }

  public QSeq append(QSeq xSeq) {
    if ((QSeq.NIL == this) || (QSeq.NIL == xSeq)) {
      return (QSeq.NIL == this) ? xSeq : this;
    }
    if (xSeq instanceof QWord) {
      return appendWords(0, 1, (QWord) xSeq);
    }
    final QSeq me = getActualObject();
    final QSeq other = xSeq.getActualObject();
    if ((null == me) || (null == other) || (null == other.mWords)) {
      return QWord.NaN;
    }
    return appendWords(other.jStart, other.jEnd, other.mWords);
  }

  public static QWordIf[] words4String(String str) {
    String[] elements = ShashFunc.markedAtoms4String(str);
    QWordIf[] out = new QWordIf[2 + 2 * elements.length];
    int count = 0;
    boolean skipped = false;
    boolean punct = true;
    for (int i0 = 0; i0 < elements.length; ++i0) {
      final String el = elements[i0];
      if (0 >= el.length()) {
        continue;
      }
      if ((el.equals(" ") || el.equals("" + ShashFunc.ValType.FLIT.marker + ' ')) && !punct) {
        skipped = true;
        punct = true;
        continue;
      }
      final char ch0 = el.charAt(0);
      QWord word; // = QWord.NONE;
      //      String shash = null;
      if (ShashFunc.SHASH_CHAR_OFFS > ch0) {
        word = QWord.createQWord(el, true);
      } else if (0xf800 <= ch0) {
        word = QWord.createQWord(el, true);
      } else if (ShashFunc.ValType.LIT.marker <= ch0) {
        word = QWord.createQWord(el.substring(1), true);
      } else if ((ShashFunc.ValType.FLIT.marker == ch0) || (ShashFunc.ValType.SLIT.marker == ch0)) {
        word = QWord.createQWordPunct(el.substring(1));
      } else if ((ShashFunc.ValType.DATE.marker == ch0)) {
        word = QWord.createQWordDate(el.substring(1));
      } else if ((ShashFunc.ValType.X.marker == ch0) || (ShashFunc.ValType.NUM.marker <= ch0)) {
        word = QWord.createQWordNumeric(el.substring(1));
      } else {
        // Marked.
        // TODO Special treatment for hex, base64, SHASH_NUM etc.
        word =
            QWord.createQWord(
                el.substring(1), true); // handleCached4AtomicLiteral(zTmp, el.substring(1));
      }
      boolean precPunct = punct;
      punct =
          !ShashFunc.isSememe(
              word.shash); // (((word.shash >>> (48 + 3)) | 0xe000) < ShashFunc.SHASH_NUM);
      if (!punct) {
        if (!precPunct) {
          out[count++] = QWord.createQWordPunct("");
        }
      } else {
        if (precPunct && skipped) {
          out[count++] = QWord.V_BLANK;
        }
      }
      skipped = false;
      if (QSeq.NIL != word) {
        out[count++] = word;
      }
    }
    return Arrays.copyOf(out, count);
  }

  private String asString4Array(long bDecBasexFull, String... fillers) {
    final int baseX = UiValTag.UI_NUMBER_BASE_SEC.i32(Dib2Root.app.qTick.get());
    if (this instanceof QWord) {
      if (3 == ((int) bDecBasexFull & 0x3)) {
        return ((QWord) this).toString() + '\t' + ((QWord) this).toBase(baseX);
      }
      return (0 != ((int) bDecBasexFull & 0x1))
          ? ((QWord) this).toString()
          : ((QWord) this).toBase(baseX);
    }
    final QSeqIf[] a0 = getActualObject().mWords;
    boolean full = (0 != (bDecBasexFull & 4L)) || (100 >= (jEnd - jStart));
    final StringBuilder out = new StringBuilder(128 + 16 * a0.length);
    final String filler = (0 < fillers.length) ? fillers[0] : " ";
    boolean punct = true;
    final int jSkip = jStart + 50;
    String sep = "";
    int delta = 2 - ((((int) bDecBasexFull) >>> 1) & 1);
    for (int hex = ((int) bDecBasexFull & 1) - 1; hex >= -1; hex -= delta) {
      out.append(sep);
      sep = "\t";
      for (int i0 = jStart; i0 < jEnd; ++i0) {
        final QSeqIf word = a0[i0];
        String s0 = "?";
        if (!full && (jSkip <= i0) && (i0 < (jSkip + 10))) {
          out.append(" ... ");
          i0 = jEnd - 20;
          punct = true;
          continue;
        }
        if (null == word) {
          continue;
        } else if (word instanceof QEnumIf) {
          s0 = ((QEnumIf) word).name();
        } else if (!(word instanceof QWord)) {
          out.append("^");
          continue;
        } else {
          s0 = (0 == hex) ? word.toString() : ((QWord) word).toBase(baseX);
        }
        //		final char shash0 =
        //				(char)
        //						(0xe000
        //								| ((word instanceof QWord)
        //										? (((QWord) word).shash >>> (48 + 3))
        //										: ShashFunc.ValType.LIT.marker));
        if ((0 == s0.length())
            || (((word instanceof QWord)
                && !ShashFunc.isSememe(
                    ((QWord) word).shash)))) { // || (ShashFunc.SHASH_NUM > shash0)) {
          // || QStr.PATTERN_SPACE_NL.matcher( "" + s0.charAt( 0 ) ).matches()) {
          // if (!(0 == s0.length()) && (ShashFunc.SHASH_ERR_MAX > shash0)) {
          //	s0 = (ShashFunc.SHASH_STR_OFFS == shash0) ? "NaN" : "ERROR";
          // }
          punct = true;
        } else {
          if (!punct) {
            out.append(filler);
          }
          punct = false;
        }
        out.append(s0);
      }
    }
    final String str = out.toString();
    if (null == zString) {
      zString = str;
    }
    return str;
  }

  /////

  @Override
  public String toString() {
    if (null != zString) {
      return zString;
    } else if (null == getActualObject()) {
      return "WiP";
    } else if (null == mWords) {
      zString = "0aNaN";
    } else {
      zString = asString4Array(0x1);
    }
    return zString;
  }
}
