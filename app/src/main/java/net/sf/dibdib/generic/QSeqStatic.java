// Copyright (C) 2016, 2020  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.generic;

import java.util.Arrays;
import java.util.regex.*;
import net.sf.dibdib.config.Dib2Constants;
import net.sf.dibdib.thread_any.StringFunc;

// TODO ...

public class QSeqStatic {

  // QWord[] seq = null;

  public int cItems = 0;
  public QWordStatic[] items = zNil;
  /** Also used for linking recycled instances ... */
  public QSeqStatic optionalErrorMsg = NIL;

  /////

  private static final QWordStatic[] zNil = new QWordStatic[0];
  private byte[] zShash;
  private String zString;

  private static final String REGEX_SPACE_NL = "[\\s\\p{Z}]";
  private static final Pattern PATTERN_SPACE_NL = Pattern.compile(REGEX_SPACE_NL);
  private static final Pattern PATTERN_SPACE_NL_SEQ = Pattern.compile(REGEX_SPACE_NL + "+");
  private static final Pattern PATTERN_SPACE_BEGIN = Pattern.compile("^\\s+");
  private static final Pattern PATTERN_SPACE_END = Pattern.compile("\\s+$");
  private static final String REGEX_LINE_BREAK =
      "\\r?[\\n\\u0085\\u2028\\u2029]"; // "\\r?[\\n\\v\\u0085\\u2028\\u2029]";
  private static final Pattern PATTERN_LINE_BREAK_TAB =
      Pattern.compile("\\r?[\\n\u0085\u2028\u2029\\t]"); // "\\r?[\\n\\v\\u0085\\u2028\\u2029\\t]"
  private static final Pattern PATTERN_WORD_CONNECTOR =
      Pattern.compile("[\\p{L}\\p{M}\\p{N}\\p{Pc}[\\p{InEnclosedAlphanumerics}&&\\p{So}]]+");
  private static final Pattern PATTERN_WORD_BASIC = Pattern.compile("[\\p{L}\\p{M}\\p{N}]+");
  public static final Pattern PATTERN_WORD_SYMBOL = Pattern.compile("[\\p{L}\\p{M}\\p{N}\\p{S}]+");
  private static final Pattern PATTERN_SYMBOLS = Pattern.compile("\\p{S}+");
  // public static final Pattern PATTERN_LETTERS_CASED = Pattern.compile( "\\p{L&}+" );
  private static final Pattern PATTERN_PUNCTUATION = Pattern.compile("\\p{P}+");
  private static final Pattern PATTERN_CONTROLS_UNI = Pattern.compile("\\p{Cc}+");
  private static final Pattern PATTERN_CONTROLS_ANSI = Pattern.compile("\\p{Cntrl}+");
  private static final Pattern PATTERN_DIGITS = Pattern.compile("\\p{Nd}+");
  private static final Pattern PATTERN_DIGITS_BASIC = Pattern.compile("\\p{Digit}+");
  private static final Pattern PATTERN_HEXS = Pattern.compile("\\p{XDigit}+");
  private static final Pattern PATTERN_NUMERICS = Pattern.compile("\\p{N}+");

  private static final String REGEX_NUMBER =
      "((([\\+\\-])|([0-9]\\.)|([1-9]))[0-9_\u00B7\\'\\.\\,]*[0-9])|([0-9])";
  private static final String REGEX_NUMBER_SEP = "[_\u00B7\\']";
  private static final Pattern PATTERN_NUMBER_SEP = Pattern.compile(REGEX_NUMBER_SEP);
  private static final String REGEX_UNIT_SUFFIX = "_[\\./A-z0-9\\p{Sc}]+";
  private static final Pattern PATTERN_UNIT = Pattern.compile(REGEX_UNIT_SUFFIX);
  private static final String REGEX_DIGITS_TEL_ETC = "[\\+\\#0][0-9\\-\\*]+\\#?";
  private static final Pattern PATTERN_DIGITS_TEL_ETC = Pattern.compile(REGEX_DIGITS_TEL_ETC);
  private static final String REGEX_OID_DIGITS_ETC = "[0-9\\%][0-9A-Za-z_\\^\\~]+";
  private static final Pattern PATTERN_OID_DIGITS_ETC = Pattern.compile(REGEX_OID_DIGITS_ETC);
  // private static final String REGEX_NUMBER_UNIT = REGEX_NUMBER + REGEX_UNIT_SUFFIX;
  private static final Pattern PATTERN_NUMBER = Pattern.compile(REGEX_NUMBER);
  private static final String REGEX_FLOAT_DEC =
      "[\\+\\-]?[0-9_\u00B7\\'\\.\\,]*[0-9][eE][\\+\\-]?[0-9]+[\\~0-9]*" // ,
      ; // + REGEX_UNIT_SUFFIX;
  private static final Pattern PATTERN_FLOAT_DEC = Pattern.compile(REGEX_FLOAT_DEC);
  private static final String REGEX_FLOAT_HEX =
      "[\\+\\-]?0[xX][0-9A-Fa-f_\u00B7\\'\\.\\,]*[0-9A-Fa-f]([pP][\\+\\-]?[0-9]+[\\~0-9]*)?"; // +
  // REGEX_UNIT_SUFFIX;
  private static final Pattern PATTERN_FLOAT_HEX = Pattern.compile(REGEX_FLOAT_HEX);
  private static final String REGEX_DATE =
      "\\-?[0-9]+\\-[0-1][0-9]\\-[0-9][0-9]T?[\\.0-9\\:\\+\\-]*";
  private static final Pattern PATTERN_DATE = Pattern.compile(REGEX_DATE);
  public static final Pattern PATTERN_DATE_D =
      Pattern.compile("[0-9][0-9]\\.[0-1][0-9]\\.[12]?[0-9]?[0-9][0-9]");
  public static final Pattern PATTERN_TIME = Pattern.compile("[0-9]+\\:[0-9][0-9](\\:[0-9][0-9])?");
  /** Include '_' for IDs in programming and '^'/'~' for OIDs/base64 */
  private static final String REGEX_WORD =
      "[_\\^\\~\\p{L}\\p{S}\\p{N}\\p{M}]*[\\p{L}\\p{S}][_\\^\\~\\p{L}\\p{S}\\p{N}\\p{M}]*";
  // private static final String REGEX_WORD =
  // "[_\\^\\~\\w\\p{L}\\p{S}\\p{N}\\p{M}]*[\\p{L}\\p{S}][_\\^\\~\\w\\p{L}\\p{S}\\p{N}\\p{M}]*";
  private static final Pattern PATTERN_WORD = Pattern.compile(REGEX_WORD);
  // Literal 'with meaning' or as ID etc., base64 x-chars explicit:
  // private static final String REGEX_SEMEME = "[\\w\\p{L}\\p{S}\\p{N}\\_\\^\\~\\p{Po}][\\-
  // \\w\\p{L}\\p{S}\\p{N}_\\^\\~\\p{Po}]*";
  // private static final Pattern PATTERN_SEMEME = Pattern.compile( REGEX_SEMEME );
  private static final String CHARS_NUM_FIRST = "+-0123456789#";
  private static final String CHARS_STRUC = "()[]{}<>"; // (cmp. Unicode 'Ps'/'Pe' open/close)
  // private static final String CHARS_STRUC_LINE = "./=;|-*:";
  private static final String CHARS_QUOTE_LEFT =
      "'\"\u00ab\u2018\u201b\u201c\u201f\u2039"; // (cmp. Unicode 'Pi' initial)
  // private static final String CHARS_QUOTE_RIGHT = "'\"\u00bb\u2019\u201d\u203a"; // (cmp. Unicode
  // 'Pf' final)
  private static final String CHARS_STRUC_MOD = "?%!&@"; // '$' moved to currency
  private static final String CHARS_FORMAT = "*_+^~"; // markdown, '#' moved to numeric
  private static final String CHARS_MARKER = CHARS_FORMAT + CHARS_STRUC + CHARS_STRUC_MOD;
  private static final String CHARS_MARKER_PLUS_LEFT = ".:" + CHARS_MARKER;
  private static final String CHARS_MARKER_DOLLAR = "$#" + CHARS_MARKER;

  // private static final String REGEX_CURRENCY = "\\p{Sc}";

  private static boolean isCurrency(char ch0) {
    return ('$' == ch0) || ((0xa2 <= ch0) && (ch0 <= 0xa5)) || ((0x20a0 <= ch0) && (ch0 < 0x20cf));
  }

  /////

  public static final QSeqStatic NIL = new QSeqStatic();
  public static final byte[] NIL_SHASH = QWordStatic.shash4QWord(QWordStatic.EMPTY);

  /////

  private QSeqStatic() {
    cItems = 0;
    items = zNil;
    optionalErrorMsg = (this == NIL) ? this : NIL;
  }

  public QSeqStatic(QWordStatic xmWord) {
    cItems = 1;
    items = new QWordStatic[] {xmWord};
    optionalErrorMsg = NIL;
  }

  public static QSeqStatic create() {
    // TODO recycle
    return new QSeqStatic();
  }

  /** Transform text string (with quote markers STX/ETX etc.) ... */
  public QSeqStatic build(String xStr) {
    String str = xStr;
    //        (xStr.startsWith("" + StringFunc.QUOTE_START) && xStr.endsWith("" +
    // StringFunc.QUOTE_END))
    //            ? xStr.substring(1, xStr.length() - 1)
    //            : xStr;
    int cOut = 0;
    QWordStatic[] out = new QWordStatic[str.length() >> 2];
    int qLevel = 0;
    String[] chunks = str.split(" ", -1);
    QWordStatic separator = QWordStatic.asQWord(0);
    // TODO if (1>=chunks.length ... separator = ...
    for (String chunk0 : chunks) {
      if ((cOut + 5) >= out.length) {
        out = Arrays.copyOf(out, 2 * out.length);
      }
      if (0 >= chunk0.length()) {
        out[cOut++] = QWordStatic.EMPTY;
        continue;
      }
      boolean zj = false;
      for (String chunk = chunk0; 0 < chunk.length(); chunk = chunk0) {
        out[cOut++] = zj ? QWordStatic.EMPTY : separator;
        chunk0 = "";
        zj = true;
        final int mem0 = chunk.indexOf(StringFunc.QUOTE_START);
        final int mem1 = chunk.indexOf(StringFunc.QUOTE_END);
        if ((0 <= mem0) && ((0 > mem1) || (mem0 < mem1))) {
          ++qLevel;
          if (0 < mem0) {
            chunk0 = chunk.substring(mem0 + 1);
            chunk = chunk.substring(0, mem0);
          } else {
            chunk = chunk.substring(1);
          }
        } else if (0 <= mem1) {
          qLevel -= (0 < qLevel) ? 1 : 0;
          if (0 < mem1) {
            out[cOut++] = QWordStatic.createLiteral(chunk.substring(0, mem1));
          }
          chunk0 = chunk.substring(mem1 + 1);
          // Might have more QUOTE markers ...:
          continue;
        }
        if (0 < qLevel) {
          out[cOut++] = QWordStatic.createLiteral(chunk);
        } else {
          int c0 = splitTextAppend(out, cOut, chunk);
          while (c0 < 0) {
            out = Arrays.copyOf(out, 2 * out.length);
            c0 = splitTextAppend(out, cOut, chunk);
          }
          cOut += c0;
        }
      }
    }
    ///// Remove implicit blanks:
    if (0 == QWordStatic.asQWord(separator)) {
      int delta = 0;
      for (int i0 = 2; i0 < cOut; ++i0) {
        if (out[i0 - 1] == QWordStatic.asQWord(0)) {
          if (QWordStatic.isSememe(out[i0 - 2]) && QWordStatic.isSememe(out[i0])) {
            ++delta;
            out[i0 - delta] = out[i0];
            ++i0;
            continue;
          }
          out[i0 - 1 - delta] = QWordStatic.EMPTY;
        }
        out[i0 - delta] = out[i0];
      }
      cOut -= delta;
    }
    // For recycling:
    int cap = (64 < cOut) ? cOut : ((16 < cOut) ? 64 : ((4 < cOut) ? 16 : ((1 < cOut) ? 4 : 1)));
    items = Arrays.copyOf(out, cap);
    cItems = cOut;
    return this;
  }

  private static int splitTextAppend(QWordStatic[] out, int cOut, String xText) {
    String slice = xText;
    while (0 < slice.length()) {
      if ((cOut + 4) >= out.length) {
        return -1;
      }
      int cut = 0;
      char ch0 = slice.charAt(0);
      for (; cut < slice.length(); ++cut) {
        if (false // .
            || (('a' <= ch0) && (ch0 <= 'z')) // .
            || (('0' <= ch0) && (ch0 <= '9')) // .
            || (('A' <= ch0) && (ch0 <= 'Z')) // .
            || (' ' > ch0) // .
            || (true // .
                && !isCurrency(ch0) // .
                && (0
                    > (CHARS_MARKER_PLUS_LEFT + CHARS_QUOTE_LEFT + '.')
                        .indexOf(slice.charAt(cut))))) {
          break;
        }
      }
      if (cut >= slice.length()) {
        out[cOut++] = QWordStatic.createFunctional(slice);
        slice = "";
        continue;
      }
      String piece = slice;
      slice = "";
      String part = piece;
      byte markerPart = QWordStatic.SHASH_LIT;
      String pre = "";
      boolean num = (0 < CHARS_NUM_FIRST.indexOf(ch0));
      if (0 < cut) {
        if ((1 == cut)
            && (1 < piece.length())
            && (('+' == piece.charAt(0)) || ('-' == piece.charAt(0)))
            && ('0' <= piece.charAt(1))
            && ('9' >= piece.charAt(1))) {
          //            cut = 0;
          num = true;
        } else {
          // Potential further splitting later on:
          pre = piece.substring(0, cut);
          part = piece.substring(cut);
          ch0 = part.charAt(0);
        }
      }
      cut = -1;
      boolean date = false;
      boolean id = false;
      if (num) {
        num = false;
        Matcher mx;
        if ((mx = PATTERN_FLOAT_HEX.matcher(part)).find() && (0 == mx.start())) {
          cut = mx.end();
          num = true;
        } else if ((mx = PATTERN_DATE.matcher(part)).find() && (0 == mx.start())) {
          cut = mx.end();
          date = true;
        } else if ((mx = PATTERN_FLOAT_DEC.matcher(part)).find() && (0 == mx.start())) {
          cut = mx.end();
          num = true;
        } else if ((mx = PATTERN_NUMBER.matcher(part)).find() && (0 == mx.start())) {
          cut = mx.end();
          int cut2 = 0;
          if ((mx = PATTERN_TIME.matcher(part)).find() && (0 == mx.start())) {
            cut2 = mx.end();
          } else if ((mx = PATTERN_DIGITS_TEL_ETC.matcher(part)).find() && (0 == mx.start())) {
            cut2 = mx.end();
          } else if ((mx = PATTERN_OID_DIGITS_ETC.matcher(part)).find() && (0 == mx.start())) {
            cut2 = mx.end();
            cut = cut2;
            id = true;
          }
          num = cut2 < cut;
          cut = num ? cut : cut2;
        } else if ((mx = PATTERN_DIGITS_TEL_ETC.matcher(part)).find() && (0 == mx.start())) {
          cut = mx.end();
        } else if ((mx = PATTERN_OID_DIGITS_ETC.matcher(part)).find() && (0 == mx.start())) {
          cut = mx.end();
          id = true;
        }
        if (num && (mx = PATTERN_UNIT.matcher(part.substring(cut))).find() && (0 == mx.start())) {
          cut += mx.end();
        }
      } else if ((1 < part.length() && ('\'' == part.charAt(1)))) {
        switch (ch0) { // TODO
          case 'P': // packed base64x
          case 'I': // id/ indirect
          case 'X': // octets
            cut = part.lastIndexOf('\'');
            if (1 >= cut) {
              cut = -1;
            } else {
              //                part = SHASH_TEMP_BITLIST + part;
              cut += 2;
            }
          default:
            ;
        }
      }
      if (0 > cut) {
        Matcher mx = null;
        cut = (mx = PATTERN_WORD.matcher(part)).find() ? mx.start() : part.length();
        int cutx = (mx = PATTERN_NUMERICS.matcher(part)).find() ? mx.start() : cut;
        if (cut <= cutx) {
          cutx = (mx = PATTERN_DIGITS.matcher(part)).find() ? mx.start() : cut;
          if (cut <= cutx) {
            cutx = (mx = PATTERN_WORD.matcher(part)).find() ? mx.start() : -1;
          }
        }
        cut = cutx;
        if (0 > cut) {
          //            pre = (0 < pre.length()) ? pre : ("" + SHASH_FLITERAL);
          pre += part;
          out[cOut++] = QWordStatic.createFunctional(pre);
          continue;
        }
        if (0 < cut) {
          //            pre = (0 < pre.length()) ? pre : ("" + SHASH_FLITERAL);
          pre += part.substring(0, cut);
          part = part.substring(cut);
        }
        cut = mx.end() - cut;
        // TODO private use area ...
        //          if ((SHASH_OFFS & 0xff00) <= ch0) {
        //            part = SHASH_LITERAL + part;
        //            ++cut;
        //          }
      } else if (date) {
        num = false;
        markerPart = QWordStatic.SHASH_DATE;
        ++cut;
      } else if (id) {
        num = false;
        markerPart = QWordStatic.SHASH_ID;
        ++cut;
      } else if (num && (0 < pre.length()) && (pre.endsWith("+") || pre.endsWith("-"))) {
        part = pre.charAt(pre.length() - 1) + part;
        ++cut;
        pre = pre.substring(0, pre.length() - 1);
      }
      if (num) {
        if ((0 < pre.length()) && isCurrency(pre.charAt(pre.length() - 1))) {
          part = pre.charAt(pre.length() - 1) + part;
          ++cut;
          pre = pre.substring(0, pre.length() - 1);
        }
        markerPart = QWordStatic.SHASH_NUM_START; // part = SHASH_TEMP_NUM_STRING + part;
        ++cut;
      }
      if (1 < pre.length()) {
        out[cOut++] = QWordStatic.createFunctional(pre);
        pre = "";
        if (0 > cut) {
          cut = 0;
        }
      }
      if ((cut >= part.length()) || (0 >= cut)) {
        out[cOut++] = QWordStatic.create(part, markerPart);
        continue;
      }
      slice = part.substring(cut);
      //      if (0xf800 <= part.charAt(0)) {
      //        out[cOut++] = QWord.createLiteral(part.substring(0, cut));
      //      } else {
      out[cOut++] = QWordStatic.create(part.substring(0, cut), markerPart);
    }
    return cOut;
  }

  @Override
  public String toString() {
    if (0 == cItems) {
      return "";
    }
    if (null == zString) {
      boolean wasSememe = false;
      StringBuilder out = new StringBuilder(cItems * 16 + 16);
      for (int i0 = 0; i0 < cItems; ++i0) {
        boolean isSememe = QWordStatic.isSememe(items[i0]);
        if (wasSememe && isSememe) {
          out.append(' ');
        }
        wasSememe = isSememe;
        out.append(QWordStatic.string4QWord(items[i0]));
      }
      // Atomic:
      zString = out.toString();
    }
    return zString;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof QSeqStatic) {
      final QSeqStatic other = (QSeqStatic) obj;
      if (cItems != other.cItems) {
        return false;
      }
      return Arrays.equals(items, other.items);
    }
    return false;
  }

  public byte[] toShash() {
    if (0 == cItems) {
      return NIL_SHASH;
    }
    if (null == zShash) {
      byte[] out = QWordStatic.shash4QWord(items[0]);
      int cnt = out.length;
      out = Arrays.copyOf(out, Dib2Constants.SHASH_MAX);
      for (int i1 = 1; (i1 < cItems) && (cnt < out.length); ++i1) {
        out[cnt++] = NIL_SHASH[0];
        byte[] shash = QWordStatic.shash4QWord(items[i1]);
        for (int i0 = 0; (i0 < shash.length) && (cnt < out.length); ++i0) {
          out[cnt++] = shash[i0];
        }
      }
      // Atomic:
      zShash = Arrays.copyOf(out, cnt);
    }
    return zShash;
  }

  @Override
  public int hashCode() {
    if (null == zShash) {
      zShash = toShash();
    }
    int out = 0;
    for (int i0 = 0; (i0 < 5) && (i0 < zShash.length); ++i0) {
      out = (out << 6) | zShash[i0];
    }
    return out;
  }
}
