// Copyright (C) 2018, 2021  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.generic;

import java.util.Arrays;

/** For token flow/ relay model, based on Petri net (Arc weight = 1)
 * with typed/ guarded Places for data tokens.
 * (Places impose guards on their preceding Transitions).
 * Elements of net ('Context'):
 * - (Q)Process (synchronized split): incoming (Q)Places with single Arcs + single Transition.
 * - (Q)Dispatcher (merged choice): single (unguarded) incoming Place + identity Transitions.
 * - (Q)Store (merged source): single guarded Place + copy Transitions with returning Arc.
 * - Plus (Q)Terminators (source, sink) for workflow: used for gateways/ bridges/ external transitions.
 */
public class QPlace {
  // =====

  public static boolean qExitRequested = false;

  private volatile QToken[] mInTokens = new QToken[4];
  private volatile QToken[] mOutTokens = mInTokens;
  private volatile int iInToken = 0;
  private volatile int iOutToken = 0;

  public QToken peek() {
    QToken old = null;
    int iOut = iOutToken;
    QToken[] last = null;
    while (null == old) {
      QToken[] in = mInTokens;
      QToken[] out = mOutTokens;
      int iIn = iInToken;
      if (last != out) {
        iOut = iOutToken;
      }
      if ((in == out) && (iIn == iOut)) {
        return null;
      }
      while (0 > iInToken) {
        out = mOutTokens;
        iOut = iOutToken;
      }
      iOut = (iOut < out.length) ? iOut : 0;
      for (; iOut < out.length; ++iOut) {
        old = out[iOut];
        if ((mInTokens == out) && (iOut == iInToken)) {
          return null;
        }
        if (null != old) {
          return old;
        }
      }
      if (last == out) {
        return (mInTokens != mOutTokens) ? mInTokens[0] : null;
      }
      last = out;
      iOut = 0;
    }
    return old;
  }

  /**
   * For guarded transitions, to be overridden.
   *
   * @param xToken Instance of (sub-) class.
   * @return true if the Token is acceptable.
   */
  public boolean matches(QToken xToken) {
    return true;
  }

  /** To be run on sender's thread. */
  public int push(QToken xmToken) {
    if (null == xmToken) {
      QToken[] in = mInTokens;
      QToken[] out = mOutTokens;
      int iIn = iInToken;
      int iOut = iOutToken;
      return ((in == out) && (iOut <= iIn) && (16 >= in.length)) ? (iIn - iOut)
          : ((in.length >= out.length) ? in.length : out.length);
    }
    int iNext = ((iInToken + 1) >= mInTokens.length) ? 0 : (iInToken + 1);
    if (mInTokens == mOutTokens) {
      int newSize = (iNext == iOutToken) ? mInTokens.length : -1;
      if ((0 < iOutToken) && (iOutToken < iInToken) && (iInToken < (mInTokens.length / 2)) && (16 < mInTokens.length)) {
        newSize = mInTokens.length / 2;
      }
      if (0 < newSize) {
        // First attempt, before resetting iInToken:
        mInTokens = new QToken[newSize];
        iInToken = 0;
        iNext = 1;
        if (mInTokens == mOutTokens) {
          // iOutToken suddenly passed me ... Take a fresh start after resetting iInToken: 
          mInTokens = new QToken[(16 < mInTokens.length) ? (mInTokens.length / 2) : mInTokens.length];
        }
      }
    } else if (0 >= iNext) {
      // As signal for the other thread:
      iInToken = -1;
      QToken[] old = mInTokens;
      mInTokens = new QToken[4];
      if (old == mOutTokens) {
        iInToken=0;
        iNext=1;
      }
      // Check again:
      if (old != mOutTokens) {
        mInTokens = Arrays.copyOf(old, 2*old.length);
        iInToken = old.length;
        iNext = iInToken+1;
      }
    }
    mInTokens[iInToken] = xmToken;
    iInToken = iNext;
    return 1;
  }

  /** To be run on receiver proc's thread. */
  public void flush(boolean resize) {
    // ... automatically resized:
    iOutToken = 0;
    final int iStop = iInToken;
    mOutTokens = mInTokens;
    for (int i0 = 0; (i0 < iStop) && (i0 < mOutTokens.length); ++i0) {
      mOutTokens[i0] = null;
    }
  }

  /** To be run on dispatcher's/ receiver proc's thread. */
  public QToken pull() {
    int iStop = (mInTokens == mOutTokens) ? iInToken : iOutToken;
    if (mInTokens == mOutTokens) {
      if (iStop == iOutToken) {
        return null;
      }
    }
    if ((mInTokens != mOutTokens) || (0 > iStop)) {
      final QToken old = mOutTokens[iOutToken];
      mOutTokens[iOutToken] = null;
      iStop = iOutToken;
      iOutToken += ((iOutToken + 1) < mOutTokens.length) ? 1 : -iOutToken;
      if (null != old) {
        return old;
      }
    }
    while (true) {
      iStop = ((0 <= iStop) && (iStop < mOutTokens.length)) ? iStop : 0;
      for (; iOutToken != iStop; iOutToken += ((iOutToken + 1) < mOutTokens.length) ? 1 : -iOutToken) {
        final QToken out = mOutTokens[iOutToken];
        if (null != out) {
          mOutTokens[iOutToken] = null;
          return out;
        }
      }
      if ((mInTokens == mOutTokens) && (0 <= iInToken)) {
        break;
      }
      iOutToken = 0;
      // Wait for other thread to settle its data: 
      do {
        mOutTokens = mInTokens;
      } while (0 > iInToken);
      iStop = iInToken;
    }
    return null;
  }

  // =====
}
