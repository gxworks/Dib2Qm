// Copyright (C) 2018, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.generic;

import com.gitlab.dibdib.picked.common.CdDammFunc;
import com.gitlab.dibdib.picked.common.Codata;
import java.math.*;
import net.sf.dibdib.config.Dib2Constants;
import net.sf.dibdib.thread_any.MiscFunc;

public final class BigSxg {

  public static final BigSxg VAL_0 = new BigSxg(0, "0");
  public static final BigSxg VAL_1 = new BigSxg(1, "1");
  public static final BigSxg VAL_NAN = new BigSxg(Double.NaN, null, "", 1);
  public static final BigSxg VAL_PI = null; // ...
  public static final BigSxg VAL_E = null; // ...

  private static final BigDecimal[] zDigits = new BigDecimal[65];
  private static final byte[] zDigitVals = new byte[0x80];

  /** Extra digits for pow() calculations etc. */
  private static final int PREC_DELTA = 20;

  private static final double kLogOf2 = Math.log(2.0);

  // TODO As BigInteger
  private final BigDecimal numerator;

  private final double numD4;
  private final long divisor;
  private final int exp2;
  private final int exp3;
  private final int exp5;
  private final int exp7;
  private String zString;

  // =====

  public static enum Symbol implements QIfs.QEnumIf {
    pi("\u03c0", VAL_PI),
    e("\u2107", VAL_E),
    qe("\u212f", Codata.ec),
    // As unit:
    i("\u03b9 \u2148 \u2149 j"),
    Angstrom("\u212b \u00c5"),
    Celsius("\u2103"),
    Fahrenheit("\u2109"),
    ohm("\u2126 ohm"),
    Kelvin("\u212a K"),
    Planck("\u210e h", Codata.h),
    Planck2("\u210f"), // , Codata.h..),
    ;

    public final String symbolsOrUnit;
    public final BigSxg val;
    public final double valD4;

    private Symbol(String unit) {
      symbolsOrUnit = unit;
      val = null;
      valD4 = Double.NaN;
    }

    private Symbol(String symbolsConst, double valueConst) {
      symbolsOrUnit = symbolsConst;
      val = null;
      valD4 = valueConst * Dib2Constants.INT_D4_FACT;
    }

    private Symbol(String symbolsConst, BigSxg valueConst) {
      symbolsOrUnit = symbolsConst;
      val = valueConst;
      valD4 = 0.0;
    }

    private Symbol(String symbolsConst, Codata valueConst) {
      symbolsOrUnit = symbolsConst;
      val = null;
      valD4 = valueConst.value * Dib2Constants.INT_D4_FACT;
    }

    @Override
    public long getShash() {
      throw new UnsupportedOperationException("Not supported yet.");
    }
  }

  // =====

  private BigSxg(long val, String xmOriginal) {
    divisor = 1;
    exp2 = 0;
    exp3 = 0;
    exp5 = 0;
    exp7 = 0;
    final boolean preciseD4 =
        (((Long.MIN_VALUE / Dib2Constants.INT_D4_FACT_LONG + 1) < val)
            && (val < (Long.MAX_VALUE / Dib2Constants.INT_D4_FACT_LONG - 1)));
    numerator = preciseD4 ? null : new BigDecimal(val);
    numD4 =
        preciseD4
            ? (val * Dib2Constants.INT_D4_FACT_LONG)
            : (Dib2Constants.INT_D4_FACT * (double) val);
    zString = xmOriginal;
  }

  private BigSxg(String xmOriginal, double xD4) {
    divisor = 1;
    exp2 = 0;
    exp3 = 0;
    exp5 = 0;
    exp7 = 0;
    numerator = null;
    numD4 = xD4;
    zString = xmOriginal;
  }

  private BigSxg(double xD4, BigDecimal xmNum, String xmOriginal, long xDivisor, int... exp2357) {
    divisor = xDivisor;
    exp2 = (0 < exp2357.length) ? exp2357[0] : 0;
    exp3 = (1 < exp2357.length) ? exp2357[1] : 0;
    exp5 = (2 < exp2357.length) ? exp2357[2] : 0;
    exp7 = (3 < exp2357.length) ? exp2357[3] : 0;
    numD4 = xD4;
    numerator = xmNum;
    zString = xmOriginal;
  }

  public static BigSxg create(long xVal, String xmOptRepr) {
    if ((0 == xVal) && ((null == xmOptRepr) || "0".equals(xmOptRepr))) {
      return VAL_0;
    } else if ((1 == xVal) && ((null == xmOptRepr) || "1".equals(xmOptRepr))) {
      return VAL_1;
    }
    return new BigSxg(xVal, xmOptRepr);
  }

  public static BigSxg create(String xmOptRepr, double xD4) {
    if ((0.0 == xD4) && ((null == xmOptRepr) || "0".equals(xmOptRepr))) {
      return VAL_0;
    } else if ((Dib2Constants.INT_D4_FACT == xD4)
        && ((null == xmOptRepr) || "1".equals(xmOptRepr))) {
      return VAL_1;
    }
    return new BigSxg(xmOptRepr, xD4);
  }

  public static BigSxg createRounded(double xD4) {
    return create(rxx4DoubleD4(xD4, 10));
  }

  private static void populateDigits() {
    if (null != zDigits[0]) {
      return;
    }
    for (int i0 = 0; i0 < zDigits.length; ++i0) {
      zDigits[i0] = new BigDecimal(i0);
    }
    for (byte i0 = 0; i0 < Dib2Constants.base60Chars.length; ++i0) {
      zDigitVals[Dib2Constants.base60Chars[i0]] = i0;
    }
  }

  public static BigSxg create4Radix(String xmVal, int xOffs, int xRadix) {
    populateDigits();
    if ((xRadix <= 1) || (64 < xRadix)) {
      return null;
    }
    int shiftR = 0;
    int shift10 = 0;
    int shift = 0;
    boolean neg = false;
    int to = xmVal.length();
    ///// Index of exp digits
    for (int i0 = to - 1; i0 > xOffs; --i0) {
      final char v0 = xmVal.charAt(i0);
      if ('a' <= v0) {
        if (((('a' - 10 + xRadix) <= v0)) || ('z' <= v0)) {
          to = i0;
          break;
        }
      } else if (('P' == v0) && (24 > xRadix)) {
        to = i0;
        break;
      } else if (('E' == v0) && (10 >= xRadix)) {
        to = i0;
        break;
      }
    }
    ///// Exp
    if (to < xmVal.length()) {
      for (int inx = to + 1; inx < xmVal.length(); ++inx) {
        final char v0 = xmVal.charAt(inx);
        if ('-' == v0) {
          neg = true;
          continue;
        } else if ('a' <= (v0 & 0x7f)) {
          shiftR = shiftR * xRadix + zDigitVals[(v0 & 0x7f) - ((36 > xRadix) ? 0x20 : 0)];
        } else {
          shiftR = shiftR * xRadix + zDigitVals[v0 & 0x7f];
        }
        shift10 = shift10 * 10 + (v0 & 0xf);
      }
      shift = shiftR;
      if ((24 > xRadix) && (('p' == xmVal.charAt(to)) || ('P' == xmVal.charAt(to)))) {
        shift = shift10;
      }
    }
    ///// Significand
    shiftR = 0;
    shift = neg ? (-shift) : shift;
    neg = ('-' == xmVal.charAt(0));
    BigDecimal outBig = null;
    long out = 0;
    // 'Real' normalized form?
    int iStrict = xOffs + ((xmVal.length() > xOffs) && ('-' == xmVal.charAt(xOffs)) ? 1 : 0);
    iStrict = iStrict + ((xmVal.length() > iStrict) && ('0' == xmVal.charAt(iStrict)) ? 1 : 0);
    iStrict =
        iStrict + ((xmVal.length() > iStrict) && ('.' == xmVal.charAt(iStrict)) ? 1 : -iStrict);
    for (int inx = xOffs; inx < to; ++inx) {
      final char v0 = xmVal.charAt(inx);
      if (('\'' == v0) || (':' == v0) || ('_' == v0)) {
        continue;
      } else if ('-' == v0) {
        neg = true;
        continue;
      } else if (('.' == v0) || (',' == v0)) {
        shiftR = -1;
        continue;
      }
      int digit = zDigitVals[v0 & 0x7f];
      if (('a' <= (v0 & 0x7f)) && (36 > xRadix)) {
        digit = zDigitVals[(v0 & 0x7f) - 0x20];
      }
      if (null == outBig) {
        out = out * xRadix + digit;
        // Containable as double?
        if ((((1L << 52) / Dib2Constants.INT_D4_FACT_LONG) << 3) <= out) {
          outBig = new BigDecimal(out);
          out = 0;
        }
      } else {
        outBig = outBig.multiply(zDigits[xRadix]).add(zDigits[digit]);
      }
      shift += shiftR;
    }
    if (neg) {
      out = -out;
      outBig = (null == outBig) ? null : outBig.negate();
    }
    if (0 == shift) {
      return (null == outBig)
          ? new BigSxg(xmVal, out * Dib2Constants.INT_D4_FACT)
          : new BigSxg(outBig.doubleValue() * Dib2Constants.INT_D4_FACT, outBig, xmVal, 1);
    }
    final int shiftAbs = (0 <= shift) ? shift : -shift;
    if ((null == outBig)
        && (0 >= iStrict)
        && ((100 > shiftAbs) || ((2002 / xRadix) > shiftAbs) || (0 == out))) {
      try {
        final double fact = ((0 == shift) || (0 == out)) ? 1.0 : Math.pow(xRadix, shift);
        final double d4 = out * Dib2Constants.INT_D4_FACT * fact;
        // JDK < 1.8:
        final boolean isFinite = Math.abs(d4) <= Double.MAX_VALUE;
        if (isFinite && (d4 / Dib2Constants.INT_D4_FACT / fact == out)) {
          return new BigSxg(xmVal, d4);
        }
      } catch (Exception e0) {
      }
    }
    int radix = xRadix;
    while ((0 == (radix % 10)) && (null != outBig)) {
      outBig = (0 <= shift) ? outBig.movePointRight(shift) : outBig.movePointLeft(-shift);
      radix /= 10;
      if (1 == radix) {
        return new BigSxg(outBig.doubleValue() * Dib2Constants.INT_D4_FACT, outBig, xmVal, 1);
      }
    }
    int exp2 = 0;
    int exp3 = 0;
    int exp5 = 0;
    int exp7 = 0;
    long div = 1;
    while ((0 == (radix & 1)) && (null == outBig)) {
      exp2 += shift;
      radix >>>= 1;
    }
    while (0 == (radix % 7)) {
      exp7 += shift;
      radix /= 7;
    }
    while (0 == (radix % 5)) {
      exp5 += shift;
      radix /= 5;
    }
    while (0 == (radix % 3)) {
      exp3 += shift;
      radix /= 3;
    }
    while ((1 < radix) && (Integer.MAX_VALUE > div) && (0 > shift)) {
      ++shift;
      div *= radix;
    }
    if ((0 == shift) || (1 >= radix)) {
      return new BigSxg(
          ((null == outBig) ? out : outBig.doubleValue()) * Dib2Constants.INT_D4_FACT,
          outBig,
          xmVal,
          1,
          exp2,
          exp3,
          exp5,
          exp7);
    }
    if (null == outBig) {
      // Leave precision handling to BigDecimal:
      outBig = new BigDecimal(out);
    }
    final MathContext mc =
        new java.math.MathContext(outBig.precision() + PREC_DELTA, RoundingMode.HALF_EVEN);
    outBig = outBig.multiply(zDigits[xRadix].pow(shift, mc), mc);
    return new BigSxg(
        outBig.doubleValue() * Dib2Constants.INT_D4_FACT, outBig, xmVal, 1, exp2, exp3, exp5, exp7);
  }

  /** Apart from '0x', '0z' etc., leading zeroes are ignored! No 'octal' for leading '0'. */
  public static BigSxg create(String xmVal) {
    if (2 > xmVal.length()) {
      return ((0 < xmVal.length()) && ('0' <= xmVal.charAt(0) && (xmVal.charAt(0) <= '9')))
          ? create(xmVal.charAt(0) & 0xf, xmVal)
          : VAL_NAN;
    }
    final int offs = ('-' == xmVal.charAt(0)) ? 1 : 0;
    if (('0' != xmVal.charAt(offs))
        || ((offs + 2) > xmVal.length())
        || (('0' <= xmVal.charAt(offs + 1)) && ('9' >= xmVal.charAt(offs + 1)))) {
      return create4Radix(xmVal, 0, 10);
    } else if ('#' == xmVal.charAt(offs + 1)) {
      final long out = bits4SxgChecked64(xmVal.substring(2 + offs), false);
      return create((0 == offs) ? out : (-out), xmVal);
    }
    int radix = radix4Marker(xmVal.substring(offs));
    if ((2 > radix) || (10 == radix)) {
      return create4Radix(xmVal, offs, 10);
    }
    return create4Radix(xmVal, offs + 2, radix);
  }

  public static int radix4Marker(String xNum) {
    if ((2 > xNum.length()) || ('0' != xNum.charAt(0))) {
      return 0;
    }
    switch (xNum.charAt(1)) {
      case 'b':
      case 'B':
        return 2;
      case 'c':
        return 10;
      case 'd':
      case 'D':
        return 12;
      case 'e':
      case 'E':
        return 10;
      case 'o':
        return 8;
      case 'x':
      case 'X':
        return 16;
      case 'y':
        return 30;
      case 'z':
        return 60;
      case '#': // Checked
        return 0;
      default:
        ;
    }
    return 10;
  }

  public static String marker4Radix(int radix) {
    switch (radix) {
      case 60:
        return "0z";
      case 16:
        return "0x";
      case 12:
        return "0d";
      case 10:
        return "";
      case 8:
        return "0o";
      case 2:
        return "0b";
      default:
        ;
    }
    return "0~" + radix + "~";
  }

  @Override
  public String toString() {
    // TODO
    return ""
        + ((null == numerator)
            ? ("" + ((numD4 == (long) numD4) ? ((long) numD4 * Dib2Constants.INT_D4_F_INV) : numD4))
            : numerator.toString())
        + "e...[d4="
        + numD4
        + ", shifted="
        + numerator
        + ", divisor="
        + divisor
        + ", original="
        + zString
        + " "
        + exp2
        + " "
        + exp3
        + " "
        + exp5
        + " "
        + exp7
        + "]";
  }

  public String toString(int radix) {
    if (10 == radix) {
      return toString();
    }
    // TODO
    if (16 != radix) {
      return "...(" + radix + "):" + toString();
    }
    return "0x..."
        + Double.toHexString(
            (null == numerator)
                ? (numD4 * Dib2Constants.INT_D4_F_INV)
                : (double) numerator.longValue())
        + "...";
  }

  public double toDoubleD4() {
    // JDK < 1.8:
    final boolean isFinite = Math.abs(numD4) <= Double.MAX_VALUE;
    if (!isFinite) {
      return Double.NaN;
    }
    double out = numD4;
    try {
      if (0 != exp2) {
        if (exp5 == exp2) {
          // Decimal ...
          out *= Math.pow(10, exp2);
        } else if ((-50 <= exp2) && (exp2 < 0)) {
          out /= (1L << (-exp2));
        } else if ((-50 >= exp2) && (exp2 > 0)) {
          out *= (1L << exp2);
        } else {
          out *= Math.pow(2, exp2);
        }
      }
      if (0 != exp3) {
        out *= Math.pow(3, exp3);
      }
      if ((0 != exp5) && (exp2 != exp5)) {
        out *= Math.pow(5, exp5);
      }
      if (0 != exp7) {
        out *= Math.pow(7, exp7);
      }
      if (1 < divisor) {
        out /= divisor;
      }
    } catch (Exception e0) {
      return Double.NaN;
    }
    return out;
  }

  public long toLong() {
    final boolean isFinite = Math.abs(numD4) <= Double.MAX_VALUE;
    if (!isFinite) {
      return (1L << 63);
    }
    try {
      if (null != numerator) {
        long ve = numerator.longValueExact();
        return ve;
      } else {
        double out = toDoubleD4() * Dib2Constants.INT_D4_F_INV;
        if ((-(1L << 60) < out) && (out < (1L << 60))) {
          return (long) out;
        }
      }
    } catch (Exception e0) {
    }
    return (1L << 63);
  }

  public static String string4DoubleD4_OLD(double valD4) {
    if ((-1.0E15 < valD4) && (valD4 < 1.0E15)) {
      long tmp = (long) valD4;
      if (valD4 == tmp) {
        final boolean neg = (0 > valD4);
        tmp = neg ? -tmp : tmp;
        final int frac = (int) (tmp % Dib2Constants.INT_D4_FACT_LONG);
        tmp /= Dib2Constants.INT_D4_FACT_LONG;
        if (0 == frac) {
          return (neg ? "-" : "") + tmp;
        }
        final long fx = (frac * 10000 / Dib2Constants.INT_D4_FACT_LONG);
        final boolean prec = (fx * Dib2Constants.INT_D4_FACT_LONG) == (frac * 1000);
        final String sFrac = ("" + (10000 + fx)).substring(1).replaceFirst("0+$", "");
        return (neg ? "-" : "") + tmp + '.' + sFrac + (prec ? "" : "..");
      }
      String out = ("" + (valD4 * Dib2Constants.INT_D4_F_INV));
      final int dp = out.indexOf('.');
      if ((0 < dp) && (out.indexOf("999999") > dp) && (14 <= out.length())) {
        valD4 =
            MiscFunc.roundForRxxUsage(
                Math.nextAfter(
                    valD4, (0 <= valD4) ? Double.POSITIVE_INFINITY : Double.NEGATIVE_INFINITY));
        out = ("" + (valD4 * Dib2Constants.INT_D4_F_INV));
      }
      if ((0 < dp) && (out.indexOf("000000") > dp) && (14 <= out.length())) {
        return out.replaceFirst("000000+[0-9][0-9]$", "");
      }
      return out;
    }
    return "" + (valD4 * Dib2Constants.INT_D4_F_INV);
  }

  private static int rxxLoop60(long xVal, char[] out, int i0) {
    while (xVal >= 60) {
      final long tmp = (xVal >>> 2) / 15;
      out[i0--] = Dib2Constants.base60Chars[(int) (xVal - (tmp << 2) * 15)];
      xVal = tmp;
    }
    out[i0] = Dib2Constants.base60Chars[(int) xVal];
    return i0;
  }

  private static int rxxLoop16(long xVal, char[] out, int i0) {
    while (xVal >= 16) {
      out[i0--] = Dib2Constants.base60Chars[(int) (xVal & 0xf)];
      xVal >>>= 4;
    }
    out[i0] = Dib2Constants.base60Chars[(int) xVal];
    return i0;
  }

  private static int rxxLoop12(long xVal, char[] out, int i0) {
    while (xVal >= 12) {
      final long tmp = (xVal >>> 2) / 3;
      out[i0--] = Dib2Constants.base60Chars[(int) (xVal - (tmp << 2) * 3)];
      xVal = tmp;
    }
    out[i0] = Dib2Constants.base60Chars[(int) xVal];
    return i0;
  }

  private static int rxxLoop10(long xVal, char[] out, int i0) {
    while (xVal >= 10) {
      final long tmp = (xVal >>> 1) / 5;
      out[i0--] = Dib2Constants.base60Chars[(int) (xVal - (tmp << 1) * 5)];
      xVal = tmp;
    }
    out[i0] = Dib2Constants.base60Chars[(int) xVal];
    return i0;
  }

  private static int rxxLoop2(long xVal, char[] out, int i0) {
    while (xVal >= 2) {
      out[i0--] = Dib2Constants.base60Chars[(int) (xVal & 0x1)];
      xVal >>>= 1;
    }
    out[i0] = Dib2Constants.base60Chars[(int) xVal];
    return i0;
  }

  public static String rxx4Long(long xVal, final int radix) {
    char[] out = new char[65];
    int i0 = 64;
    boolean neg = (xVal < 0);
    if (neg) {
      xVal = -xVal;
    }
    if (60 == radix) {
      i0 = rxxLoop60(xVal, out, i0);
    } else if (16 == radix) {
      i0 = rxxLoop16(xVal, out, i0);
    } else if (12 == radix) {
      i0 = rxxLoop12(xVal, out, i0);
    } else if (10 == radix) {
      i0 = rxxLoop10(xVal, out, i0);
    } else if (2 == radix) {
      i0 = rxxLoop2(xVal, out, i0);
    } else {
      while (xVal >= radix) {
        out[i0--] = Dib2Constants.base60Chars[(int) (xVal % radix)];
        xVal = xVal / radix;
      }
      out[i0] = Dib2Constants.base60Chars[(int) xVal];
    }
    if (neg) {
      out[--i0] = '-';
    }
    return new String(out, i0, (65 - i0));
  }

  public static String rxxFraction4DoublePos_OLD(double xVal, int radix) {
    if (0.0 >= xVal) {
      return "";
    }
    final long v0 = (long) Math.nextUp(Math.nextUp(xVal));
    double frac = xVal - v0;
    if (v0 > xVal) {
      return "+1";
    }
    if (frac <= ((long) (xVal + 2.0) / (1L << 50))) {
      return "";
    }
    char[] out = new char[20];
    int count = 0;
    out[count++] = '.';
    while ((0.0 < frac) && (count < 20)) {
      frac *= radix;
      final int digit = (int) frac;
      out[count++] = Dib2Constants.base60Chars[digit];
      frac -= digit;
    }
    return (1 >= count) ? ".0" : new String(out, 0, count);
  }

  public static String rxxFraction4DoublePos(double xVal, long optDiv, int radix, int xDigits) {
    optDiv = (1 < optDiv) ? optDiv : 1;
    if (((1.0 / (1L << 50)) >= xVal) || (0 == xDigits) || (2 > radix) || (optDiv < xVal)) {
      return (((1.0 / (1L << 50)) >= xVal) || (0 == xDigits)) ? "" : "*";
    }
    long sig = 0;
    int digits = xDigits;
    if (0 > digits) {
      double fx = xVal;
      for (digits = 0; fx != (long) fx; fx *= radix) {
        ++digits;
      }
      sig = (long) (0.5 + ((1 >= optDiv) ? fx : (fx / optDiv)));
    } else if (1 >= optDiv) {
      sig = (long) (0.5 + Math.pow(radix, digits) * xVal);
    } else {
      sig = (long) (0.5 + Math.pow(radix, digits) * xVal / optDiv);
    }
    char[] out = new char[digits + 2];
    int inx = out.length;
    int end = inx;
    for (; digits > 0; --digits) {
      final int dig = (int) (sig % radix);
      sig /= radix;
      out[--inx] = Dib2Constants.base60Chars[dig];
      if (('0' == out[inx]) && (inx >= (end - 1))) {
        --end;
      }
    }
    if (0 != sig) {
      inx = end = out.length;
      for (digits = xDigits; digits > 0; --digits) {
        out[--inx] = Dib2Constants.base60Chars[radix - 1];
      }
    } else if (inx >= end) {
      return "";
    }
    out[--inx] = '.';
    return new String(out, inx, end - inx);
  }

  public static String rxxFraction4D4(double fracD4) {
    return (0 > fracD4)
        ? ".0-"
        : rxxFraction4DoublePos(fracD4, Dib2Constants.INT_D4_FACT_LONG, 10, 10);
  }

  public static String rxxFraction4D4_OLD(double fracD4) {
    char[] out = new char[20];
    int inx = out.length;
    int end = inx;
    if ((0.0 > fracD4) || (Dib2Constants.INT_D4_FACT <= fracD4)) {
      return "*";
    }
    double frame = 1.0E10;
    long frac = (long) (0.5 + fracD4 * frame * Dib2Constants.INT_D4_F_INV);
    while (frac >= frame) {
      --frac;
    }
    for (; frame > 1.1; frame *= 0.1) {
      out[--inx] = Dib2Constants.base60Chars[(int) (frac % 10)];
      frac /= 10;
      if (('0' == out[inx]) && (inx >= (end - 1))) {
        --end;
      }
    }
    if (inx >= end) {
      return "";
    }
    out[--inx] = '.';
    return new String(out, inx, end - inx);
  }

  private static String rxx4DoublePos(double xVal, final int radix) {
    final double MIN_NORMAL = 2.2250738585072014E-308;
    if ((xVal == 0.0) || (MIN_NORMAL >= xVal)) {
      return ((xVal == 0.0) ? "0" : ".0");
    }
    int exp2 = (((1 << 11) - 1) & (int) (Double.doubleToRawLongBits(xVal) >>> 52)) - 1022 - 53;
    if (exp2 >= (1010 - 53)) {
      // Avoid tricky extras for the time being ...
      return "" + Double.POSITIVE_INFINITY;
    }
    int exp = 0;
    long signific = (Double.doubleToLongBits(xVal) & 0x000FFFFFFFFFFFFFL) | 0x0010000000000000L;
    // double check = signific * Math.pow(2, exp2);
    // if (check != xVal) {
    //  return "?";
    // }
    if (2 == radix) {
      exp = exp2;
    } else if ((0 < exp2) && (exp2 < (63 - 53))) {
      signific <<= exp2;
    } else if ((-53 < exp2) && (exp2 <= 0) && ((signific >>> (-exp2)) == xVal)) {
      signific >>>= (-exp2);
    } else {
      exp = (int) (kLogOf2 / Math.log(radix) * exp2);
      double pexp = Math.pow(radix, exp);
      final double p2 = Math.pow(2, exp2);
      while (pexp > p2) {
        --exp;
        pexp = Math.pow(radix, exp);
      }
      double signifval = signific * (p2 / pexp);
      signific = (long) signifval;
      final double fact = Math.pow(radix, exp);
      final double cmp = signific * fact;
      if ((cmp < xVal) && ((xVal - cmp) > (((signific + 1) * fact) - xVal))) {
        ++signific;
      } else if ((cmp > xVal) && ((cmp - xVal) > (xVal - (signific - 1) * fact))) {
        --signific;
      }
    }
    String digits = rxx4Long(signific, radix);
    int len = digits.length();
    final char charCmp = (6 >= len) ? 0 : digits.charAt(len - 2);
    if (((exp <= (-len)) || (0 < exp))
        && ((Dib2Constants.base60Chars[radix - 1] == charCmp) || ('0' == charCmp))) {
      // Potential rounding?
      final int e0 =
          ('0' == digits.charAt(len - 2))
              ? (Dib2Constants.base60Chars[0] - digits.charAt(len - 1))
              : (Dib2Constants.base60Chars[radix - 1] + 1 - digits.charAt(len - 1));
      // if ((-radix / 2 <= e0) && (e0 <= (radix / 2))) {
      signific += e0;
      for (int i0 = len - 3; i0 >= (len - 6); --i0) {
        if (charCmp != digits.charAt(i0)) {
          signific -= e0;
          break;
        }
      }
      digits = rxx4Long(signific, radix);
      len = digits.length();
    }
    if (0 == exp) {
      return digits;
    }
    while ((1 < len) && (digits.charAt(len - 1) == '0')) {
      --len;
      ++exp;
      if (0 == exp) {
        return digits.substring(0, len);
      }
    }
    if ((0 >= exp) && ((-exp) <= (len + 3))) {
      if (len < -exp) {
        digits = "000".substring(0, -exp - len) + digits.substring(0, len);
        len = digits.length();
        exp = -len;
      }
      return digits.substring(0, len + exp) + '.' + digits.substring(len + exp, len);
    }
    return "." + digits.substring(0, len) + '~' + rxx4Long(exp + len, radix);
  }

  public static String rxx4Double(double xVal, int radix) {
    // JDK < 1.8:
    final boolean isFinite = Math.abs(xVal) <= Double.MAX_VALUE;
    if (!isFinite) {
      return Double.isNaN(xVal) ? "0aNaN" : Double.toString(xVal);
    }
    String out = rxx4DoublePos((0.0 <= xVal) ? xVal : (-xVal), radix);
    char c0 = out.charAt(0);
    switch (radix) {
      case 60:
        out = "0z" + out;
        break;
      case 10:
        out = (('.' == c0) ? ("0" + out) : out).replace('~', 'e');
        break;
      case 16:
      case 12:
      case 8:
      case 2:
        final String marker = marker4Radix(radix);
        out = marker + out.replace('~', marker.charAt(1));
        break;
      default:
        out = marker4Radix(radix) + out;
    }
    return (0.0 <= xVal) ? out : ("-" + out);
  }

  public static String rxx4DoubleD4(double valD4, int radix) {
    // JDK < 1.8:
    final boolean isFinite = Math.abs(valD4) <= Double.MAX_VALUE;
    if (!isFinite) {
      return Double.isNaN(valD4) ? "0aNaN" : Double.toString(valD4);
    }
    valD4 = MiscFunc.roundForRxxUsage(valD4);
    double out = valD4 * Dib2Constants.INT_D4_F_INV;
    final double cmp = out * Dib2Constants.INT_D4_FACT;
    if (cmp < valD4) {
      out = Math.nextUp(out);
    } else if (cmp > valD4) {
      out = Math.nextAfter(out, Double.NEGATIVE_INFINITY);
    }
    return rxx4Double(out, radix);
  }

  public static String hex4Double_OLD(double v0, String separator4Neg) {
    final boolean neg = (0.0 > v0);
    double vx = neg ? (-v0) : v0;
    String hex = Double.toHexString(vx);
    String hexx = null;
    if ((0x1.0p-16 <= vx) && (vx < 0x1.0p48)) {
      long vxx = (long) vx;
      hex = "0x" + Long.toHexString(vxx); // .toLowerCase(Locale.ROOT);
      vx -= vxx;
      if (0.0 == vx) {
        if (neg) {
          vxx = (long) v0;
          hexx = Long.toHexString(vxx & Long.MAX_VALUE); // .toLowerCase(Locale.ROOT);
          int i0 = 1;
          for (; (i0 < hexx.length()) && ('f' == hexx.charAt(i0)); ++i0) {}
          i0 -= 3;
          if (0 <= i0) {
            hexx = ".." + hexx.substring(i0);
          }
          hexx = "0x" + hexx;
        }
      } else if ((50 / 4 + 5) >= hex.length()) {
        vx *= 1L << 48;
        vxx = (long) vx;
        String hex2 = Long.toHexString(vxx); // .toLowerCase(Locale.ROOT);
        if (12 > hex2.length()) {
          hex2 = "000000000000".substring(hex2.length()) + hex2;
        }
        hex += "." + hex2; // .replaceFirst( "0+$", "" );
        int lenx = hex.length() - 1;
        while ((lenx > 0) && ('0' == hex.charAt(lenx))) {
          --lenx;
        }
        hex = hex.substring(0, lenx + 1);
      }
    } else if (0.0 == vx) {
      hex = "0x0";
    }
    if ((null == separator4Neg) || !neg || (null == hexx)) {
      return neg ? ("-" + hex) : hex;
    }
    return "-" + hex + separator4Neg + hexx;
  }

  public static double doubleD4oString_OLD(String val, double fallbackValueD4) {
    try {
      if ((0 < val.indexOf('x')) || (0 < val.indexOf('X'))) {
        int exp = val.indexOf('p');
        exp = (0 <= exp) ? exp : val.indexOf('P');
        if (0 > exp) {
          exp = val.length();
          val += "p0";
        }
        if (0 > val.indexOf('.')) {
          val = val.substring(0, exp) + ".0" + val.substring(exp);
        }
      } else if (0 > val.indexOf('.')) {
        int exp = val.indexOf('e');
        exp = (0 <= exp) ? exp : val.indexOf('E');
        exp = (0 <= exp) ? exp : val.length();
        val = val.substring(0, exp) + ".0" + val.substring(exp);
      }
      return MiscFunc.roundForRxxUsage(Double.parseDouble(val) * Dib2Constants.INT_D4_FACT);
    } catch (Exception e) {
    }
    return fallbackValueD4;
  }

  public static double doubleD4oString(String xVal, double fallbackValueD4) {
    try {
      BigSxg val = create(xVal);
      double out = val.toDoubleD4();
      // JDK < 1.8:
      final boolean isFinite = Math.abs(out) <= Double.MAX_VALUE;
      if (isFinite) {
        return out;
      }
    } catch (Exception e) {
    }
    return fallbackValueD4;
  }

  public static long long4String(String xVal, long defaultValue) {
    try {
      if (xVal.startsWith("0")) {
        BigSxg val = create(xVal);
        long out = val.toLong();
        return ((1L << 63) == out) ? defaultValue : out;
      }
      return Long.parseLong(xVal);
    } catch (Exception e) {
    }
    return defaultValue;
  }

  public static long bits4SxgChecked64(String sxc, boolean strict) {
    final String sx = ("000000000000" + sxc).substring(sxc.length());
    final long p1 = bits4sxg(sx.substring(0, 10));
    final long p2 = bits4sxg(sx.substring(10));
    final long p1low = ((0xffffffffL & p1) * 3600);
    final long lowx = p1low + p2;
    final long high = (p1 >>> 32) * 3600 + (lowx >>> 32);
    final long chk = lowx & 0x3f;
    final long val = (high << (32 - 6)) | ((0xffffffffL & lowx) >>> 6);
    if (chk != (0x19 ^ CdDammFunc.check64(val))) {
      return strict ? 0 : ((val << 6) | chk);
    }
    return val;
  }

  /** For Z'...' or 0#... Length <= 12 for 8 bytes. */
  public static String sxgChecked64(long bits) {
    final int cd = 0x19 ^ CdDammFunc.check64(bits);
    final int high = (int) (bits >>> (64 - 7));
    final long low = ((bits << 7) >>> 1);
    // 6 + 1 + 63 bits:
    final char[] out = new char[12];
    int ix = sxg4Bits(high, (low | cd), out);
    return new String(out, ix, out.length - ix);
  }

  public static long bits4SxgFlipped64(String idHumanReadable, boolean strict) {
    if (12 > idHumanReadable.length()) {
      return strict ? 0 : bits4sxg(idHumanReadable);
    }
    final long p1 = bits4sxg(idHumanReadable.substring(0, 10));
    final long p2 = bits4sxg(idHumanReadable.substring(10));
    final long p1low = ((0xffffffffL & p1) * 3600);
    final long lowx = p1low + p2;
    final long high = (p1 >>> 32) * 3600 + (lowx >>> 32);
    final long chk = ((high >>> 32) & 0x30L) | ((lowx >>> 32) & 0xfL);
    final long val =
        ((((0xffffffffL & (high >>> 4)) ^ 0xffffL) << 32) | ((0xffffffffL & lowx) ^ 0xffffL));
    if (strict && (chk != (0x3f & ~CdDammFunc.check64(val)))) {
      return 0;
    }
    return val;
  }

  /** Length 12 for 8 bytes. */
  public static String sxgFlipped64(long bits) {
    ///// Some bits flipped to compensate for leading 0s etc.
    int x = 0x3f & ~CdDammFunc.check64(bits);
    final int p0 = 0xffff ^ (int) bits;
    final long p1 = (bits >>> 32) ^ 0xffffL;
    // No sign bit:
    final long b = (x & 0xfL) << 32 | ((p1 << 37) >>> 1) | (p0 & 0xffffffffL);
    // 6 + 1 + 63 bits:
    final char[] out = new char[12];
    int ix = sxg4Bits((int) (((x >>> 4) << 5) | (p1 >>> 27)), b, out);
    for (--ix; ix >= 0; --ix) {
      out[ix] = '0';
    }
    return new String(out);
  }

  public static int highx4sxg(String sxg) {
    final int len = sxg.length();
    if (10 >= len) {
      return 0;
    }
    final int j0 = (((0 < sxg.length()) && ('-' == sxg.charAt(0)))) ? 1 : 0;
    final long p1 = bits4sxg(sxg.substring(j0, len - 2));
    final long p2 = bits4sxg(sxg.substring(len - 2));
    final long p1low = ((0xffffffffL & p1) * 3600);
    final long lowx = p1low + p2;
    final int highx = (int) (((p1 >>> 32) * 3600 + (lowx >>> 32)) >>> 31);
    return (0 >= j0) ? highx : -highx;
  }

  /** Without leading '0z', possibly negative if original value has up to 64 bits. */
  public static long bits4sxg(String sxg) {
    final int len = sxg.length();
    if (0 >= len) {
      return 0;
    }
    final int j0 = ('-' == sxg.charAt(0)) ? 1 : 0;
    if ((10 + j0) >= len) {
      return bits4sxgPart(sxg);
    }
    final long p1 = bits4sxgPart(sxg.substring(j0, len - 2));
    final long p2 = bits4sxgPart(sxg.substring(len - 2));
    final long p1low = ((0xffffffffL & p1) * 3600);
    final long lowx = p1low + p2;
    final long high = (p1 >>> 32) * 3600 + (lowx >>> 32);
    final long val = ((0x7fffffffL & high) << 32) | (0xffffffffL & lowx);
    return (0 >= j0) ? val : ((0 == val) ? (1L << 63) : -val);
  }

  /** With leading 0z. */
  public static String sxg4Long(long val) {
    final char[] out = new char[12];
    final int j0 = sxg4Bits(0, (0 <= val) ? val : -val, out);
    return ((0 <= val) ? "0z" : "-0z") + new String(out, j0, out.length - j0);
  }

  /** Sexagesimal for up to 70 bits, requires 12/13 bytes: n = a<<63 | b. */
  public static int sxg4Bits(int a, long b, char[] ySxg) {
    a = (0 >= a) ? 0 : a;
    final long p = (0 <= b) ? b : -b; // ((0 >= a) ? -b : (b << 1) >>> 1);
    // c: n / 60^4 = (a<<63 + b) / 2^8 / 15^4.
    long c = (((long) a << 55) | (p >>> 8)) / (15 * 15 * 15 * 15);
    // d: (n - c * 60^4) / 2 = (a<<63 - c * 60^4 + b) >>> 1.
    int d = (int) (((((long) a << 55) - c * 15L * 15 * 15 * 15) << 7) + (p >>> 1));
    // n: c * 60^4 + 2*d + (1&b).
    int inx = ySxg.length;
    ySxg[--inx] = Dib2Constants.base60Chars[((d % 30) << 1) | (1 & (int) p)];
    d /= 30;
    if (0 == c) {
      c = d;
    } else {
      ySxg[--inx] = Dib2Constants.base60Chars[d % 60];
      d /= 60;
      ySxg[--inx] = Dib2Constants.base60Chars[d % 60];
      d /= 60;
      ySxg[--inx] = Dib2Constants.base60Chars[d % 60];
    }
    while (c > 0) {
      ySxg[--inx] = Dib2Constants.base60Chars[(int) (c % 60)];
      c /= 60;
    }
    if (0 > b) {
      ySxg[--inx] = '-';
    }
    return inx;
  }

  private static byte[] bits4sxg_base = null;

  public static long bits4sxgPart(String sxg) {
    if (null == bits4sxg_base) {
      final byte[] tmp = new byte[0x80];
      for (int i0 = Dib2Constants.base60Chars.length - 1; i0 >= 0; --i0) {
        tmp[Dib2Constants.base60Chars[i0]] = (byte) i0;
      }
      // Atomic:
      bits4sxg_base = tmp;
    }
    long out = 0;
    final boolean neg = ((0 < sxg.length()) && ('-' == sxg.charAt(0)));
    for (int i0 = neg ? 1 : 0; i0 < sxg.length(); ++i0) {
      out = out * 60 + bits4sxg_base[0x7f & sxg.charAt(i0)];
    }
    return neg ? -out : out;
  }
}
