// Copyright (C) 2016, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.generic;

import net.sf.dibdib.config.Dib2Constants;
import net.sf.dibdib.thread_any.*;

/** 'Word' can be both, part of a Sequence and itself a Sequence. */
public final class QWord extends QSeq implements QIfs.QWordIf {

  public static final QWord V_NULL = new QWord("(WIP/NULL)", 0);
  public static final QWord V_BLANK = new QWord(" ", ShashFunc.shashBits4PunctFS(" ") & ~1L);
  public static final QWord V_0 = new QWord(0L);
  public static final QWord V_1 = new QWord(1L);
  public static final QWord NEG_1 = new QWord(-1L);
  public static final QWord NaN = new QWord(Double.NaN, ShashFunc.shashBits4DoubleD4(null, Double.NaN) & ~1L);
  public static final QWord FALSE = V_0;
  public static final QWord TRUE = NEG_1;
  public static final QWord V_DOT = new QWord(".", ShashFunc.shashBits4PunctFS(".") & ~1L);
  public static final QWord V_WILD = new QWord("*", ShashFunc.shashBits4PunctFS("*") & ~1L);

  /** Numeric value times primes (if applicable) */
  public double qValD4;

  /** Integer value (if applicable) */
  public long qValLong;

  protected QWord(String original, long stamp) {
    super(null, stamp);
    zString = original;
    qValD4 = Double.NaN;
  }

  protected QWord(long numeric) {
    super(null, ShashFunc.shashBits4DoubleD4(null, numeric * Dib2Constants.INT_D4_FACT_LONG) & ~1L);
    qValLong = numeric;
    qValD4 = numeric * Dib2Constants.INT_D4_FACT;
  }

  protected QWord(double numericD4, long stamp) {
    super(null, stamp);
    qValLong = 0;
    qValD4 = numericD4;
  }

  protected QWord(String original, double numericD4, long stamp) {
    super(null, stamp);
    //shash = stamp;
    zString = original;
    qValLong = 0;
    qValD4 = numericD4;
  }

  public static QWord createQWord(String original, boolean forceLiteral) {
    if (1 == original.length()) {
      switch (original.charAt(0)) {
        case ' ':
          return V_BLANK;
        case '*':
          return V_WILD;
        case '.':
          return V_DOT;
        case '0':
          if (!forceLiteral)
            return V_0;
        case '1':
          if (!forceLiteral)
            return V_1;
        default:
          ;
      }
    }
    return new QWord(original, ShashFunc.shashBits4String(original, forceLiteral));
  }

  public static QWord createQWordAscii(String ascii) {
    if (1 == ascii.length()) {
      return createQWord(ascii, true);
    }
    return new QWord(ascii, ShashFunc.shashBits4Ansi(ascii) & ~1L);
  }

  public static QWord createQWordPunct(String punct) {
    if (".".equals(punct)) {
      return V_DOT;
    }
    return new QWord(punct, ShashFunc.shashBits4PunctFS(punct) & ~1L);
  }

  public static QWord createQWordDate(String date) {
    return new QWord(date, DateFunc.hash62oDate(date) & ~1L);
  }

  /** Note: tick references shifted ignoring sign => check for delta > 50y. */
  public static QWord createQWordDateOrTickRef(long hash62OrTick2) {
    return new QWord(null, hash62OrTick2);
  }

  public static QWord createQWordDate(double eraTicks) {
    return new QWord(null, (DateFunc.hash62oEraTicks(eraTicks)));
  }

  public static QWord createQWordD4(double valueD4) {
    if (0.0 == valueD4) {
      return V_0;
    } else if (1.0 * Dib2Constants.INT_D4_FACT == valueD4) {
      return V_1;
    } else if (-1.0 * Dib2Constants.INT_D4_FACT == valueD4) {
      return NEG_1;
    } else if (Double.isNaN(valueD4)) {
      return NaN;
    }
    return new QWord(valueD4, ShashFunc.shashBits4DoubleD4(null, valueD4) & ~1L);
  }

  public static QWord createQWordD4(String original, double valueD4) {
    final QWord out = createQWordD4(valueD4);
    if (original.equals(out.toString())) {
      return out;
    }
    return new QWord(original, valueD4, ShashFunc.shashBits4DoubleD4(null, valueD4) & ~1L);
  }

  public static QWord createQWord(double value) {
    return createQWordD4(value * Dib2Constants.INT_D4_FACT);
  }

  public static QWord createQWordInt(long value) {
    if (0 == value) {
      return V_0;
    } else if (1 == value) {
      return V_1;
    } else if (-1L == value) {
      return NEG_1;
    }
    return new QWord(value);
  }

  public static QWord createQWordNumeric(String num) {
    if ((2 < num.length()) && (('9' < num.charAt(1)) || ('9' < num.charAt(2)))) {
      final int offs = ('-' == num.charAt(0)) ? 1 : 0;
      if (('0' == num.charAt(offs))
          && (('z' == num.charAt(offs + 1)) || ('#' == num.charAt(offs + 1)))) {
        if (num.matches("-?0.[0-9A-Za-z]+")) {
          final long val = BigSxg.long4String(num, 0);
          if (0 != val) {
            return createQWordInt(val);
          }
        }
      }
    }
    return new QWord(
        num, BigSxg.doubleD4oString(num, 0), ShashFunc.shashBits4DoubleD4(num, 0) & ~1L);
  }

  public static QWord createSpecial(String original, long shash) {
    return new QWord(original, shash);
  }

  @Override
  public String toString() {
    if (null == zString) {
      try {
        if (0 != qValLong) {
          if ((-1000 <= qValLong) && (qValLong < 1000)) {
            zString = "" + qValLong;
          } else {
            // zString = (0 <= qValLong) ? ("0x" + Long.toHexString(qValLong)) : ("-0x" +
            // Long.toHexString(-qValLong));
            zString = BigSxg.sxg4Long(qValLong);
          }
        } else if (isNumeric()) {
          zString = BigSxg.rxx4DoubleD4(qValD4, 10);
        } else {
          zString = ShashFunc.string4ShashBits(shash);
        }
      } catch (Exception e0) {
        zString = "0aNaN";
      }
    }
    return zString;
  }

  public String toBase(int xBase) {
    if (isDate()) {
      return BigSxg.rxx4Double(DateFunc.eraTicks4Hash62(shash), xBase);
    } else if (!isNumeric()) {
      return StringFunc.hexUtf8(toString(), true);
    }
    return ((0 == qValD4)
        ? "0/F"
        : ((-1 * Dib2Constants.INT_D4_FACT == qValD4)
            ? "-1/T"
            : BigSxg.rxx4DoubleD4(qValD4, xBase)));
  }

  public double d4() {
    return qValD4;
  }

  public long i64() {
    return qValLong;
  }

  public boolean isDate() {
    return ShashFunc.isDate(shash);
  }

  public boolean isNumeric() {
    return !Double.isNaN(qValD4);
  }
}
