// Copyright (C) 2016, 2021  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.generic;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import net.sf.dibdib.thread_any.StringFunc;

// TODO ...

/** Strings/ numeric/ date values for specific optimizations, may be replaced by simple 'long'. */
// Represents the primitive data types (atomic values); class as wrapper for debugging ...!
// May be replaced in source by: / * h * / l o n g ,
// except for: / * o * / Q W o r d replace 'QWord.asQWord(' by: / * h * / (
// Shash as sortable hash, used for partially sortable handles.
public /*final*/ class /*o*/ QWordStatic { // extends QObject_OLD {

  public long stamp;

  private String zString;

  /////

  private static final int BOX_SHIFT = 12;
  private static final int BOX_SIZE = 1 << BOX_SHIFT;
  private static final long BOX_MASK = (1 << BOX_SHIFT) - 1;
  // private static final String MARKER_DELETED = "" + (char) 1;

  private static final int BOX_COMBINED_BITS__24 = 2 * BOX_SHIFT;
  private static final long BOX_COMBINED_MASK = (1 << BOX_COMBINED_BITS__24) - 1;
  private static final long BOX_MARKER_BIT = 1L;

  /////

  private static AtomicInteger mcPairs = new AtomicInteger(0);
  /** Pairs of shashes (byte[]) and words (String). */
  private static Object[][] mBoxes = new Object[1][BOX_SIZE];

  private static char[] zColl64Char4Key = null;
  private static char[] zColl64CharUpper4Key;
  private static byte[] zColl64Key4Char;

  // ...0 => shash carries value, ...1 => shash has partial value
  // TODO  ==>  00.. date/functional (00000001: functional), 01/10 float, 11 string collation

  // Shash markers:
  // 0001 functional: .1.+: delimiter/ separator (punctuation etc.), 00.+: structural
  // 0010 date
  // 0011 RFU float
  // 01.  neg. float (bits toggled + 1)
  // 1000: 0
  // 10.  pos. float
  // 1100: NaN/ error
  // 1100 RFU float
  // 1101 number, id
  // 1110: ""
  // 1110 string collation

  public static final byte SHASH_FUNC = 1;
  public static final byte SHASH_DATE = 2;
  public static final byte SHASH_NUM_START = 3;
  public static final byte SHASH_NUM_NAN = 0xc;
  public static final byte SHASH_NUM_END = 0xd;
  public static final byte SHASH_ID = 0xd;
  public static final byte SHASH_LIT = 0xe;

  private static final byte SHASH_X = 0xf;

  public static final byte SHASHBYTE_BITS = 6;
  public static final byte SHASHBYTE_AS_CHAR = (byte) (1 << SHASHBYTE_BITS);

  private static final byte SHASHBYTE_EMPTY = SHASH_FUNC;
  private static final byte SHASHBYTE_ERROR = SHASH_NUM_NAN;
  private static final byte SHASHBYTE_TBD = SHASH_X;

  // Collation handle, last bits:
  //  0: value == handle2Double()/Str() (lowercase/num)
  //  2: RFU
  //  4: value == handle2Str()/ 1st uppercase
  //  6: value == handle2Str()/ all uppercase

  public static final int HANDLE_CAPITALIZED = 4;
  public static final int HANDLE_CAPS_ALL = 6;

  public static final QWordStatic BLANK =
      QWordStatic.asQWord(((long) SHASH_FUNC << 60) | (1L << 54));
  public static final QWordStatic EMPTY = new QWordStatic(SHASHBYTE_EMPTY);
  public static final QWordStatic ERROR = new QWordStatic(SHASHBYTE_ERROR);
  public static final QWordStatic TBD = new QWordStatic(SHASHBYTE_TBD);

  private static final byte[] EMPTY_SHASH = new byte[] {SHASHBYTE_EMPTY};
  private static final byte[] ERROR_SHASH = new byte[] {SHASHBYTE_ERROR};
  private static final byte[] TBD_SHASH = new byte[] {SHASHBYTE_TBD};

  private static final String EMPTY_STR = "";
  private static final String ERROR_STR = "(^ERROR)";
  private static final String TBD_STR = "(^TBD)";

  /////

  private static void getCollArrays() {
    if (null == zColl64Char4Key) {
      Object[] aa = StringFunc.getCollArrays();
      zColl64Char4Key = (char[]) aa[0];
      zColl64CharUpper4Key = (char[]) aa[1];
      zColl64Key4Char = (byte[]) aa[2];
    }
  }

  /////

  protected QWordStatic() {}

  private /*o*/ QWordStatic(long handle) {
    this.stamp = handle;
  }

  public static QWordStatic create() {
    // ?TODO recycle?
    return new QWordStatic();
  }

  //  private static QWord create(byte xMarker) {
  //    return asQWord((long) xMarker << 60);
  //  }

  public static /*o*/ QWordStatic asQWord(long handle) {
    return new /*o*/ QWordStatic(handle);
  }

  public static long asQWord(/*o*/ QWordStatic val) {
    return val.stamp;
  }

  /** Only for debugging! */
  @Override
  public String toString() {
    if (null == zString) {
      // Need 'QWord.asQWord' for optimization via search/replace --> /*h*/(...)
      zString = string4QWord(QWordStatic.asQWord(stamp));
    }
    return zString;
  }

  /** Only for debugging! */
  @Override
  public boolean equals(Object obj) {
    if (obj instanceof QWordStatic) {
      return stamp == ((QWordStatic) obj).stamp;
    } else if (obj instanceof Long) {
      return stamp == (Long) obj;
    }
    return false;
  }

  /** Only for debugging! */
  @Override
  public int hashCode() {
    return 31 + (int) (stamp ^ (stamp >>> 32));
  }

  public static boolean isSememe(QWordStatic xHandle) {
    // Need 'QWord.asQWord' for optimization via search/replace --> /*h*/(...)
    long handle = QWordStatic.asQWord(xHandle);
    if (0 == (handle << 4)) {
      return (handle >>> 60) != SHASH_NUM_NAN;
    }
    return (handle >>> 60) >= SHASH_DATE;
  }

  public static QWordStatic createAsciiShort(String xAscii) {
    long out = (long) SHASH_LIT << 60;
    int i0 = 0;
    for (int shift = 60 - SHASHBYTE_BITS; (shift >= 0) && (i0 < xAscii.length()); --i0) {
      out |= (xAscii.charAt(i0) & 0x1fL) << shift;
      shift -= SHASHBYTE_BITS;
    }
    return QWordStatic.asQWord(out & ~BOX_MARKER_BIT);
  }

  private static long handle4LitOrFunc(String xmLiteral, byte xMarker) {
    byte[] shash = StringFunc.coll64xBytes(xmLiteral, 1);
    shash[0] = (byte) (SHASHBYTE_AS_CHAR | xMarker);
    long handle = handleOffset(shash);
    if ((shash.length <= 11) && ((shash.length - 1) == xmLiteral.length())) {
      getCollArrays();
      int len = shash.length - 1;
      int i0 = 0;
      boolean upper = false;
      boolean first = false;
      for (; i0 < len; ++i0) {
        final char ch0 = xmLiteral.charAt(i0);
        if ((zColl64Key4Char.length > ch0)
            && (0 < zColl64Key4Char[ch0])
            && (zColl64Key4Char[ch0] < 63)) {
          if (zColl64Char4Key[shash[i0 + 1] & 0x3f] == ch0) {
            upper = false;
            continue;
          }
          if (zColl64CharUpper4Key[shash[i0 + 1] & 0x3f] == ch0) {
            upper = true;
            first = (i0 == 0);
            continue;
          }
        }
        break;
      }
      if (i0 == len) {
        if ((!upper) && (len < 10)) {
          return handle;
        }
        String try0 = string4HandleColl(handle);
        if (!upper && try0.equals(xmLiteral)) {
          return handle;
        } else if (upper && StringFunc.toUpperCase(try0).equals(xmLiteral)) {
          return handle | HANDLE_CAPS_ALL;
        } else if (first && try0.substring(1).equals(xmLiteral.substring(1))) {
          return handle | HANDLE_CAPITALIZED;
        }
      }
    }
    int ubox = allocBox(shash, xmLiteral);
    return (handle & BOX_COMBINED_MASK) | ubox | BOX_MARKER_BIT;
  }

  public static QWordStatic createLiteral(String xmLiteral) {
    // Need 'QWord.asQWord' for optimization via search/replace --> /*h*/(...)
    return QWordStatic.asQWord(handle4LitOrFunc(xmLiteral, SHASH_LIT));
  }

  public static QWordStatic createDate(String xmDate) {
    return null; // QWord.asQWord(handle4Date(xmId, SHASH_DATE));
  }

  public static QWordStatic createId(String xmId) {
    return null; // QWord.asQWord(handle4Id(xmId, SHASH_ID));
  }

  public static QWordStatic createFunctional(String xmSepOrStruc) {
    return QWordStatic.asQWord(handle4LitOrFunc(xmSepOrStruc, SHASH_FUNC));
  }

  public static QWordStatic create(String part, byte markerPart) {
    switch (markerPart) {
      case SHASH_DATE:
        return createDate(part);
      case SHASH_FUNC:
        return createFunctional(part);
      case SHASH_ID:
        return createId(part);
      case SHASH_LIT:
        return createLiteral(part);
      default:
        ;
    }
    throw new UnsupportedOperationException(
        "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
  }

  private static long handleOffset(byte[] xShash) {
    long out = (xShash[0] & 0xfL) << 60;
    int i0 = 1;
    for (int shift = 60 - SHASHBYTE_BITS; shift >= 0; shift -= SHASHBYTE_BITS) {
      if (i0 >= xShash.length) {
        break;
      }
      out |= (xShash[i0] & 0x3fL) << shift;
    }
    return out & ~BOX_MARKER_BIT;
  }

  private static String string4HandleColl(long xHandle) {
    getCollArrays();
    int len = 10;
    long val = xHandle;
    for (; (0 == (val & 0x3f)) && (len > 0); --len) {
      val >>>= SHASHBYTE_BITS;
    }
    char[] out = new char[len];
    for (--len; len >= 0; --len) {
      out[len] = zColl64Char4Key[(int) (val & 0x3f)];
      val >>>= SHASHBYTE_BITS;
    }
    return new String(out);
  }

  public static byte[] shash4QWord(QWordStatic xHandle) {
    long handle = QWordStatic.asQWord(xHandle);
    if (0 == (handle >>> 60)) {
      return ERROR_SHASH;
    }
    if (0 == (handle & BOX_MARKER_BIT)) {
      int len = 64 / SHASHBYTE_BITS + 1;
      long shifted = handle;
      for (; 0 == (shifted & BOX_MASK); --len) {
        shifted >>>= SHASHBYTE_BITS;
      }
      byte[] out = new byte[len];
      int shift = 60;
      for (int i0 = 0; i0 < len; ++i0) {
        out[i0] = (byte) (((handle >>> shift) & (SHASHBYTE_AS_CHAR - 1)) | SHASHBYTE_AS_CHAR);
        shift -= SHASHBYTE_BITS;
      }
      return out;
    }
    int box = (int) ((handle >>> BOX_SHIFT) & BOX_MASK);
    if ((mcPairs.get() << 1) <= box) {
      return ERROR_SHASH;
    }
    Object out = mBoxes[box][~1 & (int) (handle & BOX_MASK)];
    return (null == out) ? ERROR_SHASH : (byte[]) out;
  }

  public static String string4QWord(QWordStatic xHandle) {
    long handle = QWordStatic.asQWord(xHandle);
    if (0 == (handle >>> 60)) {
      return ERROR_STR;
    }
    if (0 == (handle & BOX_MARKER_BIT)) {
      String out = string4HandleColl(handle);
      if (0 != (HANDLE_CAPS_ALL & handle)) {
        return StringFunc.toUpperCase(out);
      } else if (0 != (HANDLE_CAPITALIZED & handle)) {
        return StringFunc.toUpperCase("" + out.charAt(0)) + out.substring(1);
      }
      return out;
    }
    int box = (int) ((handle >>> BOX_SHIFT) & BOX_MASK);
    if ((mcPairs.get() << 1) <= box) {
      return ERROR_STR;
    }
    Object out = mBoxes[box][1 | (int) (handle & BOX_MASK)];
    return (null == out) ? ERROR_STR : (String) out;
  }

  private static int allocBox(byte[] xmShash, String xmWord) {
    int ubox = mcPairs.getAndIncrement() << 1;
    int box = ubox >>> BOX_SHIFT;
    int inx = ubox & (int) BOX_MASK;
    if (mBoxes.length <= box) {
      if (0 == inx) {
        mBoxes = Arrays.copyOf(mBoxes, box + 1);
      } else {
        while (mBoxes.length <= box) {
          Thread.yield();
        }
      }
    }
    mBoxes[box][inx] = xmShash;
    mBoxes[box][inx | 1] = xmWord;
    return ubox;
  }
}
