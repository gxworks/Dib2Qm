// Copyright (C) 2016, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_wk;

import com.gitlab.dibdib.picked.common.ExceptionAdapter;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_wk.CcmSto.CcmTag;

/**
 * Concept mapping, saved as CSV with tabs (TSV, commas if tabs missing) as single line (trailing
 * '\n' as '\t').
 */
public final class CcmSto {
  // =====

  /** 'My' ID for exporting, internally using 0 as id to refer to myself! */
  public long mUserPid;
  /** Main ID: e-mail address etc. */
  public String mUserAddr;

  QMMap zMappingsPid;
  QMMap zMappingsLabel;

  ///// TODO Use QIfs ..., make variables local for each stack

  /** To be saved as hex literals! (with or without leading "X'") */
  public static ConcurrentHashMap<String, byte[]> qHidden = null;

  /** X/Y/Z ==> stack, T=Term, Q=Memory, L=Last, G=Graphics, E=Error */
  private ConcurrentHashMap<String, String> zVariables = null;

  public static ConcurrentHashMap<String, Long> qPid4ReferencedId = new ConcurrentHashMap<String, Long>();

  private static ConcurrentHashMap<String, String> qOldLabel4ReferencedId = new ConcurrentHashMap<String, String>();//contacts4Name.size());

  static final LinkedHashMap<Long, QSTuple> badIds = new LinkedHashMap<Long, QSTuple>();
  static final ConcurrentHashMap<String, String> chatName4OldName = new ConcurrentHashMap<String, String>();

  /////

  public static enum CcmTag implements QIfs.QTagIf {
    // ID (stored as 'stamp')
    LABEL, // NAME
    CATS,
    DAT, // DAT is last for CSV line, = DAT_* or DAT_0
    TIME, // TIME_STAMP,
    TAGSREFS,
    CNTRB, // e-mail or PID, 0 for myself, 1 for N.N.
    SRCS,
    RECV, // --> PROT
    TRASHED,
    DAT_X, // if available: DAT_1, DAT_2 etc.
    ;

    private long shash = 0;

    @Override
    public long getShash() {
      if (0 == shash) {
        shash = ShashFunc.shashBits4Ansi(name());
      }
      return shash;
    }

    public long getAsKey(QSTuple xTuple) {
      return xTuple.getAsKey(this);
    }

    public QItemIf getValue(QSTuple xTuple) {
      return xTuple.getValue(ordinal());
    }

    public QWord getWord(QSTuple xTuple) {
      QItemIf out = xTuple.getValue(ordinal());
      return (out instanceof QWord) ? (QWord) out : QWord.V_0;
    }

    @Override
    public String formatValue(QItemIf xValue) {
      switch (this) {
        case CATS:
          {
            if (xValue instanceof QWord) {
              final long flags = ((QWord) xValue).i64();
              return Cats.cats4Flags(flags);
            }
          }
        case CNTRB:
          {
            if ((xValue instanceof QWord) && ((QWord)xValue).isNumeric()) {
              final long pid = ((QWord) xValue).i64();
              return "0#" + BigSxg.sxgChecked64(pid);
            } else if (xValue instanceof QSeq) {
              return ((QSeq) xValue).toStringFull();
            } else if (null != xValue) {
              return xValue.toString();
            }
          }
        case TAGSREFS:
          {
            if ((xValue instanceof QSeq) && (0 < ((QSeq) xValue).size())) {
              StringBuilder out = new StringBuilder(10 * ((QSeq) xValue).size());
              boolean filler = false;
              for (QWordIf wd : ((QSeq) xValue).atoms()) {
                if (filler) {
                  out.append(' ');
                } else {
                  filler = true;
                }
                if ((wd instanceof QWord) && ((QWord) wd).isDate()) {
                  final long stamp = ((QWord) wd).i64();
                  out.append("0#" + BigSxg.sxgChecked64(stamp));
                } else {
                  out.append(wd.toString());
                }
              }
              return out.toString();
            }
          }
        default:
          ;
      }
      return xValue.toString();
    }

    private static String tsvFieldNames_z = null;

    public static String tsvFieldNames() {
      if (null == tsvFieldNames_z) {
        StringBuilder fieldNames = new StringBuilder(100);
        for (int i0 = 0; i0 < DAT_X.ordinal(); ++i0) {
          CcmTag field = CcmTag.values()[i0];
          if (field == DAT) {
            continue;
          }
          fieldNames.append('\t');
          fieldNames.append(field);
        }
        fieldNames.append("\tDAT\tDAT1\tDAT2\tDAT3");
        tsvFieldNames_z = fieldNames.toString();
      }
      return tsvFieldNames_z;
    }
  }

  /////

  public CcmSto() {
    mUserPid = DateFunc.createId();
    mUserAddr = ".";
    qHidden = new ConcurrentHashMap<String, byte[]>();
    zVariables = new ConcurrentHashMap<String, String>();
    // Identity: PID connected to e-mail address or phone number etc.:
    // variableForceOrIndex(".", QWord.createQWord("" + BigSxg.sxgChecked64(mUserPid) + "@?",
    // true));
    qHidden.put("pub", "".getBytes(StringFunc.CHAR8));
    qHidden.put("sec", "".getBytes(StringFunc.CHAR8));
    zMappingsLabel = QMMap.create(CcmTag.LABEL);
    zMappingsPid = QMMap.create(null);
  }

  public static CcmTag mapTag(String xTag) {
    try {
      return CcmTag.valueOf(xTag);
    } catch (Exception e0) {
      // NOP
    }
    if (xTag.startsWith("NAM")) {
      return CcmTag.LABEL;
    } else if (xTag.startsWith("TIM")) {
      return CcmTag.TIME;
    } else if (xTag.startsWith("DAT")) {
      return CcmTag.DAT;
    } else if (xTag.startsWith("CON")) {
      return CcmTag.CNTRB;
    } else if (xTag.startsWith("SOU")) {
      return CcmTag.SRCS;
    } else if (xTag.startsWith("TAGS")) {
      return CcmTag.TAGSREFS;
    }
    return CcmTag.DAT_X;
  }

  public static long pid4String(String id, boolean avoidZero) {
    if (0 >= id.length()) {
      return 0;
    }
    long out = 0;
    final long stampMax = DateFunc.createId();
    String idx = null;
    char ch0 = id.charAt(0);
    if (('0' == ch0) && id.matches("0#[0-9A-NP-Za-y]+")) {
      id = id.substring(2);
      out = BigSxg.bits4SxgChecked64(id, true);
      idx = BigSxg.sxgChecked64(out);
    } else if (('0' == ch0) && id.matches("0z[0-9A-NP-Za-y]+")) {
      id = id.substring(2);
      out = BigSxg.bits4sxg(id);
      idx = BigSxg.sxg4Long(out).substring(2);
    } else if (('Z' == ch0) && id.matches("Z'[0-9A-NP-Za-y]+'")) {
      id = id.substring(2, id.length() - 1);
      out = BigSxg.bits4SxgChecked64(id, true);
      idx = BigSxg.sxgChecked64(out);
    } else if (id.matches("[0-9A-NP-Za-y]+")) {
      out = BigSxg.bits4sxg(id);
      idx = BigSxg.sxg4Long(out).substring(2);
    }
    if ((99888 > out) && (0 <= out)) {
      out = (1 < out) ? 1 : out;
    } else if (!id.equals(idx) || (0 != (out & 1L))
        || (out > stampMax)) {
      out = 0;
      if (avoidZero) {
        for (int i0 = 0; i0 < id.length(); ++i0) {
          out = (out << 5) ^ id.charAt(i0);
        }
        out = (out << 2) >>> 1;
      }
    }
    return out;
  }

  public static QSeqIf mapItem(String xItemText, CcmTag xTag) {
    xItemText = (null != xItemText) ? xItemText : "";
    // ID:
    if ((null == xTag) || (CcmTag.CNTRB == xTag)) {
      xItemText = xItemText.trim();
      if ((null != xTag)
          && ((0 < xItemText.indexOf('@')) || (0 == xItemText.indexOf('+')) || (0 < xItemText.indexOf(':')))) {
        return QWord.createQWord(xItemText, true);
      }
      long out = pid4String(xItemText, true);
      return ((0 != out) || (0 >= xItemText.length()) || (0 == xItemText.indexOf('0'))) ? QWord.createQWordInt(out)
          : QWord.createQWord(xItemText, true);
    }
    // Others:
    switch (xTag) {
      case LABEL:
        return QWord.createQWord(xItemText, false);
      case CATS:
        return QWord.createQWordInt(Cats.toFlags(xItemText));
      case DAT:
      case TRASHED:
        return QSeq.createQSeq(xItemText);
      case TIME:
        return (2 < xItemText.length()) ? QWord.createQWordDate(xItemText) : QWord.V_0;
      case TAGSREFS:
      case SRCS:
      case RECV:
        return QSeq.createQSeq(xItemText);
      default:
        ;
    }
    ExceptionAdapter.throwAdapted(new IndexOutOfBoundsException(), CcmSto.class, "" + xTag);
    return null;
  }

  static final CcmTag[] createTagMap_MAP_0 = new CcmTag[] {CcmTag.LABEL, CcmTag.CATS, CcmTag.DAT};
  static final CcmTag[] createTagMap_MAP_MAIL =
      new CcmTag[] {
        CcmTag.DAT_X,
        CcmTag.LABEL,
        CcmTag.CATS,
        CcmTag.TIME,
        CcmTag.CNTRB,
        CcmTag.SRCS,
        CcmTag.RECV,
        CcmTag.DAT
      };
  static CcmTag[] createTagMap_FULL = null;

  /** CcmTag.DAT_X == map[0] if first field has PID. */
  public static CcmTag[] createTagMap(String[] tags) {
    if ((null == tags) || (3 > tags.length)) {
      return (null == tags) ? createTagMap_MAP_MAIL : createTagMap_MAP_0;
    }
    CcmTag[] map = new CcmSto.CcmTag[tags.length];
    long bSet = 0;
    for (int ix = 0; ix < tags.length; ++ix) {
      map[ix] = CcmSto.mapTag(tags[ix]);
      if (0 != (bSet & (1L << map[ix].ordinal()))) {
        map[ix] = CcmTag.DAT_X;
      }
      bSet |= 1L << map[ix].ordinal();
    }
    return map;
  }

  public static CcmTag[] getTagMapStd(int len) {
    switch (len) {
      case 3:
        return createTagMap_MAP_0;
      case 7:
        return createTagMap_MAP_MAIL;
      default:
        ;
    }
    if (null == createTagMap_FULL) {
      CcmTag[] out = new CcmTag[CcmTag.values().length];
      System.arraycopy(CcmTag.values(), 0, out, 1, out.length - 1);
      // Atomic:
      createTagMap_FULL = out;
    }
    return ((0 >= len) || (createTagMap_FULL.length == len)) ? createTagMap_FULL : null;
  }

  public static QSTuple createTuple4Tsv(String line, CcmTag[] map) {

    // First element as PID?
    boolean bId = CcmTag.DAT_X == map[0];
    if ((line.indexOf("\t") < 0) && (line.indexOf(",") > 0)) {
      line = line.replaceAll("\"? *, *\"?", "\t");
    }
    String[] a0 = line.split("\t"); // , 6 + iOid );
    QSTuple tup = new QSTuple(CcmTag.values(), CcmTag.DAT_X.ordinal());
    tup.jEnd = tup.items.length;
    // tup.items = new QItemIf[CcmTag.DAT_X.ordinal()];
    if (2 >= a0.length) {
      if ((0 >= line.trim().length()) || (0 >= a0.length)) {
        return null;
      }
      tup.items[createTagMap_MAP_0[0].ordinal()] =
          CcmSto.mapItem(
              ((1 < a0.length) || (30 > a0[0].length())) ? a0[0] : a0[0].substring(0, 15),
              createTagMap_MAP_0[0]);
      tup.items[createTagMap_MAP_0[2].ordinal()] =
          CcmSto.mapItem((1 < a0.length) ? a0[1] : a0[0], createTagMap_MAP_0[2]);
      tup.stamp = DateFunc.currentTimeNanobisLinearized(true);
      return tup;
    }
    StringBuilder dat = new StringBuilder(line.length());
    // int iTime = -2;
    tup.stamp = 0;
    if (bId && (5 <= a0[0].trim().length())) {
      try {
        tup.stamp = pid4String(a0[0].trim(), false);
      } catch (Exception e0) {
        tup.stamp = 0;
      }
      if (0 == tup.stamp) {
        tup.items[CcmTag.TRASHED.ordinal()] = QSeq.createQSeq(":ID:" + a0[0]);
      }
    }
    boolean found = false;
    for (int ix = bId ? 1 : 0; ix < a0.length; ++ix) {
      if ((ix >= map.length) || (CcmTag.DAT_X == map[ix])) {
        dat.append('\n');
        dat.append(a0[ix]);
      } else if (CcmTag.DAT == map[ix]) {
        found = true;
        if (dat.length() > 0) {
          dat = new StringBuilder(a0[ix] + dat.toString());
        } else {
          dat.append(a0[ix]);
        }
      } else {
        tup.items[map[ix].ordinal()] = CcmSto.mapItem(a0[ix], map[ix]);
      }
    }
    if (0 == tup.stamp) {
      QSeq ackRcv = CcmSto.getQSeq(tup, CcmTag.RECV, QSeq.NIL);
      ackRcv = QSeq.createQSeq(ackRcv.toStringFull() + "\n:ID:" + a0[0]);
      tup.set(CcmTag.RECV, ackRcv);
      tup.stamp = pid4String(a0[0].trim(), true);
      if ((0 <= tup.stamp) && (tup.stamp <= 99888)) {
        tup.stamp = DateFunc.currentTimeNanobisLinearized(true);
      }
    }
    if (0 >= dat.length()) {
      if (!found) {
        dat.append(a0[a0.length - 1]);
      } else {
        dat.append("(" + a0[0] + ")");
      }
    }
    if (null == tup.items[CcmTag.LABEL.ordinal()]) {
      tup.items[CcmTag.LABEL.ordinal()] =
          QWord.createQWord(DateFunc.currentTimeNanobisLinearized(true));
    }
    if (0 < dat.length()) {
      tup.items[CcmTag.DAT.ordinal()] = QSeq.createQSeq(dat.toString());
      return tup;
    }
    return null;
  }

  private static QWord getQWord(QSTuple xTup, CcmTag xTag, QWord fallBack) {
    final QSeqIf val = xTup.getValue(xTag);
    return (val instanceof QWord) ? (QWord) val : fallBack;
  }

  private static QSeq getQSeq(QSTuple xTup, CcmTag xTag, QSeq fallBack) {
    final QSeqIf val = xTup.getValue(xTag);
    return (val instanceof QSeq) ? (QSeq) val : fallBack;
  }

  public static QSTuple createEntry(QWordIf xmLabel, long xCats, QSeqIf... xmDat) {
    final int size = CcmTag.DAT_X.ordinal() - 1 + ((0 >= xmDat.length) ? 1 : xmDat.length);
    final QSTuple out = new QSTuple(CcmTag.values(), size);
    out.stamp = DateFunc.createId();
    out.jEnd = CcmTag.DAT_X.ordinal();
    out.set(CcmTag.LABEL, xmLabel);
    out.set(CcmTag.TIME, QWord.createQWordDate(DateFunc.date4Millis()));
    out.set(CcmTag.CATS, QWord.createQWordInt(xCats));
    out.set(CcmTag.DAT, (0 < xmDat.length) ? xmDat[0] : QSeq.NIL);
    if (1 < xmDat.length) {
      System.arraycopy(xmDat, 1, out.items, CcmTag.DAT_X.ordinal(), xmDat.length - 1);
      out.jEnd += xmDat.length - 1;
    }
    return out;
  }

  ///// TODO ==> Dib2Root values

  public byte[] hidden_get(String key) {
    key = key.replace('\t', ' ').replace('\n', ' ');
    if (".".equals(key)) {
      return null;
    }
    byte[] out = qHidden.get(key);
    return (out == null) ? null : out.clone();
  }

  public String hidden_getHex(String key, boolean marked) {
    byte[] out = hidden_get(key);
    return (null == out) ? "" : StringFunc.hex4Bytes(out, marked);
  }

  public synchronized void hidden_set(String key, byte[] value) { // , QWord oid4ForcingInitial) {
    key = key.replace('\t', ' ').replace('\n', ' ').trim();
    if (null == value) {
      qHidden.remove(key);
      return;
    } else if (0 >= key.length()) {
      return;
    }
    // if (null != oid4ForcingInitial) {
    if (".".equals(key)) {
      // Should not be 'hidden' ...
      if (1 >= value.length) {
        return;
      }
      byte[] trash = qHidden.get("trash");
      trash =
          (null == trash)
              ? value
              : StringFunc.bytesUtf8(
                  StringFunc.string4Utf8(trash) + '\t' + StringFunc.string4Utf8(value));
      qHidden.put("trash", trash);
      return;
    } else if ("email_address".equals(key)) {
      // For new root ID or keep ID for new address!
      if (1 >= mUserAddr.length()) {
        final String s0 = StringFunc.string4Utf8(value);
        if (0 < s0.indexOf('@')) {
          mUserAddr = s0;
        }
      }
    } else if ("lastId".equals(key)) {
      final long id = BigSxg.long4String(new String(value, StringFunc.CHAR8), 0);
      if (0 < id) {
        DateFunc.alignId(id);
      }
      // return;
    }
    qHidden.put(key, value.clone()); // MiscFunc.toHexLiteral( value, true ) );
  }

  public synchronized void hidden_setHex(
      String key, String hexLiteral) { // , QWord oid4ForcingInitial) {
    byte[] val = null;
    if (null == hexLiteral) {
    } else if (0 >= hexLiteral.length()) {
      val = new byte[0];
    } else {
      char ch = hexLiteral.charAt(0);
      if (('X' == ch) && hexLiteral.startsWith("X'")) {
        val = StringFunc.bytes4Hex(hexLiteral);
      } else if (((('0' <= ch) && (ch <= '9')) || (('A' <= ch) && (ch <= 'F')))
          && hexLiteral.matches("[0-9A-F ]+")) {
        // Bad luck if it is meant to be a decimal number.
        val = StringFunc.bytes4Hex(hexLiteral);
      } else {
        // Not hex after all.
        val = StringFunc.bytesUtf8(hexLiteral);
      }
    }
    hidden_set(key, val);
  }

  public synchronized void hidden_remove(String key) {
    Dib2Root.log("preference_remove", key);
    qHidden.remove(key);
  }

  /////

  public QSeq variable_get(String name) {
    final QSeq out = QValPool.qval4String(zVariables.get(name));
    return (null == out) ? QSeq.NIL : out;
  }

  public int peekVariables(String[] yLines, int offs) {
    if (null == yLines) {
      return zVariables.size() + offs;
    }
    if ((yLines.length - offs) >= zVariables.size()) {
      TreeSet<String> sorted = new TreeSet<String>();
      for (String var : zVariables.keySet()) {
        // Atomic:
        final String val = zVariables.get(var);
        if (null != val) {
          sorted.add(var + '\t' + val);
        }
      }
      for (String line : sorted) {
        if (offs >= yLines.length) {
          // Race condition, ignore.
          break;
        }
        yLines[offs++] = line;
      }
    } else {
      char iMemory = '0';
      for (; (iMemory <= 'z') && (offs < yLines.length); ++iMemory) {
        // Atomic:
        final String val = zVariables.get("M" + iMemory);
        if (null != val) {
          yLines[offs++] = "M" + iMemory + '\t' + val;
        }
      }
    }
    return offs;
  }

  /**
   * Set variable. Name is expected to have more than 1 char for user variables.
   *
   * @param name Name of variable.
   * @param value null iff variable is to be removed.
   */
  public synchronized void variable_set(String name, QSeq value) {
    if (0 >= name.length()) {
      return;
    }
    name = StringFunc.nameNormalize(name, 0xff);
    ///// Reserved?
    if (1 >= name.length() && (('A' > name.charAt(0)) || ('X' <= name.charAt(0)))) {
      // e.g. '.' for ROOT
      return;
    } else if (('X' <= name.charAt(0))
        && ('Z' >= name.charAt(0))
        && (name.substring(1).matches("[0-9]+"))) {
      return;
    }
    if ((null == value) || (QWord.V_NULL == value)) {
      zVariables.remove(name);
    } else {
      zVariables.put(
          name, // QValMapSto.string4QVal(
          value.toStringFull());
    }
  }

  public synchronized void variable_remove(String name) {
    name = StringFunc.nameNormalize(name, 0xff);
    zVariables.remove(name);
  }

  public synchronized int variableForceOrIndex(String name, QSeq value) {
    //    final JResult pooled = JResult.get8Pool();
    name = StringFunc.nameNormalize(name, 0xff);
    int iTo = -1;
    if (1 >= name.length()) {
      if (0 >= name.length()) {
        return -1;
      } else if (('X' <= name.charAt(0)) && ('Z' >= name.charAt(0))) {
        iTo = name.charAt(0) - 'X';
      } else if (name.equals(".")) {
        mUserPid = value.atom().qValLong;
        return -1;
      }
    } else if (name.substring(1).matches("[0-9]+")) {
      if (('X' <= name.charAt(0)) && ('Y' >= name.charAt(0))) {
        return -1;
      } else if (('Z' == name.charAt(0))) {
        iTo = Integer.parseInt(name.substring(1)) + 2;
      }
    }
    if (0 > iTo) {
      if ((null == value) || (QWord.V_NULL == value)) {
        zVariables.remove(name);
      } else {
        zVariables.put(name, value.toStringFull());
      }
    }
    // UiPres.INSTANCE.setStackValueForced(iTo, value);
    return iTo;
  }

  public synchronized void clearXVar(boolean all) {
    variableForceOrIndex("C", QWord.V_NULL);
    variableForceOrIndex("E", QWord.V_NULL);
    variableForceOrIndex("L", QWord.V_NULL);
    variableForceOrIndex("T", QWord.V_NULL);
    if (all) {
      zVariables = new ConcurrentHashMap<String, String>();
    }
  }

  public static String toString4VariableList(QSTuple tup, char separator) {
    if (tup.jStart >= tup.jEnd) {
      return "";
    }
    final StringBuilder out = new StringBuilder((tup.jEnd - tup.jStart) * 10 + 10);
    final long flags = ((QWord) tup.items[CcmTag.CATS.ordinal()]).i64();
    if (Cats.DEF.flag != flags) {
      return tup.toStringFull(separator, CcmTag.DAT.ordinal());
    }
    int iVarName = -2;
    for (int i0 = tup.jEnd - 1;
        i0 >= CcmTag.DAT.ordinal(); // ,
        i0 = (CcmTag.DAT_X.ordinal() == i0) ? CcmTag.DAT.ordinal() : (i0 - 1)) {
      if (null == tup.items[i0]) {
        continue;
      }
      if (-2 < iVarName) {
        out.append("\n0#");
        out.append(BigSxg.sxgChecked64(DateFunc.currentTimeNanobisLinearized(true)));
        out.append(separator);
      }
      out.append(
          (-2 == iVarName)
              ? "X"
              : ((-1 == iVarName) ? "Y" : ((0 == iVarName) ? "Z" : ("Z" + iVarName))));
      out.append(separator);
      out.append(Cats.VAR.name());
      for (int ix = 1 + CcmTag.CATS.ordinal(); ix < CcmTag.DAT_X.ordinal(); ++ix) {
        out.append(separator);
      }
      String dat = tup.items[i0].toString();
      if (0 != separator) {
        dat = StringFunc.makePrintable(dat);
      }
      out.append(dat);
      ++iVarName;
    }
    return out.toString();
  }

  public static QMMap peekMappings() {
    return Dib2Root.ccmSto.zMappingsLabel;
  }

  public static QItemIf peek(long xPid, boolean forRef) {
    if (0 == xPid) {
      return null;
    }
    QItemIf out = Dib2Root.ccmSto.zMappingsPid.search(xPid);
    if (null == out) {
      out = Dib2Root.ccmSto.zMappingsPid.search(xPid ^ 1);
      if ((null == out) && forRef) {
        out = Dib2Root.ccmSto.zMappingsPid.search(xPid | (1L << 63));
        if (null == out) {
          out = Dib2Root.ccmSto.zMappingsPid.search((xPid ^ 1) | (1L << 63));
        }
      }
    }
    return out;
  }

  private static final QIfs.QItemIf[] searchBunch_empty = new QIfs.QItemIf[0];

  public static QIfs.QItemIf[] searchBunch(QSeq xPidOrLabel, long xCats) {
    QIfs.QItemIf[] items = null;
    QWord lbx;
    if ((xPidOrLabel instanceof QWord)) {
      lbx = (QWord) xPidOrLabel;
    } else {
      String s0 = xPidOrLabel.toStringFull();
      lbx = QWord.createQWord(s0, true);
    }
    if (0 != ((QWord) lbx).i64()) {
      final long pid = lbx.i64();
      QIfs.QItemIf item = peek(pid, false);
      if (null != item) {
        items = new QIfs.QItemIf[] {item};
      }
    }
    if (null == items) {
      items = Dib2Root.ccmSto.zMappingsLabel.searchBunch(lbx.shash);
      if ((null == items) || (0 == items.length)) {
        return searchBunch_empty;
      }
      final String label = xPidOrLabel.toStringFull();
      for (int i0 = 0; i0 < items.length; ++i0) {
        if (!(items[i0] instanceof QSTuple)) {
          items[i0] = null;
        } else if (xCats != ((CcmTag.CATS.getWord((QSTuple) items[i0]).i64() & xCats))) {
          items[i0] = null;
        } else if (!label.equals(CcmTag.LABEL.getWord((QSTuple) items[i0]).toStringFull())) {
          items[i0] = null;
        }
      }
    }
    int i1 = items.length - 1;
    for (int i0 = 0; i0 < i1; ++i0) {
      for (; (i0 < i1) && (null != items[i0]); ++i0)
        ;
      for (; (i0 < i1) && (null == items[i1]); --i1)
        ;
      items[i0] = items[i1];
      items[i1] = null;
    }
    return Arrays.copyOf(items, i1 + ((null == items[i1]) ? 0 : 1));
  }

  /** Trying to find matching unique data, preferring CTRB 0 over others. */
  // @return 0 if nothing found or not unique.
  public static long peek(QSeq xPidOrLabel, long xCats, boolean xUnique) {
    QIfs.QItemIf[] items = searchBunch(xPidOrLabel, xCats);
    QSTuple found = null;
    boolean unique = true;
    for (QIfs.QItemIf item : items) {
      if (!(item instanceof QSTuple)) {
        continue;
      }
      final QSTuple mpg = (QSTuple) item;
      if (null == found) {
        found = mpg;
        continue;
      }
      if (CcmTag.CNTRB.getWord(found).i64() == 0) {
        if (CcmTag.CNTRB.getWord(mpg).i64() == 0) {
          return 0;
        }
      } else if (CcmTag.CNTRB.getWord(mpg).i64() == 0) {
        found = mpg;
        unique = true;
      } else {
        unique = false;
      }
    }
    return ((null == found) || (!unique && xUnique)) ? 0 : found.getShash();
  }

  public static String getTaggedValueOr(String tag, String xDat, String xDelim, String xyOr) {
    tag = tag.startsWith(":") ? tag : (":" + tag + ':');
    if ((null == xDat) || !xDat.contains(tag)) {
      return xyOr;
    }
    int i1 = xDat.indexOf(tag) + tag.length();
    for (; (i1 < xDat.length()) && (' ' == xDat.charAt(i1)); ++i1) {}
    int i0 = i1;
    for (; (i1 < xDat.length()) && (0 > xDelim.indexOf(xDat.charAt(i1))); ++i1) {}
    return xDat.substring(i0, i1).trim();
  }

  public static String setTaggedValue(String tag, String xFull, String xNew) {
    tag = tag.startsWith(":") ? tag : (":" + tag + ':');
    int i0 = xFull.indexOf(tag);
    String out = "";
    String delim = "\n";
    if (0 <= i0) {
      //for (; (i1 < xFull.length()) && (' ' == xFull.charAt(i1)); ++i1) {}
      int i1 = i0 + tag.length();
      for (; (i1 < xFull.length()) && (' ' <= xFull.charAt(i1)); ++i1) {}
      out = xFull.substring(0, i0);
      xFull = xFull.substring(i1);
      delim = "";
    }
    return out + tag + " " + xNew + delim + xFull;
  }

  public static QSTuple[] findEntries4Cats(long xCatFlags) {
    QSTuple[] out = new QSTuple[256];
    long pid = -1L;
    int count = 0;
    do {
      final QIfs.QItemIf item = Dib2Root.ccmSto.zMappingsPid.searchNext(pid, null, 0);
      if (null == item) {
        break;
      }
      pid = item.getShash();
      if (!(item instanceof QSTuple)) {
        continue;
      }
      final QSTuple mpg = ((QSTuple) item);
      long cats = CcmTag.CATS.getWord(mpg).i64();
      if (xCatFlags != (xCatFlags & cats)) {
        continue;
      }
      if (count >= out.length) {
        out = Arrays.copyOf(out, 2 * out.length);
      }
      out[count++] = mpg;
    } while (pid != -1L);
    return Arrays.copyOf(out, count);
  }

  public static String getFirstEmailAddress(String xDat) {
    int ix = xDat.indexOf('@');
    if (0 >= ix) {
      return null;
    }
    int iTag = xDat.indexOf(":EMAIL:");
    if (ix < iTag) {
      ix = xDat.indexOf('@', iTag);
    }
    int ia = ix - 1;
    for (; ia >= 0; --ia) {
      final char ch = xDat.charAt(ia);
      if (('+' > ch) || (('9' < ch) && (ch < 'A')) || (('Z' < ch) && (ch < '_')) || (('z' < ch) && (ch < 0x80))) {
        break;
      }
    }
    for (++ix; ix < xDat.length(); ++ix) {
      final char ch = xDat.charAt(ix);
      if (('+' > ch) || (('9' < ch) && (ch < 'A')) || (('Z' < ch) && (ch < '_')) || (('z' < ch) && (ch < 0x80))) {
        break;
      }
    }
    final String addr = xDat.substring(ia + 1, ix);
    return (5 > addr.length()) ? null : addr;
  }

  private static ConcurrentHashMap<String, Long> findMainEmailAddr4Cats(long xCatFlags) {
    ConcurrentHashMap<String, Long> out = new ConcurrentHashMap<String, Long>(500);
    long pid = -1L;
    do {
      final QIfs.QItemIf item = Dib2Root.ccmSto.zMappingsPid.searchNext(pid, null, 0);
      if (null == item) {
        break;
      }
      pid = item.getShash();
      if (!(item instanceof QSTuple)) {
        continue;
      }
      final QSTuple mpg = ((QSTuple) item);
      long cats = CcmTag.CATS.getWord(mpg).i64();
      if (xCatFlags != (xCatFlags & cats)) {
        continue;
      }
      String dat = ((QSeq) mpg.getValue(CcmTag.DAT)).toStringFull();
      if (0 <= dat.indexOf(":GROUP:")) {
        continue;
      }
      String label = ((QSeq) mpg.getValue(CcmTag.LABEL)).toStringFull();
      String email = getFirstEmailAddress(dat);
      String opt = null;
      if (0 < label.indexOf('@')) {
        opt = email;
        email = getFirstEmailAddress(label);
      } else if (null != mpg.getValue(CcmTag.SRCS)) {
        String srcs = ((QSeq) mpg.getValue(CcmTag.SRCS)).toStringFull();
        opt = getFirstEmailAddress(srcs);
      }
      if ((null == email) || ((null != opt) && !email.equals(opt))) {
        if ((null == email) || (0 == (Cats.CONTACT.flag & cats))) {
          email = opt;
        }
      }
      if (null != email) {
        if (null != out.get(email)) {
          QSTuple cmp = (QSTuple) Dib2Root.ccmSto.zMappingsPid.search(out.get(email));
          if (0 < CcmTag.LABEL.getWord(cmp).toStringFull().indexOf('@')) {
            continue;
          }
        }
        out.put(email, mpg.stamp);
      }
    } while (pid != -1L);
    return out;
  }

  public static HashSet<Long> findMsgs4Chat(long xhChat) {
    QItemIf chat = Dib2Root.ccmSto.zMappingsPid.search(xhChat);
    if (null == chat) {
      return null;
    }
    String chatName = CcmTag.LABEL.getWord((QSTuple) chat).toStringFull();
    HashSet<Long> out = new HashSet<Long>(500);
    long pid = -1L;
    do {
      final QIfs.QItemIf item = Dib2Root.ccmSto.zMappingsPid.searchNext(pid, null, 0);
      if (null == item) {
        break;
      }
      pid = item.getShash();
      if (!(item instanceof QSTuple)) {
        continue;
      }
      final QSTuple mpg = ((QSTuple) item);
      long cats = CcmTag.CATS.getWord(mpg).i64();
      if (0 == (Cats.MSG.flag & cats)) {
        continue;
      }
      QItemIf ref = CcmTag.TAGSREFS.getValue(mpg);
      if ((null != ref)
          && (null != ((QSeq) ref).atomAt(0))
          && ((((QSeq) ref).atomAt(0).i64() & ((1L << 63) - 1)) == (xhChat & ((1L << 63) - 1)))) {
        out.add(mpg.stamp);
        continue;
      }
      String label = ((QSeq) mpg.getValue(CcmTag.LABEL)).toStringFull();
      if (label.startsWith(chatName)) {
        final int ic = label.indexOf(':');
        String s0 = label;
        if (0 < ic) {
          s0 = label.substring(0, ic);
        }
        if (chatName.equals(s0)) {
          out.add(mpg.stamp);
        }
      }
    } while (pid != -1L);
    return out;
  }

  /** Return 0 if null or no valid data, PID of contact if missing FP, otherwise PID of chat. */
  public static long ensureContact4Chat(long xPidChat, boolean ovrFp, boolean mark) {
    QIfs.QItemIf item = Dib2Root.ccmSto.zMappingsPid.search(xPidChat);
    if (!(item instanceof QSTuple)) {
      return 0;
    }
    final QSTuple chat = ((QSTuple) item);
    long cats = CcmTag.CATS.getWord(chat).i64();
    if (0 == (Cats.CHAT.flag & cats)) {
      return 0;
    }
    String label = CcmTag.LABEL.getValue(chat).toString();
    String dat = ((QSeq) CcmTag.DAT.getValue(chat)).toStringFull();
    if (0 > label.indexOf('@')) {
      return dat.contains(":GROUP:") ? xPidChat : 0;
    }
    QIfs.QItemIf ack = CcmTag.RECV.getValue((QSTuple) chat);
    QSTuple contact = null;
    String contactName = label.substring(0, label.indexOf('@'));
    Long pid = qPid4ReferencedId.get(label);
    if ((null == pid) || (0 == pid)) {
      pid = (Dib2Root.ccmSto.mUserAddr.equals(label)) ? Dib2Root.ccmSto.mUserPid : 0L;
      if ((0 != pid) && (null == Dib2Root.ccmSto.zMappingsPid.search(pid))) {
        contact = CcmSto.createEntry(QWord.createQWord(contactName, true), Cats.CONTACT.flag,
            QSeq.createQSeq(":EMAIL: " + label + "\n" + dat));
        contact.stamp = pid;
        Dib2Root.ccmSto.zMappingsPid.add4Handle(contact);
        Dib2Root.ccmSto.zMappingsLabel.add4Multi(getQSeq(contact, CcmTag.LABEL, null).getShash(), contact);
        return pid;
      } else if (0 == pid) {
        pid = peek((QSeq) CcmTag.LABEL.getValue(chat), Cats.CONTACT.flag, false);
      }
      if (0 == pid) {
        ConcurrentHashMap<String, Long> emails = findMainEmailAddr4Cats(Cats.CONTACT.flag);
        pid = emails.get(label);
        pid = (null != pid) ? pid : 0L;
      }
    }
    if (0L == pid) {
      item = CcmTag.TAGSREFS.getValue(chat);
      if (item instanceof QSeq) {
        pid = ((QSeq) item).atomAt(0).i64();
      } else {
        item = CcmTag.SRCS.getValue(chat);
        if (item instanceof QSeq) {
          String email = ((QSeq) item).toStringFull();
          email = getFirstEmailAddress(email);
          if ((null != email) && !label.equals(email) && (0 < email.indexOf('@'))) {
            return 0;
          }
        }
      }
    }
    if (0L != pid) {
      item = Dib2Root.ccmSto.zMappingsPid.search(pid);
      if (item instanceof QSTuple) {
        contact = (QSTuple) item;
        String dx = ((QSeq) CcmTag.DAT.getValue(contact)).toStringFull();
        if (dx.contains(":EMAIL:") && !label.equals(getFirstEmailAddress(dx))) {
          return 0;
        } else if (!dx.contains(":EMAIL:")) {
          dx = ":EMAIL:" + label + '\n' + dx;
          contact.set(CcmTag.DAT, QSeq.createQSeq(dx));
        }
      }
    }
    long out = xPidChat;
    if (null == contact) {
      contact =
          CcmSto.createEntry(
              QWord.createQWord(contactName, true),
              Cats.CONTACT.flag,
              QSeq.createQSeq(":EMAIL: " + label + "\n" + dat));
      Dib2Root.ccmSto.zMappingsPid.add4Handle(contact);
      Dib2Root.ccmSto.zMappingsLabel.add4Multi(getQSeq(contact, CcmTag.LABEL, null).getShash(), contact);
      out = contact.stamp;
    }
    item = CcmTag.DAT.getValue(contact);
    if (dat.contains(":FP:")) {
      String fp = dat.substring(dat.indexOf(":FP:") + 4);
      if (0 < fp.indexOf('\n')) {
        fp = fp.substring(0, fp.indexOf('\n'));
      }
      fp = fp.trim();
      if (item instanceof QSeq) {
        if (mark) {
          out = contact.stamp;
          chat.set(CcmTag.RECV, QSeq.createQSeq("?:FP"));
        }
        String dx = ((QSeq) item).toStringFull();
        if ((8 <= fp.length()) && !dx.contains(fp)) {
          out = contact.stamp;
          if (ovrFp) {
            out = contact.stamp;
            dx = dx.replaceAll("\n\\:FP\\:[^\n]*", "");
            dx += (dx.endsWith("\n") ? "" : "\n") + ":FP: " + fp;
            contact.set(CcmTag.DAT, QSeq.createQSeq(dx));
          } else if (dx.contains(":FP:")) {
            // Conflicting values ...
            chat.set(CcmTag.RECV, QSeq.createQSeq("?:FP"));
          }
        }
      }
    } else if ((null != ack) && (((QSeq) ack).toStringFull().contains("?:FP"))) {
      out = contact.stamp;
    } else if (!(item instanceof QSeq) || !((QSeq) item).toStringFull().contains(":FP:")) {
      out = contact.stamp;
    } else if (ovrFp && ((ack instanceof QWord) && ack.toString().startsWith("1970"))) {
      out = contact.stamp;
    }
    if (out != xPidChat) {
      qPid4ReferencedId.put(contactName, pid);
    }
    return out;
  }

  private static boolean importPrefsNVars_pidFound = false;

  private static QSeq[] importPrefsNVars(
      boolean replace,
      long flagsMarkAdjusttimeKeyHex,
      QSTuple mpg,
      long cats,
      long[] optIds,
      QSeq[] stack,
      StringBuilder trash) {
    final String key = getQSeq(mpg, CcmTag.LABEL, null).toStringFull();
    final QSeq value = getQSeq(mpg, CcmTag.DAT, QSeq.NIL);
    String valueS = value.toStringFull();
    if (0 >= key.length() || (0 >= valueS.length())) {
      if (0 < key.length()) {
        trash.append('\t');
        trash.append(key);
      }
      if (0 < valueS.length()) {
        trash.append('\t');
        trash.append(valueS);
      }
      return stack;
    }
    final boolean oldFormatPref =
        (0 == (Cats.HIDDEN.flag & cats)) && (0 != (Cats.PREF.flag & cats));
    ///// Do not override current entries when importing older data.
    if (((0 != (Cats.HIDDEN.flag & cats)) || oldFormatPref) && (1 < key.length())) {
      ///// 'value' as hexstring!
      if (replace
          || !qHidden.containsKey(key)
          || (null == qHidden.get(key))
          || (0 >= qHidden.get(key).length)) {
        if (0 != (flagsMarkAdjusttimeKeyHex & 4)
            && key.startsWith("KEY")
            && (valueS.length() > 3 * "CAAU".length())
            && valueS.matches("3.3.3.3.*")) {
          Dib2Root.ccmSto.hidden_setHex(key, StringFunc.string4HexUtf8(valueS)); // , null);
        } else {
          Dib2Root.ccmSto.hidden_setHex(
              key, valueS); // , replace ? QWord.createQWordInt(mpg.stamp) : null);
        }
      }
    } else {
      if (replace
          || !Dib2Root.ccmSto.zVariables.containsKey(key)
          || (null == Dib2Root.ccmSto.zVariables.get(key))
          || (0 >= Dib2Root.ccmSto.zVariables.get(key).toString().length())) {

        if (key.equals(".")) {
          if (replace && !importPrefsNVars_pidFound) {
            importPrefsNVars_pidFound = true;
            DateFunc.alignTime(-1, mpg.stamp);
            Dib2Root.ccmSto.mUserPid = DateFunc.alignTime(mpg.stamp, 1);
            if ((1L << 32) > Dib2Root.ccmSto.mUserPid) {
              Dib2Root.ccmSto.mUserPid = optIds[0];
            } else {
              Dib2Root.ccmSto.mUserAddr = "";
            }
            if (null != mpg.items[CcmTag.TIME.ordinal()]) {
              final QSeqIf tim = mpg.items[CcmTag.TIME.ordinal()];
              if ((tim instanceof QWord) && ((QWord) tim).isDate()) {
                long h62 = ((QWord) tim).getShash();
                double tx = DateFunc.eraTicks4Hash62(h62);
                DateFunc.alignId(DateFunc.nanobis4EraTicks(tx));
              }
            }
            if (null != mpg.items[CcmTag.DAT.ordinal()]) {
              final String udat = mpg.items[CcmTag.DAT.ordinal()].toString();
              if ((1 >= Dib2Root.ccmSto.mUserAddr.length()) && (1 < udat.length())) {
                Dib2Root.ccmSto.mUserAddr = udat;
              }
            }
            final long id = getQWord(mpg, CcmTag.CNTRB, QWord.V_0).i64();
            if (((1L << 32) < id) && (id == DateFunc.alignTime(id, 1))) {
              optIds[1] = id;
            }
          }
          return stack;
        }

        final int ix = Dib2Root.ccmSto.variableForceOrIndex(key, value);
        if (0 <= ix) {
          if (stack.length <= ix) {
            stack = Arrays.copyOf(stack, ((ix >>> 3) + 2) << 3);
          }
          stack[ix] = value;
        }
      }
    }
    return stack;
  }

  private static QSTuple update(QSTuple cur, QSTuple mpg) {
    long cats = CcmTag.CATS.getWord(cur).i64();
    if (0 == (cats & (CcmTag.CATS.getWord(mpg).i64()))) {
      return null;
    }
    QItemIf ctrb = CcmTag.CNTRB.getValue(mpg);
    QItemIf ctrbOld = CcmTag.CNTRB.getValue(cur);
    if ((null == ctrb) || ((ctrb instanceof QWord) && (Dib2Root.ccmSto.mUserPid == ((QWord) ctrb).i64()))) {
      ctrb = QWord.V_0;
    } else if ((ctrb instanceof QSeq) && ((QSeq) ctrb).toStringFull().contains(Dib2Root.ccmSto.mUserAddr)) {
      ctrb = QWord.V_0;
    }
    if ((null == ctrbOld) || ((ctrbOld instanceof QWord) && (Dib2Root.ccmSto.mUserPid == ((QWord) ctrbOld).i64()))) {
      ctrbOld = QWord.V_0;
    } else if ((ctrbOld instanceof QSeq) && ((QSeq) ctrbOld).toStringFull().contains(Dib2Root.ccmSto.mUserAddr)) {
      ctrbOld = QWord.V_0;
    }
    cur.set(CcmTag.CNTRB, (QSeqIf) ctrbOld);
    if (!ctrb.toString().equals(ctrbOld.toString()) && (0 == (cats & (Cats.MSG.flag | Cats.CHAT.flag)))) {
      cur.set(CcmTag.TRASHED, (QSeqIf) CcmTag.DAT.getValue(mpg));
      return cur;
    }
    if ((0 != (cats & Cats.CHAT.flag)) && (CcmTag.TIME.getValue(mpg) instanceof QWord)) {
      cur.set(CcmTag.TIME, (QWord) CcmTag.TIME.getValue(mpg));
    }
    QItemIf prot = CcmTag.RECV.getValue(mpg);
    cur.set(CcmTag.RECV, (prot instanceof QSeqIf) ? (QSeqIf) prot : QWord.V_0);
    String xlabel = ((QSeq) CcmTag.LABEL.getValue(mpg)).toString();
    String newData = ((QSeq) CcmTag.DAT.getValue(mpg)).toStringFull();
    String trash = ((QSeq) CcmTag.DAT.getValue(cur)).toStringFull();
    if (trash.contains(xlabel)) {
      xlabel = null;
    }
    boolean dropped = trash.length() > (newData.length()+2);
    String[] oldData = trash.split("\n");
    final QItemIf oldTrash = CcmTag.DAT.getValue(cur);
    trash += '\n';
    trash += (oldTrash instanceof QSeq) ? ((QSeq) oldTrash).toStringFull().trim() : "";
    if (1 < trash.length()) {
      for (String line : oldData) { //curData) {
        if ((null != xlabel) && (line.contains(xlabel))) {
          xlabel = null;
        }
        line = line.trim();
        if ((0 < line.length()) && newData.contains(line)) {
          trash = trash.replace(line, "");
        } else if (0 < line.length()) {
          dropped = true;
        }
      }
    }
    if ((null != xlabel) && (0 < xlabel.length())) {
      trash = xlabel + "\n" + trash;
    }
    trash = trash.replaceAll("\\n+", "\n").trim();
    cur.set(CcmTag.DAT, QSeq.createQSeq(newData));
    if (dropped) {
        if (trash.length() >= 2048) {
          trash = trash.substring(0, 2005) + "\n...";
        }
        cur.set(CcmTag.TIME, QWord.createQWordDate(DateFunc.eraTicks4NanobisOrCurrent(-1)));
        if (1 < trash.length()) {
          cur.set(CcmTag.TRASHED, QSeq.createQSeq(trash));
        }
    }
    return cur;
  }

  private static int addAsTrash_cTrash = 0;

  private static void addAsTrash(QSTuple mpg, String trash) {
    mpg.items[CcmTag.LABEL.ordinal()] = QWord.createQWord("TRASH" + (++addAsTrash_cTrash), true);
    mpg.items[CcmTag.CATS.ordinal()] = QWord.createQWordInt(Cats.TRASH.flag);
    mpg.items[CcmTag.DAT.ordinal()] = QSeq.createQSeq(trash.toString());
    Dib2Root.ccmSto.zMappingsPid.add4Handle(mpg);
    Dib2Root.ccmSto.zMappingsLabel.add4Multi(getQSeq(mpg, CcmTag.LABEL, null).getShash(), mpg);
  }
  
  private static boolean handleAck(QSTuple mpg) {
    if (0 == (CcmTag.CATS.getWord(mpg).i64() & Cats.TRASH.flag)) {
      return false;
    }
    final String dat = ((QSeq) CcmTag.DAT.getValue(mpg)).toStringFull();
    if ((null == dat) || !dat.startsWith("ACK")) {
      return false;
    }
    final String[] acks = dat.split("\n");
    mpg = null;
    for (int i0 = 1; i0 < acks.length; ++i0) {
      try {
        long pid = pid4String(acks[i0], true);
        QIfs.QItemIf ix = Dib2Root.ccmSto.zMappingsPid.search(pid);
        if ((null == ix) && acks[i0].matches("[0-9]+")) {
          pid = Long.parseLong(acks[i0]);
          ix = Dib2Root.ccmSto.zMappingsPid.search(pid);
        }
        if ((null != ix)
            && (ix instanceof QSTuple)
            && (0 != (CcmTag.CATS.getWord((QSTuple) ix).i64() & Cats.MSG.flag))) {
          ((QSTuple) ix).set(CcmTag.RECV, QWord.V_1);
        }
      } catch (Exception e0) {}
    }
    return true;
  }

  static QSeq[] importData(
      boolean asInternal, long updateCats, long flagsMarkAdjusttimeKeyHex, QSTuple... xDat) {
    QSeq[] stack = new QSeq[8];
    ConcurrentHashMap<String, Long> emails = CcmSto.findMainEmailAddr4Cats(Cats.CONTACT.flag);
    ConcurrentHashMap<String, Long> chats = CcmSto.findMainEmailAddr4Cats(Cats.CHAT.flag);
    StringBuilder trash = new StringBuilder();
    long[] optIds = new long[] {Dib2Root.ccmSto.mUserPid, Dib2Root.ccmSto.mUserPid};
    try {
      for (QSTuple mpg : xDat) {
        if (null == mpg) {
          continue;
        }
        if (0 != (CcmTag.CATS.getWord(mpg).i64() & Cats.TRASH.flag)) {
          if (handleAck(mpg)) {
            continue;
          }
        }
        final String label = CcmTag.LABEL.getWord(mpg).toString();
        if ((null == label) || (0 >= label.length())) {
          final QItemIf dat = CcmTag.DAT.getValue(mpg);
          if ((null == dat) || (0 >= dat.toString().trim().length())) {
            continue;
          }
          addAsTrash(mpg,dat.toString());
          continue;
        }
        final long cats = getQWord(mpg, CcmTag.CATS, QWord.V_0).i64();
        final QSeq ctrb = getQSeq(mpg, CcmTag.CNTRB, null); // QWord.V_1);
        if (0 != ((Cats.PREF.flag | Cats.VAR.flag) & cats)) {
          ///// Pairs that are stored separately (PREF, VAR, ...).
          if (!asInternal) { // && !VAR) || (2<=key.length()) ...
            continue;
          }
          if ((ctrb instanceof QWord) && ((QWord) ctrb).isNumeric()) {
            final long ctrbn = ((QWord) ctrb).i64();
            if (((optIds[0] == ctrbn) || (optIds[1] == ctrbn) || (Dib2Root.ccmSto.mUserPid == ctrbn))) {
              mpg.items[CcmTag.CNTRB.ordinal()] = QWord.V_0;
            }
            if (9999 < ctrbn) {
              if ((optIds[0] == optIds[1]) || (optIds[0] == Dib2Root.ccmSto.mUserPid)) {
                optIds[0] = ctrbn;
              } else if (optIds[1] == Dib2Root.ccmSto.mUserPid) {
                optIds[1] = ctrbn;
              }
            }
          }
          stack =
              importPrefsNVars(
                  (0 != ((Cats.PREF.flag | Cats.VAR.flag) & updateCats)),
                  flagsMarkAdjusttimeKeyHex,
                  mpg,
                  cats,
                  optIds,
                  stack,
                  trash);
          if (0>=trash.length()) {
            continue;
          }
          addAsTrash(mpg,trash.toString());
          trash = new StringBuilder();
          continue;
        } else if ((0 == cats)
            || (0 >= label.length() || ((1 == label.length()) && ('0' > label.charAt(0))))) {
          final QSeq value = getQSeq(mpg, CcmTag.DAT, QSeq.NIL);
          String valueS = value.toStringFull();
          if (0 < label.length() || (0 < valueS.length())) {
            addAsTrash(mpg, label + ' ' + valueS);
          }
          continue;
        } else {
          ///// Note: Not duplicating values if they are used as PREF or VAR.
          String sx = null;
          if ((ctrb instanceof QWord) && ((QWord) ctrb).isNumeric()) {
            final long ctrbn = ((QWord) ctrb).i64();
            if (asInternal && ((optIds[0] == ctrbn) || (optIds[1] == ctrbn) || (Dib2Root.ccmSto.mUserPid == ctrbn))) {
              mpg.items[CcmTag.CNTRB.ordinal()] = QWord.V_0;
            } else if ((0 < ctrbn) && (ctrbn < 9999)) {
              mpg.set(CcmSto.CcmTag.CNTRB, QWord.V_1);
              final QIfs.QSeqIf tSrc = mpg.getValue(CcmSto.CcmTag.SRCS);
              if (tSrc instanceof QSeq) {
                sx = ((QSeq) tSrc).toStringFull();
              }
            }
          } else if (null == ctrb) {
            mpg.items[CcmTag.CNTRB.ordinal()] = QWord.V_0;
          } else {
            mpg.set(CcmSto.CcmTag.CNTRB, QWord.V_1);
            sx = ctrb.toStringFull();
          }
          if (null != sx) {
            if (0 >= sx.indexOf('@')) {
              sx = getFirstEmailAddress(label);
              sx = (null == sx) ? "" : sx;
            }
            if (0 < sx.indexOf('@')) {
              if (sx.contains(Dib2Root.ccmSto.mUserAddr)) {
                mpg.set(CcmSto.CcmTag.CNTRB, QWord.V_0); //asInternal ? QWord.V_0 : QWord.V_1);
              } else {
                sx = CcmSto.getFirstEmailAddress(sx);
                Long id = emails.get(sx);
                if (null != id) {
                  mpg.set(CcmSto.CcmTag.CNTRB, QWord.createQWordInt((long) id));
                } else {
                  mpg.set(CcmSto.CcmTag.CNTRB, QSeq.createQSeq(sx));
                }
              }
            }
          }

          if (!asInternal) {
            QIfs.QItemIf cx = Dib2Root.ccmSto.zMappingsPid.search(mpg.stamp);
            if (null == cx) {
              long cur =
                  peek((QSeq) mpg.getValue(CcmTag.LABEL), CcmTag.CATS.getWord(mpg).i64(), false);
              if (0 != cur) {
                cx = Dib2Root.ccmSto.zMappingsPid.search(cur);
              }
            }
            QSTuple cur = null;
            while ((cur != cx) && (cx instanceof QSTuple)) {
              cur = (QSTuple) cx;
              cx = update(cur, mpg);
              if (null == cx) {
                do {
                  mpg.stamp += 2;
                  cx = Dib2Root.ccmSto.zMappingsPid.search(mpg.stamp);
                } while (null != cx);
              }
            }
            if (null != cx) {
              continue;
            }
          }

          if (0 != (CcmTag.CATS.getWord(mpg).i64() & Cats.CONTACT.flag)) {
            String dat = ((QSeq) CcmTag.DAT.getValue(mpg)).toStringFull();
            if ((null != dat) && (0 < dat.indexOf('@'))) {
              dat = getFirstEmailAddress(dat);
              if (null != dat) {
                if (dat.equals(Dib2Root.ccmSto.mUserAddr) && (mpg.stamp != Dib2Root.ccmSto.mUserPid)) {
                  if (optIds[0] != mpg.stamp) {
                    optIds[1] = mpg.stamp;
                  }
                  mpg.stamp = Dib2Root.ccmSto.mUserPid;
                }
                emails.put(dat, mpg.stamp);
              }
            }
          } else if (0 != (CcmTag.CATS.getWord(mpg).i64() & (Cats.CHAT.flag | Cats.MSG.flag))) {
            if (0 != (CcmTag.CATS.getWord(mpg).i64() & Cats.CHAT.flag)) {
              if (0 < label.indexOf('@')) {
                chats.put(getFirstEmailAddress(label), mpg.stamp);
              } else if (label.matches("G[0-9]+")) {
                chats.put(label, mpg.stamp);
              }
            } else {
              String chat = (0 < label.indexOf(':')) ? label.substring(0, label.indexOf(':')) : label;
              String opt = chatName4OldName.get(chat);
              if (!chats.containsKey(chat) && ((null == opt) || !chats.contains(opt))) {
                chats.put((null != opt) ? opt : chat, (long) 0);
              }
            }
          }
          if (null != mpg) {
            Dib2Root.ccmSto.zMappingsPid.add4Handle(mpg);
            Dib2Root.ccmSto.zMappingsLabel.add4Multi(getQSeq(mpg, CcmTag.LABEL, null).getShash(), mpg);
          }
        }
      }

      ///// Check IDs once again

      for (QSTuple mpg : xDat) {
        if (null == mpg) {
          continue;
        }
        if (0 != (CcmTag.CATS.getWord(mpg).i64() & Cats.TRASH.flag)) {
          if (handleAck(mpg)) {
            continue;
          }
        }
        if (mpg.items[CcmTag.CNTRB.ordinal()] instanceof QWord) {
          final long ctrb = ((QWord) mpg.items[CcmTag.CNTRB.ordinal()]).qValLong;
          if (((optIds[0] == ctrb) || (optIds[1] == ctrb) || (Dib2Root.ccmSto.mUserPid == ctrb))) {
            mpg.items[CcmTag.CNTRB.ordinal()] = QWord.V_0;
          }
        }
      }

      ///// Ensure chats

      if (!chats.containsKey(Dib2Root.ccmSto.mUserAddr) && (0 < Dib2Root.ccmSto.mUserAddr.indexOf('@'))) {
        chats.put(Dib2Root.ccmSto.mUserAddr, 0L);
      }
      for (Entry<String, Long> chat : chats.entrySet()) {
        if (0 == (long) chat.getValue()) {
          String name = chat.getKey();
          final String label = ((0 < name.indexOf('@')) || name.matches("G[0-9]+")) ? name
              : ("G" + ShashFunc.hash4shash(ShashFunc.shashBits4String(name, true)));
          if (0 < name.indexOf('@')) {
            name = (null == qOldLabel4ReferencedId.get(name)) ? name.substring(0, name.indexOf('@'))
                : qOldLabel4ReferencedId.get(name);
          }
          QIfs.QItemIf[] x0 = CcmSto.searchBunch(QWord.createQWord(label, true), Cats.CHAT.flag);
          if ((null == x0) || (0 == x0.length)) {
            QSTuple mpg = createEntry(QWord.createQWord(label, true), Cats.CHAT.flag,
                QWord.createQSeq(":TOPIC: " + name));
            Dib2Root.ccmSto.zMappingsPid.add4Handle(mpg);
            Dib2Root.ccmSto.zMappingsLabel.add4Multi(getQSeq(mpg, CcmTag.LABEL, null).getShash(), mpg);
          }
        }
      }

    } catch (Exception e0) {
      return null;
    }

    if (asInternal) {
      final ConcurrentHashMap<String, Long> ids = CcmSto.findMainEmailAddr4Cats(Cats.CONTACT.flag);
      for (Entry<String, Long> entry : ids.entrySet()) {
        qPid4ReferencedId.put(entry.getKey(), entry.getValue());
      }
    }
    if (asInternal || (1 < xDat.length)) {
      String ed = "" + (long) (1000 * DateFunc.currentTimeEraDay());
      ed = (3 >= ed.length()) ? "*" : (ed.substring(0, ed.length() - 3) + "." + ed.substring(ed.length() - 3));
      Dib2Root.ccmSto.variableForceOrIndex("L", QSeq.createQSeq("" + xDat.length + " record(s) on ED " + ed));
    }
    return stack;
  }

  static QSTuple[] exportData(QSeqIf[] xStack, int cStack, long catFilter, long bHidden) {
    int cMap = 0;
    QSTuple[] out = new QSTuple[Dib2Root.ccmSto.zVariables.size() + cStack + 1000];
    catFilter = (0 == catFilter) ? ~0 : catFilter;
    final QWord me = QWord.createQWordInt(Dib2Root.ccmSto.mUserPid);
    final QSeq meDat = QSeq.createQSeq(Dib2Root.ccmSto.mUserAddr);
    QSTuple tup = createEntry(QWord.V_DOT, Cats.VAR.flag, meDat);
    // For keeping lastID info:
    tup.set(CcmTag.TIME, QWord.createQWordDate(DateFunc.eraTicks4NanobisOrCurrent(tup.stamp)));
    tup.setShashOrIgnore(Dib2Root.ccmSto.mUserPid);
    tup.set(CcmTag.CNTRB, me);
    out[cMap++] = tup;
    if (0 < cStack) {
      out[cMap++] =
          createEntry(
              QWord.V_WILD,
              Cats.DEF.flag,
              (cStack == xStack.length) ? xStack : Arrays.copyOf(xStack, cStack));
    }
    if (0 != (catFilter & Cats.VAR.flag)) {
      if (1 == (bHidden & 1)) {
        for (String key : qHidden.keySet()) {
          out = (cMap >= out.length) ? Arrays.copyOf(out, 2 * out.length) : out;
          String val = StringFunc.hex4Bytes(qHidden.get(key), true);
          if (null == val) {
            continue;
          }
          out[cMap++] =
              createEntry(
                  QWord.createQWord(key, true),
                  Cats.HIDDEN.flag | Cats.VAR.flag,
                  QWord.createQWord(val, true));
        }
      }
      for (String key : Dib2Root.ccmSto.zVariables.keySet()) {
        out = (cMap >= out.length) ? Arrays.copyOf(out, 2 * out.length) : out;
        String val = Dib2Root.ccmSto.zVariables.get(key).toString();
        if (null == val) {
          continue;
        }
        out[cMap++] =
            createEntry(QWord.createQWord(key, true), Cats.VAR.flag, QWord.createQWord(val, true));
      }
    }
    int c0 = Dib2Root.ccmSto.zMappingsPid.dump(out, cMap, 0);
    if (0 > c0) {
      out = Arrays.copyOf(out, -c0);
      // Should now turn positive unless something skipped (and ignored) due to race condition:
      c0 = Dib2Root.ccmSto.zMappingsPid.dump(out, cMap, 0);
      if (0 > c0) {
        // Mark it as incomplete:
        out[out.length - 1] = createEntry(QOpMain.zzWIPSYM, Cats.VAR.flag, QWord.V_NULL);
      }
    }
    for (int i0 = 0; i0 < c0; ++i0) {
      final QItemIf ctrb = out[i0].items[CcmTag.CNTRB.ordinal()];
      if ((ctrb instanceof QWord) && ((QWord) ctrb).isNumeric() && (0 == ((QWord) ctrb).i64())) {
        out[i0].items[CcmTag.CNTRB.ordinal()] = me;
      }
    }
    return (0 <= c0) ? Arrays.copyOf(out, c0) : out;
  }

  static QSTuple[] archiveData() {
    int cMap = 0;
    QSTuple[] out = new QSTuple[1000];
    final QWord me = QWord.createQWordInt(Dib2Root.ccmSto.mUserPid);
    final QSeq meDat = QSeq.createQSeq(Dib2Root.ccmSto.mUserAddr);
    QSTuple tup = createEntry(QWord.V_DOT, Cats.VAR.flag, meDat);
    // For keeping lastID info:
    tup.set(CcmTag.TIME, QWord.createQWordDate(DateFunc.eraTicks4NanobisOrCurrent(tup.stamp)));
    tup.setShashOrIgnore(Dib2Root.ccmSto.mUserPid);
    tup.set(CcmTag.CNTRB, me);
    out[cMap++] = tup;
    int c0 = Dib2Root.ccmSto.zMappingsPid.dump(out, cMap, 0);
    if (0 > c0) {
      out = Arrays.copyOf(out, -c0);
      // Should now turn positive unless something skipped (and ignored) due to race condition:
      c0 = Dib2Root.ccmSto.zMappingsPid.dump(out, cMap, 0);
      if (0 > c0) {
        return null;
      }
    }
    long[] sizesPerDay = new long[33];
    long today = DateFunc.currentTimeNanobisLinearized(false);
    long yesterday = today - ((24 * 3600L) << 30);
    today = DateFunc.hash62oNanobis(today);
    yesterday = DateFunc.hash62oNanobis(yesterday);
    final long perDay = today - yesterday;
    long total = 0;
    for (int i0 = 0; i0 < c0; ++i0) {
      try {
        long cats = ((QWord) out[i0].getValue(CcmTag.CATS)).i64();
        long tim62 = ((QWord) out[i0].getValue(CcmTag.TIME)).i64();
        if (0 != (Cats.TRASH.flag & cats)) {
          tim62 = 0;
        } else if (0 != ((Cats.CONTACT.flag | Cats.CHAT.flag | Cats.HIDDEN.flag) & cats)) {
          tim62 = today;
        }
        long iDay = (int) ((today - tim62) / perDay);
        iDay = (0 > iDay) ? 0 : ((sizesPerDay.length > iDay) ? iDay : (sizesPerDay.length - 1));
        int siz = ((QSeq) out[i0].getValue(CcmTag.DAT)).toStringFull().length();
        sizesPerDay[(int) iDay] += siz;
        total += siz;
      } catch (Exception e0) {
      }
    }
    long part = 0;
    for (int i1 = sizesPerDay.length - 1; i1 > 3; --i1) {
      part += sizesPerDay[i1];
      if ((part >= total / 3) && (12000 < part)) {
        for (int i0 = 0; i0 < c0; ++i0) {
          try {
            long cats = ((QWord) out[i0].getValue(CcmTag.CATS)).i64();
            long tim62 = ((QWord) out[i0].getValue(CcmTag.TIME)).i64();
            if (0 != (Cats.TRASH.flag & cats)) {
              tim62 = 0;
            } else if (0 != ((Cats.CONTACT.flag | Cats.CHAT.flag | Cats.HIDDEN.flag) & cats)) {
              tim62 = today;
            }
            long iDay = (int) ((today - tim62) / perDay);
            if (iDay < i1) {
              out[i0] = null;
            } else {
              Dib2Root.ccmSto.zMappingsPid.remove(out[i0].stamp, out[i0]);
              Dib2Root.ccmSto.zMappingsLabel.remove(out[i0].items[0].getShash(), out[i0]);
            }
          } catch (Exception e0) {
          }
        }
        return out;
      }
    }
    return null;
  }

  public static String checkAck(QSeq ack, long millis) {
    millis = (0 < millis) ? millis : DateFunc.currentTimeMillisLinearized() - (1000 * 4 * 24 * 3600);
    String x0 = ack.toStringFull();
    int ia0 = x0.indexOf("*:") + 2;
    if (2 > ia0) {
      return null;
    }
    for (int ia1 = ia0; true; ++ia1) {
      if ((ia1 >= x0.length()) || (x0.charAt(ia1) < '0') || ('9' < x0.charAt(ia1))) {
        try {
          long tim = Long.parseLong(x0.substring(ia0, ia1));
          if (tim <= millis) {
            return x0.substring(0, ia0 - 2) + "\n" + x0.substring(ia1);
          }
        } catch (Exception e0) {
        }
        break;
      }
    }
    return null;
  }

  public static QSTuple[] ccmCleanup(QSTuple[] xyMpgs, long replaceCats, QWord ctrbFallback) {
    long stampMax = DateFunc.currentTimeNanobisLinearized(true);
    final LinkedHashMap<Long, QSTuple> goodIds = new LinkedHashMap<Long, QSTuple>();
    final ConcurrentHashMap<String, Long> contact4Name = new ConcurrentHashMap<String, Long>();
    final long minTime62 = DateFunc.hash62oDate("1970-02-02");

    for (int iMpg = 0; iMpg < xyMpgs.length; ++iMpg) {
      QSTuple mpg = xyMpgs[iMpg];
      if (null == mpg) {
        continue;
      }
      if (null == CcmTag.CATS.getValue(mpg)) {
        mpg.set(
            CcmTag.CATS,
            QWord.createQWordInt((-1L == replaceCats) ? Cats.NOTE.flag : Cats.TRASH.flag));
      }
      String label = ((QSeq) mpg.getValue(CcmTag.LABEL)).toStringFull();
      if (0 >= label.length()) {
        mpg.set(CcmTag.LABEL, QWord.createQWordInt(mpg.stamp));
      }
      String dat = ((QSeq) mpg.getValue(CcmTag.DAT)).toStringFull();
      if (0 != (CcmTag.CATS.getWord(mpg).i64() & Cats.CHAT.flag)) {
        // final String label = tup.getValue(CcmTag.LABEL).toString();
        if ((0 >= label.indexOf('@')) && dat.contains(":ADMIN:") && !dat.contains(":GROUP:")) {
          if (!dat.contains(":TOPIC:")) {
            dat = ":TOPIC: " + label + '\n' + dat;
          } else if (!dat.contains(label)) {
            dat += "\n:OLD: " + label;
          }
          dat = dat.replace(":ADMIN:", ":ADMX:");
          if (dat.contains(":OTHER:")) {
            // Group chat:
            dat = dat.replace(":OTHER:", ":GROUP:");
            dat = dat.replace(":EMAIL:", ":INIT:");
            int ix = dat.indexOf(":ADMX:") + 6;
            for (; (dat.charAt(ix) < '0') || ('9' < dat.charAt(ix)); ++ix)
              ;
            int ia = ix;
            for (; ('0' <= dat.charAt(ix)) && (dat.charAt(ix) <= '9'); ++ix)
              ;
            mpg.set(CcmTag.LABEL, QWord.createQWord("G" + dat.substring(ia, ix), true));
          } else {
            String s0 = CcmSto.getFirstEmailAddress(dat);
            if (null != s0) {
              mpg.set(CcmTag.LABEL, QWord.createQWord(s0, true));
            }
          }
          mpg.set(CcmTag.DAT, QSeq.createQSeq(dat));
        } else if (dat.contains(":GROUP:") && !label.matches("G[0-9]+")) {
          String lx = "G" + ShashFunc.hash4shash(ShashFunc.shashBits4String(label, true));
          mpg.set(CcmTag.LABEL, QWord.createQWord(lx, true));
        }
        chatName4OldName.put(label, CcmTag.LABEL.getWord(mpg).toString());
      }
      QWord tim = CcmTag.TIME.getWord(mpg);
      if ((null == tim) || (tim.getShash() < minTime62)) {
        if (CcmTag.CATS.getWord(mpg).i64() > (Cats.HIDDEN.flag << 1)) {
          mpg.set(
              CcmTag.TIME, QWord.createQWordDate(DateFunc.eraTicks4NanobisOrCurrent(mpg.stamp)));
        }
      }

      ///// Check ID

      final long stamp0 = mpg.stamp;
      if (stamp0 <= (1L << 32)) {
        mpg.stamp = DateFunc.currentTimeNanobisLinearized(true);
      } else if (stampMax <= stamp0) {
        // Time zone shifted or so ...?
        if ((125L << 30) >= (stamp0 - DateFunc.currentTimeNanobisLinearized(true))) {
          stampMax = DateFunc.alignTime(-1, stamp0);
        } else {
          mpg.stamp = (stamp0 & 0xffffffffffffffL) | (1L << 56);
        }
      }
      // Has to be unique ...
      while ((null != goodIds.get(mpg.stamp))
          || (null != badIds.get(mpg.stamp))
          || (null != Dib2Root.ccmSto.zMappingsPid.search(mpg.stamp))) {
        if (0 != (CcmTag.CATS.getWord(mpg).i64() & replaceCats)) {
          QIfs.QItemIf px = Dib2Root.ccmSto.zMappingsPid.search(mpg.stamp);
          if ((px instanceof QSTuple) && (0 != (CcmTag.CATS.getWord((QSTuple) px).i64() & replaceCats))) {
            if (null != update((QSTuple) px, mpg)) {
              xyMpgs[iMpg] = null;
              break;
            }
          }
        }
        mpg.stamp += 2;
      }
      if (null == xyMpgs[iMpg]) {
        continue;
      }
      if ((stamp0 != mpg.stamp) && (0 != stamp0)) {
        badIds.put(stamp0, mpg);
        QIfs.QItemIf tx = CcmTag.TRASHED.getValue(mpg);
        String trash = (dat.contains(":ID:")) ? "" : (":ID: " + stamp0 + "\n");
        if (tx instanceof QSeq) {
          trash += ((QSeq) tx).toStringFull();
        }
        if (2048 <= trash.length()) {
          trash = trash.substring(0, 2000) + "...";
        }
        mpg.set(CcmTag.TRASHED, QSeq.createQSeq(trash));
      }
      goodIds.put(mpg.stamp, mpg);

      if (0 != (CcmTag.CATS.getWord(mpg).i64() & Cats.CONTACT.flag)) {
        String email = CcmSto.getFirstEmailAddress(((QSeq) mpg.getValue(CcmTag.DAT)).toStringFull());
        if (null != email) {
          contact4Name.put(CcmTag.LABEL.getWord(mpg).toString(), mpg.stamp);
          if (0 < email.indexOf('@')) {
            chatName4OldName.put(email.substring(0, email.indexOf('@')), email);
          }
          qOldLabel4ReferencedId.put(email, CcmTag.LABEL.getWord(mpg).toString());
        }
      }
    }

    int count = xyMpgs.length;
    long millis4 = DateFunc.currentTimeMillisLinearized() - (1000 * 4 * 24 * 3600);

    ///// Check references to ID etc.

    for (int i0 = count - 1; i0 >= 0; --i0) {
      final QSTuple tup = xyMpgs[i0];
      if (null == tup) {
        --count;
        xyMpgs[i0] = xyMpgs[count];
        continue;
      }
      QItemIf ctrb = tup.items[CcmTag.CNTRB.ordinal()];
      if ((ctrb instanceof QWord) && ((QWord) ctrb).isNumeric()) {
        final long v0 = ((QWord) ctrb).i64();
        QSTuple refd = goodIds.get(v0);
        if (null == refd) {
          refd = badIds.get(v0);
        }
        if (null == refd) {
          if ((v0 <= (1L << 32)) || (stampMax <= v0)) {
            if (0 == (CcmTag.CATS.getWord(tup).i64() & Cats.MSG.flag)) {
              ctrb = ctrbFallback;
            } else if (0 != v0) {
              ctrb = QWord.V_1;
            }
          }
        } else {
          ctrb = QWord.createQWordInt(refd.stamp);
        }
      } else if (!(ctrb instanceof QWord)) {
        if (ctrb instanceof QSeq) {
          ctrb = QWord.createQWord(((QSeq) ctrb).toStringFull(), true);
        } else {
          ctrb = null;
        }
      }
      if (ctrb instanceof QWord) {
        tup.items[CcmTag.CNTRB.ordinal()] = (QWord) ctrb;
      } else if (null == tup.items[CcmTag.CNTRB.ordinal()]) {
        tup.items[CcmTag.CNTRB.ordinal()] = ctrbFallback;
      }

      if (0 != (CcmTag.CATS.getWord(tup).i64() & Cats.MSG.flag)) {
        final String label = CcmTag.LABEL.getWord(tup).toString();
        QIfs.QItemIf ack = CcmTag.RECV.getValue(tup);
        if (0 >= label.indexOf(':')) {
          String chatName = chatName4OldName.get(label);
          Long hx = qPid4ReferencedId.get(label);
          if ((null != hx) && (0 != (long) hx)) {
            chatName = label;
          } else if (null == chatName) {
            chatName = qOldLabel4ReferencedId.get(label);
            if ((null != chatName) && (0 >= chatName.indexOf('@') && !chatName.matches("G[0-9]+"))) {
              chatName = chatName4OldName.get(chatName);
            }
          }
          if (null == chatName) {
            hx = contact4Name.get(label);
            if ((null != hx) && (0 != (long) hx)) {
              QItemIf cx = peek(hx, false);
              if (cx instanceof QSTuple) {
                final QItemIf dx = ((QSTuple) cx).getValue(CcmTag.DAT);
                if (dx instanceof QSeq) {
                  chatName = getFirstEmailAddress(((QSeq) dx).toStringFull());
                }
              }
            }
          }
          if (null == chatName) {
            chatName = "G" + ShashFunc.hash4shash(ShashFunc.shashBits4String(label, true));
            if (count >= xyMpgs.length) {
              xyMpgs = Arrays.copyOf(xyMpgs, xyMpgs.length + contact4Name.size() + 1);
            }
            xyMpgs[count++] = CcmSto.createEntry(QWord.createQWord(chatName, true), Cats.CHAT.flag,
                QSeq.createQSeq(":TOPIC: " + label));
            chatName4OldName.put(label, chatName);
          }
          tup.set(CcmTag.LABEL,
              QWord.createQWord(chatName + ':' + BigSxg.sxg4Long(CcmTag.TIME.getValue(tup).getShash()), true));
          tup.set(CcmTag.TRASHED, QWord.createQWord(label, true));
        }
        if ((ack instanceof QSeq) && ((QSeq) ack).toStringFull().contains("*:")) {
          String x0 = checkAck((QSeq) ack, millis4);
          if (null != x0) {
            tup.set(CcmTag.RECV, QSeq.createQSeq(x0));
          }
        }
      }
    }
    return Arrays.copyOf(xyMpgs, count);
  }

  // =====
}
