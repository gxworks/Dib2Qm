// Copyright (C) 2022, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.picked.net;

import java.util.*;
import net.sf.dibdib.config.Dib2Root;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_net.*;
import net.sf.dibdib.thread_wk.*;
import net.sf.dibdib.thread_wk.CcmSto.CcmTag;

public class MessengerQm extends QmAct implements QOpNet.MessengerIf {

  private QmDb.Contact makeChat(long xhContactOrChat) {
    QIfs.QItemIf c0 = CcmSto.peek(xhContactOrChat, false);
    if (!(c0 instanceof QSTuple)) {
      return null;
    }
    long curTime = DateFunc.currentTimeMillisLinearized();
    QSTuple t0 = (QSTuple) c0;
    String s0 = ((QSeq) t0.getValue(CcmSto.CcmTag.DAT)).toStringFull();
    String addr = CcmSto.getFirstEmailAddress(s0);
    final String label = CcmTag.LABEL.getWord(t0).toStringFull();
    final QIfs.QItemIf tim62 = CcmTag.TIME.getValue(t0);
    QIfs.QItemIf ack = CcmTag.RECV.getValue(t0);
    final boolean normalChat = s0.contains(":GROUP:") && label.matches("G[0-9]+");
    QmDb.Contact out = null;
    if (normalChat) {
      //ack = null;
      addr = ((null == addr) || (0 >= addr.indexOf('@'))) ? label : addr.substring(0, addr.indexOf('@'));
      out = QmDb.db.contact_get_group_by_address_and_group(null, Integer.parseInt(label.substring(1)));
      out._name = CcmSto.getTaggedValueOr(":TOPIC:", s0, "\n", label);
      String[] lx = //Arrays.asList(
          CcmSto.getTaggedValueOr(":GROUP:", s0, "\n", addr).split("[, \\[\\]]+");
      HashSet<String> sx = new HashSet<String>();
      sx.addAll(out._members);
      sx.addAll(Arrays.asList(lx));
      out._members.clear();
      out._members.addAll(sx);
      out._members.remove("");
      for (String mem : out._members) {
        if (null == QmDb.db.cache.get(mem)) {
          QIfs.QItemIf[] am = CcmSto.searchBunch(QWord.createQWord(mem, true), Cats.CHAT.flag);
          if ((null != am) && (1 == am.length)) {
            makeChat(am[0].getShash());
          }
        }
      }
    } else {
      if ((null == addr) || (0 >= addr.indexOf('@'))) {
        if (0 >= label.indexOf('@')) {
          return null;
        }
        addr = (label.indexOf(':') > label.indexOf('@')) ? label.substring(0, label.indexOf(':')) : label;
      }
      out = QmDb.db.contact_get_person_by_address(addr);
      if (null == out) {
        return null;
      }
      //QIfs.QItemIf ack = CcmTag.RECV.getValue(t0);
      //out._unread = ((null == ack) || (5 >= ack.toString().length())) ? 1 : 0;
    }
    out.oid = t0.stamp;
    try {
      out._time_lastact = ((tim62 instanceof QWord)
          ? ((long) (1000.0 * (Double) DateFunc.convert4Hash62(((QWord) tim62).shash, "X")))
          : curTime);

      out._unread = ((ack instanceof QWord) && ((QWord) ack).isDate())
          ? ((long) (1000.0 * (Double) DateFunc.convert4Hash62(ack.getShash(), "X")))
          : -99;
    } catch (Exception e0) {}
    return out;
  }

  @Override
  public String[] inviteOrConfirmKey(long xhContact, boolean force) {
    // if (s0.contains(":FP:")...
    QmDb.Contact cx = makeChat(xhContact);
    if (null != cx) {
      send_key(cx, force); // || (0 < cx._unread));
      cx._unread = force ? -1 : ((0 < cx._unread) ? cx._time_lastact : 9);
      return QmDb.db.dump();
    }
    return null;
  }

  static final String[] receiveMessages_empty = new String[0];

  @Override
  public String[] receiveMessages(int msecs, boolean looping) {
    if (looping) {
      mail.idle(msecs);
      if (Mail.unread == 0) {
        return receiveMessages_empty;
      }
    }
    Mail.unread = 0;
    int rc = mail.recv();
    if (0 > rc) {
      // Delay next attempt
      try {
        Thread.sleep(msecs / 2);
      } catch (InterruptedException e) {
      }
    } else if (0 == rc) {
      mail.idle(msecs);
    }
    return QmDb.db.dump();
  }

  @Override
  public String[] sendOrAckMessages(long xhChat, boolean ackOnly, long... xahMsgs) {
    QmDb.MessageData next = null;
    QmDb.Contact cx = makeChat(xhChat);
    if (null == cx) {
      return receiveMessages_empty;
    }
    if ((cx._type == QuickMsg.TYPE_PERSON) && !QmDb.pgp.public_keyring_check_by_address(cx._address)) {
      inviteOrConfirmKey(xhChat, false);
    } else if ((cx._type != QuickMsg.TYPE_PERSON) && (0 > cx._unread) && !cx._name.matches("G[0-9]+")) {
      cx._unread = cx._time_lastact;
      next = new QmDb.MessageData(cx._inx, 1, cx._time_lastact, ":CHATNAME: " + cx._name);
    }
    boolean go = false;
    for (long hMsg : xahMsgs) {
      QIfs.QItemIf msg = CcmSto.peek(hMsg, false);
      if (!(msg instanceof QSTuple)) {
        continue;
      }
      QIfs.QItemIf tim62 = CcmTag.TIME.getValue((QSTuple) msg);
      QIfs.QItemIf ack = CcmTag.RECV.getValue((QSTuple) msg);
      QIfs.QItemIf from0 = CcmTag.CNTRB.getValue((QSTuple) msg);
      //QIfs.QItemIf trashed = CcmTag.TRASHED.getValue((QSTuple) msg);
      String txt = ((QSeq) (CcmTag.DAT.getValue((QSTuple) msg))).toStringFull();
      long from = ((from0 instanceof QWord) && ((QWord) from0).isNumeric()) ? ((QWord) from0).i64() : 0;
      if ((0 == from) && (from0 instanceof QSeq)) {
        if ((from0 instanceof QWord) && ((QWord) from0).isNumeric()) {
          from = ((QWord) from0).i64();
        } else {
          String sx = ((QSeq) from0).toStringFull();
          from = Dib2Root.ccmSto.mUserAddr.contains(sx) ? 0 : -1;
        }
      }
      from = (0 == from) || (Dib2Root.ccmSto.mUserPid == from) ? 1 : cx._inx;
      if ((1 == from) && ackOnly) {
        continue;
      }
      QmDb.MessageData mx =
          new QmDb.MessageData(
              cx._inx,
              (int) from,
              ((tim62 instanceof QWord)
                  ? ((long) (1000.0 * (Double) DateFunc.convert4Hash62(((QWord) tim62).shash, "X")))
                  : DateFunc.currentTimeMillisLinearized()),
              txt);
      mx.oid = BigSxg.sxg4Long(msg.getShash());
      final String ax = (ack instanceof QSeq) ? ((QSeq) ack).toStringFull() : "*:0";
      int i0 = ax.indexOf("*:") + 2;
      if (1 < i0) {
        for (; (i0 < ax.length()) && ('0' > ax.charAt(i0) && (ax.charAt(i0) > '9')); ++i0) {}
        int i1 = i0;
        for (; (i1 < ax.length()) && ('0' <= ax.charAt(i1) && (ax.charAt(i1) <= '9')); ++i1) {}
        if (i1 > i0) {
          mx.ack = Long.parseLong(ax.substring(i0, i1));
        }
        i0 = ax.indexOf(":ID:");
        if (i1 < i0) {
          i1 = ax.indexOf("\n", i0);
          i1 = (i0 < i1) ? i1 : ax.length();
          mx.oidOptional = ax.substring(i0 + 4, i1);
        }
      }
      if (0 >= mx.ack) {
        continue;
      }
      if ((cx._type == QuickMsg.TYPE_PERSON) || (1 != from)) {
        go = true;
        //if (null != trashed) {
        //  // Optional ID:
        //  mx._text = ((QSeq) trashed).toStringFull() + '\n' + mx._text;
        //}
        QmDb.db.message_add(mx);
      } else {
        mx.ack = 0;
        if (null != next) {
          QmDb.db.message_add(next);
          send_msg_attachment(cx, next); //null, next._text);
        }
        next = mx;
      }
    }
    if (null != next) {
      QmDb.db.message_add(next);
      send_msg_attachment(cx, next); //null, next._text);
      go = true;
    } else if (go) {
      send_msg_attachment(cx, null); //, " ");
    }
    return go ? QmDb.db.dump() : receiveMessages_empty;
  }

  @Override
  public String getFingerPrint() {
    return (null == QmDb.pgp) ? "" : QmDb.pgp.fingerprint(QmDb.my_addr) + '/' + QmDb.my_addr;
  }

  private static int dropZeros(char[] str, int from, int excl) {
    int i0 = 0;
    for (; i0 < excl; ++i0) {
      if (0 == str[i0]) {
        break;
      }
    }
    int put = i0++;
    for (; i0 < excl; ++i0) {
      if (0 != str[i0]) {
        str[put++] = str[i0];
      }
    }
    return put;
  }


  /** Replace mnemonics in string. '_' rfc1345 (without '_') '_', '__' -> '_', '_ ' -> '' ... */
  // '_' rfc1345 (without '_') '_',
  // '__' -> '_', '_ ' -> '',
  // '_[*' rfc1345 (any) ']_', '_[_' alpha1345 ']_',
  // '_//' quote '//_': for escaping mnemonics,
  // '_{{' quasi-quote '}}_' : '_{{' > STX, '}}_' > ETX
  // '_[[' list ']]_' : '_[[' > EOT, ']]_' > ENQ
  // '_<' template '>_' : '_<' > SO, '>_' > SI
  // '_#' NN/NNNN unicode hex '_': ,
  // '_^^' tipa '_',
  // '_^' n/r/t/@/A/... control char  ('^*': +0x80) '_'
  // Note: RFC1345 does not use '_' in combination with '{}[]()<>/' or '#...'.
  public static String string4Mnemonics_OLD(String mnem) {
    if (0 > mnem.indexOf('_')) {
      return mnem;
    }
    final char[] out = mnem.toCharArray();
    int len = out.length;
    int nextQuotedChunk = -1;
    for (int i0 = 0; i0 < len; ++i0) {
      if ('_' != out[i0]) {
        continue;
      }
      if (i0 >= nextQuotedChunk) {
        len = mnem.indexOf("_//", i0); // quote
        if (0 > len) {
          len = out.length;
        } else if (i0 < len) {
          ++len;
        } else {
          ///// Skip the quote.
          out[i0++] = 0;
          out[i0++] = 0;
          out[i0++] = 0;
          len = mnem.indexOf("//_", i0);
          if (i0 <= len) {
            i0 = len;
            out[i0++] = 0;
            out[i0++] = 0;
            out[i0] = 0;
          }
          len = 1 + mnem.indexOf("_//", i0); // next one
          if (0 >= len) {
            len = out.length;
          }
        }
        nextQuotedChunk = len;
      }
      if ((i0 + 1) >= out.length) {
        continue;
      }
      out[i0++] = 0;
      int ix;
      int v0;
      switch (out[i0]) {
        case '_':
          continue;
        case ' ':
          out[i0] = 0;
          continue;
        case '#': // unicode hex
          out[i0] = 0;
          if (0 > (ix = StringFunc.findChar('_', out, i0))) {
            continue;
          } else if ((4 + 1) < (ix - i0)) {
            continue;
          }
          v0 = 0;
          for (++i0; i0 < ix; ++i0) {
            final int b0 = 0xff & StringFunc.NIBBLE[out[i0] & 0x1f];
            v0 = (v0 << 4) | b0;
            out[i0] = 0;
          }
          out[i0 - 1] = (char) v0;
          out[i0] = 0;
          continue;
        case '^': // control char or tipa
          out[i0] = 0;
          if (0 > (ix = StringFunc.findChar('_', out, i0))) {
            continue;
          }
          if ('^' == out[i0 + 1]) {
            out[++i0] = 0;
            StringFunc.ipa4Tipa(out, i0 + 1, ix);
          } else {
            out[i0] = StringFunc.char4Escape(out[i0 + 1], out[ix - 1]);
            out[++i0] = 0;
            // Potential override is okay:
            out[ix - 1] = 0;
          }
          i0 = ix;
          out[i0] = 0;
          continue;
        case '{': // quote
          //        if (0 > (ix = findChars( "}_", out, i0 ))) {
          //          continue;
          //        }
          //        out[ i0 ] = TEMPLATE_START;
          //        i0 = ix;
          //        out[ i0 - 1 ] = TEMPLATE_END;
          //        out[ i0 ] = 0;
          //        continue;
        case '<': // template
        case '[': // RFC1345 or list
          //        if (0 > (ix = findChars( "]_", out, i0 ))) {
          if (0 > (ix = StringFunc.findChars("" + (char) (out[i0] + 2) + "_", out, i0))) {
            continue;
          }
          if ((out[i0 + 1] == out[i0]) || ('<' == out[i0])) {
            if (out[ix - 2] == out[ix - 1]) {
              out[ix - 2] = 0;
            }
            // Might not be the matching one!:
            out[ix - 1] =
                ('<' == out[i0]) ? StringFunc.TEMPLATE_END : (('[' == out[i0]) ? StringFunc.LIST_END : StringFunc.QUOTE_END);
            out[ix] = 0;
            if (out[i0 + 1] == out[i0]) {
              out[i0 + 1] = 0;
            }
            out[i0] =
                ('<' == out[i0]) ? StringFunc.TEMPLATE_START : (('[' == out[i0]) ? StringFunc.LIST_START : StringFunc.QUOTE_START);
            ++i0;
            continue;
          } else if ('[' != out[i0]) {
            //          || (('_' != out[ i0 + 1 ]) && ((ix - i0) > 6))) {
            continue;
          }
          out[ix--] = 0;
          out[i0++] = 0;
          break;
        default:
          if (' ' > out[i0]) {
            out[i0] = 0;
            continue;
          }
          // RFC1345 wo '_'
          if (0 > (ix = StringFunc.findChar('_', out, i0))) {
            continue;
          }
      }
      if ('_' == out[i0]) {
        i0 = StringFunc.replaceAlpha1345(out, i0, ix);
      } else { // '*', or wo '_'
        i0 = StringFunc.replaceRfc1345(out, i0, ix);
      }
      for (; i0 < ix; ++i0) {
        out[i0] = 0;
      }
      out[i0] = 0;
    }
    final int len2 = dropZeros(out, 0, len);
    return new String(out, 0, len2);
  }

  public static String mnemonics4String_OLD(String text, boolean leaveLfTab, boolean leaveExotic) {
    int i0 = 0;
    int lenLine = 0;
    for (; i0 < text.length(); ++i0, ++lenLine) {
      final char ch = text.charAt(i0);
      if ((78 < lenLine) && !leaveLfTab) {
        break;
      }
      if ((' ' > ch)) {
        if ('\n' == ch) {
          lenLine = -1;
        } else if ('\t' != ch) {
          break;
        }
        if (leaveLfTab) {
          continue;
        }
        // TODO: STX ETX SLS etc.
        break;
      } else if ((0x7f <= ch) || ('_' == ch)) { // (0 <= "_{}[]".indexOf( ch ))) {
        break;
      }
    }
    if (i0 >= text.length()) {
      return text;
    }
    char[] tmp = new char[1];
    StringBuilder out = new StringBuilder(text.length() * 2);
    out.append(text.substring(0, i0));
    int last = i0;
    final int len = text.length();
    for (; i0 < len; ++i0, ++lenLine) {
      char ch = text.charAt(i0);
      boolean breakLine = (77 < lenLine) && !leaveLfTab;
      if (!breakLine && (70 <= lenLine)) {
        breakLine = ((' ' > ch) || (0x7f <= ch) || ('_' == ch)); // (0 <= "_{}[]".indexOf( ch )));
      }
      if (breakLine) {
        for (int stop = ((i0 - 40) >= last) ? (i0 - 20) : last; i0 > stop; --i0) {
          if (' ' >= ch) {
            break;
          }
        }
        out.append(text.substring(last, i0));
        last = i0;
        out.append('\n');
        lenLine = -1;
        --i0;
        continue;
      }
      if ((' ' > ch)) {
        if (('\n' == ch) || ('\t' == ch)) {
          if (leaveLfTab) {
            lenLine = ('\n' == ch) ? -1 : lenLine;
            continue;
          }
          out.append(text.substring(last, i0));
          last = i0 + 1;
          if ('\n' == ch) {
            out.append("_^n_\n");
          } else if (40 > lenLine) {
            out.append("_^t_");
            lenLine += 3;
            continue;
          } else {
            out.append("_^t_\n");
          }
          lenLine = -1;
          continue;
        }
        if (!leaveExotic) {
          tmp[0] = ch;
          StringFunc.replaceCtrlNOld(tmp, 0, 1, 0x7);
          if (' ' < tmp[0]) {
            ch = tmp[0];
          }
        }
      } else if (0x7f > ch) {
        if ('_' == ch) {
          out.append(text.substring(last, i0 + 1));
          last = i0 + 1;
          ++lenLine;
          out.append('_');
          continue;
        }
        continue;
      }
      out.append(text.substring(last, i0));
      last = i0 + 1;
      if (' ' < ch) {
        String repl = StringFunc.kChar2Rfc1345.get(ch);
        if ((null != repl) && (1 < repl.length()) && (0 > repl.indexOf('_'))) {
          out.append('_');
          out.append(repl);
          out.append('_');
          lenLine += 1 + repl.length();
          continue;
        }
      }
      out.append("_#");
      if (0x100 <= ch) {
        out.append(StringFunc.HEX[(ch & 0xf000) >>> 12]);
        out.append(StringFunc.HEX[(ch & 0xf00) >>> 8]);
      }
      out.append(StringFunc.HEX[(ch & 0xf0) >>> 4]);
      out.append(StringFunc.HEX[ch & 0xf]);
      out.append('_');
      lenLine += (0x100 <= ch) ? 6 : 4;
    }
    out.append(text.substring(last, i0));
    return out.toString();
  }
}
