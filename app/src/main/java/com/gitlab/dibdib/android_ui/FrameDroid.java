// Copyright (C) 2018, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.android_ui;

import android.app.*;
import android.content.*;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.*;
import android.util.Log;
import com.gitlab.dibdib.common.*;
import java.io.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_feed.MainThreads;
import net.sf.dibdib.thread_ui.UiFrame;
import net.sf.dibdib.generic.QIfs.TsvCodecIf;

public class FrameDroid implements QIfs.PlatformIf {

  // =====

  static UiFrame mFrame;
  static FrameDroid instance = null;
  public static PlatformFunc platformSecondary = null;

  File qSafeDir;

  /////

  public static FrameDroid create(Context ctx) {
    if (null == instance) {
      instance = new FrameDroid();
      mFrame = new UiFrame();
    }
    if (null != ctx) {
      instance.qSafeDir = ctx.getFilesDir();
      UtilDroid.loadConfigValuesAll(ctx);
    }
    if (Dib2Lang.AppState.CREATE == Dib2Root.app.appState) {
      Dib2Root.create('A', Dib2Root.app.appShort, FrameDroid.instance, new TsvCodecIf[] { TcvCodecAes.instance });
      Dib2Root.init(Dib2Root.app.bAllowDummyPass);
    }
    return instance;
  }

  /*
   ///// The big mess of it ... :-) from developer.android.com @2021.09.30

  /////

   //Request code for selecting a PDF document.
   private static final int PICK_PDF_FILE = 2;

   //Request code for creating a PDF document.
   private static final int CREATE_FILE = 1;

   private void createFile(Uri pickerInitialUri) {
     Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
     intent.addCategory(Intent.CATEGORY_OPENABLE);
     intent.setType("application/pdf");
     intent.putExtra(Intent.EXTRA_TITLE, "invoice.pdf");

     // Optionally, specify a URI for the directory that should be opened in
     // the system file picker when your app creates the document.
     intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, pickerInitialUri);

     startActivityForResult(intent, CREATE_FILE);
   }

   private void openFile(Uri pickerInitialUri) {
     Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
     intent.addCategory(Intent.CATEGORY_OPENABLE);
     intent.setType("application/pdf");

     // Optionally, specify a URI for the file that should appear in the
     // system file picker when it loads.
     intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, pickerInitialUri);

     startActivityForResult(intent, PICK_PDF_FILE);
   }

   public void onActivityResult(int requestCode, int resultCode,
       Intent resultData) {
     if (requestCode == your - request - code
         && resultCode == Activity.RESULT_OK) {
       // The result data contains a URI for the document or directory that
       // the user selected.
       Uri uri = null;
       if (resultData != null) {
         uri = resultData.getData();
         // Perform operations on the document using its URI.
       }
     }
   }

   private String readTextFromUri(Uri uri) throws IOException {
     StringBuilder stringBuilder = new StringBuilder();
     try (InputStream inputStream = getContentResolver().openInputStream(uri);
         BufferedReader reader = new BufferedReader(
             new InputStreamReader(Objects.requireNonNull(inputStream)))) {
       String line;
       while ((line = reader.readLine()) != null) {
         stringBuilder.append(line);
       }
     }
     return stringBuilder.toString();
   }

   private void alterDocument(Uri uri) {
     try {
       ParcelFileDescriptor pfd = getActivity().getContentResolver().openFileDescriptor(uri, "w");
       FileOutputStream fileOutputStream = new FileOutputStream(pfd.getFileDescriptor());
       fileOutputStream.write(("Overwritten at " + System.currentTimeMillis() +
           "\n").getBytes());
       // Let the document provider know you're done by closing the stream.
       fileOutputStream.close();
       pfd.close();
     } catch (FileNotFoundException e) {
       e.printStackTrace();
     } catch (IOException e) {
       e.printStackTrace();
     }
   }

   */

  /////

  private String[] getLicense_lines;

  @Override
  public String[] getLicense(String[] xAdditionalVersionInfo, String... resources) {
    if (null == getLicense_lines) {
      String license = "(Version " + Dib2Constants.VERSION_STRING + ")\n";
      if (null != xAdditionalVersionInfo) {
        for (String line : xAdditionalVersionInfo) {
          license += line + '\n';
        }
      }
      license += '\n';
      Activity ax = Dib2Activity.getActivity();
      if (null == ax) {
        license += "(Cannot open license files)";
      } else {
        AssetManager am = ax.getAssets();
        InputStream is;
        try {
          is = am.open("license.txt");
          license += MiscFunc.readStream(is);
          license += '\n';
          is = am.open("spongycastle_license.txt");
          license += MiscFunc.readStream(is);
          license += '\n';
          is = am.open("javamail_license.txt");
          license += MiscFunc.readStream(is);
          license += '\n';
          is = am.open("apache_license2.txt");
          license += MiscFunc.readStream(is);
        } catch (IOException e) {
          return Dib2Constants.NO_WARRANTY;
        }
      }
      getLicense_lines = license.split("\n");
    }
    return getLicense_lines;
  }

  // android:requestLegacyExternalStorage="true"
  @Override
  public File getFilesDir(String... parameters) {
    try {
      if (0 == parameters.length) {
        return null;
      } else if (StringFunc.toLowerCase(parameters[0]).contains("safe")
          || StringFunc.toLowerCase(parameters[0]).contains("main")) {
        return qSafeDir;
      }
      File dir =
          // if (Build.VERSION.SDK_INT >=  Build.VERSION_CODES.O) {
          // createFile/ ... the big mess of it ... :-)
          // } else {
          Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
      if (!dir.exists()) {
        dir.mkdirs();
        if (!dir.exists()) {
          return null;
        }
      }
      if (StringFunc.toLowerCase(parameters[0]).contains("download")
          && !StringFunc.toLowerCase(parameters[0]).contains("sub")) {
        return dir;
      }
      File sub = new File(dir, "dibdib");
      if (!sub.exists()) {
        sub.mkdirs();
      }
      if (!sub.isDirectory()) {
        return dir;
      }
      return sub;
    } catch (Throwable e) {
      return null;
    }
  }

  @Override
  public void log(String... aMsg) {
    Log.d(aMsg[0], aMsg[1]);
  }

  @Override
  public void invalidate() {
    //final Context ctx = this;
    Activity act = Dib2Activity.getActivity();
    if (null == act) {
      boolean halt = false;
      if (Dib2Lang.AppState.EXIT_REQUEST.ordinal() <= Dib2Root.app.appState.ordinal()) {
        if (!MainThreads.isIdle()) {
          Thread.yield();
        }
        halt = true;
      } else if (MainThreads.isIdle()) {
        try {
          for (int i0 = 0; (i0 < 10) && MainThreads.isIdle(); ++i0) {
            Thread.sleep(500);
          }
        } catch (Exception e0) {}
      }
      if (MainThreads.isIdle()) {
        if ((0 < Dib2Root.app.serviceInterval_msec) && (null != Dib2Service.zService)) {
          Dib2Receiver.startTimer(Dib2Service.zService);
        }
        if ((null == Dib2Activity.getActivity()) && (null != Dib2Service.zService)
            && (Dib2Lang.AppState.CREATE.ordinal() < Dib2Root.app.appState.ordinal())) {
          Dib2Service.zService.stopService(true);//halt);
        }
      }
    } else {
      if (act instanceof Dib2Activity) {
        ((Dib2Activity) act).callUpdateViews();
      }
    }
  }

  @Override
  public boolean pushClipboard(String label, String text) {
    return Dib2Activity.pushClipboard(label, text);
  }

  @Override
  public String getClipboardText() {
    return Dib2Activity.getClipboardText();
  }

  @Override
  public void toast(String msg) {
    Dib2Activity.toast(msg);
  }

  @Override
  public Object parseUri(String path) {
    return Uri.parse(path);
  }

  @Override
  public InputStream openInputStreamGetType(Object xUri, String[] yType) {
    //  ContentResolver cR = ContentResolver.getContentResolver();
    //  InputStream is;
    //  // try {
    //  is = cR.openInputStream(uri);
    //  // } catch (FileNotFoundException e) {
    //  if (null == is) {
    //    // Log.d("send_message", e.getMessage());
    //    return null;
    //  }
    //  String type = cR.getType(uri);
    return (null == platformSecondary)
        ? null
        : platformSecondary.openInputStreamGetType(xUri, yType);
  }

  // =====
}
